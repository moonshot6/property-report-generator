# Property Report Generator User's Guide
Version 1.2.0

This tool is intended to generate a series of reports based on property Average Daily Rates (ADR) data, monthly occupancy data, and seasonal rates. The information is then aggregated into several charts and tables that represent the performance of a property during a 3 year interval (last 2 years plus present).

## Table Of Contents

[[_TOC_]]

## Installation

Open the folder containing the ZIP file called `property-report-generator.zip` , right click the file, choose "Extract All" from the menu that appear, and click "Extract". This process may take a couple of minutes to finish. 

Once the file is extracted, open the folder called `property-report-generator` and double-click `property-report-generator.exe` to start the installation process. If you're prompted by windows to allow this file to run, please select "Yes". Installation should proceed automatically. After completion, an entry in your start menu will be created called Property Report Generator.

<div style="page-break-after: always"></div>

##  Report View
When opening the application for the first time, you'll immediately see the property report screen that allows you to quickly print reports for clients in the application. To view the property editor, click the ![image-20210615141027623](docs/images/image-20210615141027623.png) at the top left of the screen.
![image-20210615141344078](docs/images/image-20210615141344078.png)

## Property Editor
This screen displays a list of all clients/properties imported into the tool. Clients can be added either manually or imported through CSV files (to be discussed later in this document).

![image-20210615141809132](docs/images/image-20210615141809132.png)

Selecting a saved client can be done by clicking the drop down on this page and clicking an item from the list or by typing part of the client's name to filter the list. After selecting a client, the application will display a list of all associated seasonal rates and fees. Normally data for each property is bulk imported (discussed in the Import Data section of this document), but this tool does provide the functionality to manually add either seasonal or fee data to each property.

![image-20210615141913114](docs/images/image-20210615141913114.png)

Seasons can be added by clicking the "Link Season" button and selecting "Add New Season". Prior to adding new season, confirm that one with the right name and date range does not already exist in the drop down to the left of this dialog. If the drop down or the "Link Existing" button cannot be clicked, no seasons are currently stored in the application.

![image-20210615142002307](docs/images/image-20210615142002307.png)

Similar to seasons, fees can be added by clicking the "Add Fee" button. On this screen you can choose to either add a fee from a known list of previous fees or add a new entry. Fees must have unique names for each property. To add an existing fee, click and item from the drop down to the left and then click "Add Existing". To add a new fee, click the "Add New Fee" button.

![image-20210615142031068](docs/images/image-20210615142031068.png)

## Importing Data
To import CSV data into this application, click the "Import Data" button at the top of the *Property Editor* page. A dialog should appear with two options:

![image-20210615142059744](docs/images/image-20210615142059744.png)

- Import Property Rates/Occupancy - *Click this button to open a wizard that will guide you through importing ADR, Occupancy, Revenue, and Seasonal Daily Rates data.*
- Import Property Fees - *Click this button to bulk import fee data for all properties*

### Importing Property Rates
Click "Import Property Rates/Occupancy" from the "Select an option" dialog. On this screen, you'll be presented with a series of steps for importing ADR, Occupancy, Revenue, and Daily Rates data. Each step requires a CSV file that matches the example data displayed. 
#### Importing ADR Data
Drag and drop a matching CSV file from your computer for each of the three years indicated on this step. You can also click each box individually and a dialog will appear for you to pick the file from your computer. After each box has a green check mark, click "Next".

![image-20210615142215745](docs/images/image-20210615142215745.png)

![image-20210615142316095](docs/images/image-20210615142316095.png)

### Importing Occupancy Data
The exact same process applies to this step. Drag and drop a matching CSV file from your computer for each of the three years indicated on this step. You can also click each box individually and a dialog will appear for you to pick the file from your computer. After each box has a green check mark, click "Next". 
### Importing Monthly Revenue Data
No action is required on this step as the data is calculated as a product of ADR and occupancy information. After the revenue data is saved, the wizard will inform you if there were any discrepancies between the ADR and occupancy data for each of the three years. Please review this information, correct any irregularities, and repeat the last two steps to imported corrected data. If no errors exist or you wish to fix them at a later date, click "Next".

![image-20210615142356296](docs/images/image-20210615142356296.png)

![image-20210615142411314](docs/images/image-20210615142411314.png)

### Importing Daily (Seasonal) Rates Data
Drag and drop a CSV file that contains seasonal rate data for your properties. This data should follow the format illustrated in the "Example Data" image. After dropping your data, you'll be presented with a screen that will allow you to add and group the rates into seasons.

To start, select a season from the right-hand "target season" drop down. If no seasons exist, you can add one with the "+" button to the right of the drop-down. On this dialog, provide a name, minimum stay (in nights), start date, and end date for the season. Click "Save" to add the season when finished or "Cancel" to discard the changes. This process must be repeated for **each** season that needs to be part of the client report. 

![image-20210615142604468](docs/images/image-20210615142604468.png)

![image-20210615142631118](docs/images/image-20210615142631118.png)

Once all of your season groups are created, select one from the drop down and drag/drop any number of labels from the left box to the box on the right that should be imported as part of that season group. To change season groups, select a new one from the drop down list or add one. Continue this process until the labels on the left have all been mapped or are no longer relevant (some CSV files may contain columns that *do not* map to a season group). When you are confident in your mappings, click "Save and Close" to continue.

![image-20210615142714831](docs/images/image-20210615142714831.png)

If you have any labels remaining in the box to the left, you'll be presented with a warning to confirm your selection.

Click "Finish" to close the import wizard. You should now be able to select a property from the drop down list in the Property Editor and see the data you imported.

![image-20210615142807067](docs/images/image-20210615142807067.png)

### Importing Fees
To import fees, click "Import Data" and then select "Import Property Fees" from the dialog that appears. On this screen, you'll see an example of the CSV data that can be imported. Ensure data follows a similar format prior to importing. On the dialog there are two special checkboxes you can use to alter the imported fees:

![image-20210615142840237](docs/images/image-20210615142840237.png)

- Average All Fee Data - *Instead of grabbing the value of each fee from first column of each fee, average all columns into a single value.*
- Ignore Fees with $0 Value - *Don't import any fees with a value of $0.*
Check or uncheck either of these two boxes and **then** drop your CSV into the file upload area at the bottom of the dialog.

A new dialog should appear that will allow you to add and group your fees where appropriate. Select a fee name from the drop-down on the right side of this dialog or click the "+" to add a new fee. Drag the column names from the left box to the right box for each fee you wish to import. Multiple columns can be added to a single fee to add them together as single entry in your client's report. 
Once you're finished mapping CSV columns to fees, click "Save And Close" to continue. The tool will import and save each fee to the appropriate property and then display them on the Property Editor. 

### Creating Reports
To generate PDF reports for all properties click the "Generate Reports" button to switch to the Report View. Click the ![image-20210615143447975](docs/images/image-20210615143447975.png) icon at the top right of the page to start the export process. The tool will first verify the data imported for each property and provide a detailed summary of any issues that may need to be addressed prior to saving the reports.

![image-20210615143530148](docs/images/image-20210615143530148.png)

To view an issue for a property, click the ![image-20210615143619880](docs/images/image-20210615143619880.png) to the left of the property name to view the warnings/errors that need attention.

![image-20210615143556760](docs/images/image-20210615143556760.png)

 To continue, click "Print Anyway" to start the export process. 
The tool will ask you to identify a folder to drop the PDFs in. Pick a folder on your computer and click "Select Folder". To cancel, the export process, click "Cancel" at any time.

## Advanced Operations
### Manually Adding a Season Group To A Property
Season Groups can be manually added to a property from the Property Editor at any time. Select a property/client from the drop down at the top of the screen and click the "Link Season" button that appears. On this screen, you can either add a new season or link an existing one. If you choose to add or link an existing season that does not have rate data, the tool will display a "Data Missing" warning in the "Daily Rate" column. To add rate data, click the wrench icon to the right of the season name and fill in the rate manually.

To remove a season group from a property, click the unlink button to the right of the season name.

Only seasons that appear beneath each property will appear on the final report. Unlinked seasons will not be displayed.


### Modifying Season Groups
Season groups can be modified at any time. Navigate to the Property Editor screen and click the gear icon at the top right. On this screen, click "Season Groups" on the left if not already selected. 

Here you can pick a season group from the drop down. You will immediately see a list of properties that already have seasonal rates for this group on the right. 

You can add properties from the left by checking them and clicking the "Add" button in the center. To add all properties, click "Add All".

> Properties without data for the season group will show as "$0" on the report screen. You can add this data manually by selecting the property on the Property Editor screen and then clicking the wrench icon to the right of the season name.

To remove properties, check them in the right hand box and then click the "Remove" button in the center. To remove all properties from this season group, click "Remove All".

To edit the name, start date, end date, or minimum stay values for the selected season group, click the "+" button at the bottom right and click the "Edit" button from the menu that appears.

To delete the selected season, click the "+" button at the bottom right and select "Delete" from the menu that appears.

You can add a season at any time on this screen by clicking the "+" button at the bottom right and selecting "Add". Provide a name, minimum night stay, start date and end date and then click "Save". The selected season should change automatically.

## Resetting The Application

Under the settings menu ![image-20210615143050145](docs/images/image-20210615143050145.png), click the "Data Caching" option from the left. Here you can click the "Reset All Application Data" button to completely wipe the database used by this tool. Since this process is irreversible, a confirmation dialog will appear. Click "Yes" to continue and reset back to factory defaults.

