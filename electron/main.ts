import { app, ipcMain, BrowserWindow } from 'electron';
import { IUpdateStatus, UpdateStatusType } from '../src/model/UpdateStatus';
import * as path from 'path';
import * as isDev from 'electron-is-dev';
import { autoUpdater } from 'electron-updater';
import installExtension, { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';

let win: BrowserWindow | null = null;

///////////////////
// Auto upadater //
///////////////////
autoUpdater.requestHeaders = { 'PRIVATE-TOKEN': 'C6GFfBvz_FpupazFsmfk' };
autoUpdater.autoDownload = true;

autoUpdater.setFeedURL({
    provider: 'generic',
    url:
        'https://gitlab.com/api/v4/projects/27554148/jobs/artifacts/master/raw/electron-release/?job=build-job',
});

function sendStatusToWindow(status: IUpdateStatus) {
    if (win) {
        win.webContents.send('asynchronous-message', status);
    }
}

autoUpdater.on('checking-for-update', function () {
    sendStatusToWindow({
        type: UpdateStatusType.CheckingForUpdates,
        message: 'Checking for updates...',
    });
});

autoUpdater.on('update-available', function (info) {
    sendStatusToWindow({
        type: UpdateStatusType.UpdateAvailable,
        message: 'Update available.',
    });
});

autoUpdater.on('update-not-available', function (info) {
    sendStatusToWindow({
        type: UpdateStatusType.UpdateNotAvailble,
        message: 'No update required!',
    });
});

autoUpdater.on('error', function (err) {
    sendStatusToWindow({
        type: UpdateStatusType.UpdateError,
        message: `Error in auto-updater: ${err.message}`,
    });
});

autoUpdater.on('download-progress', function (progressObj) {
    const transferred = (progressObj.transferred * 0.0000076294).toFixed(2);
    const total = (progressObj.total * 0.0000076294).toFixed(2);
    const mbps = (progressObj.bytesPerSecond * 0.0000076294).toFixed(2);

    sendStatusToWindow({
        type: UpdateStatusType.DownloadProgress,
        message: `Downloading: (${transferred}MB /${total}MB) ${mbps}Mbps`,
        downloadProgress: progressObj.percentage,
    });
});

autoUpdater.on('update-downloaded', function (info) {
    sendStatusToWindow({
        type: UpdateStatusType.UpdateDownloaded,
        message: `Update Downloaded. Restarting Momentarily.`,
    });
});

autoUpdater.on('update-downloaded', function (info) {
    sendStatusToWindow({
        type: UpdateStatusType.RestartingToUpdate,
        message: 'Restarting...',
    });
    setTimeout(function () {
        autoUpdater.quitAndInstall();
    }, 1000);
});

ipcMain.on('request-update', (event) => {
    autoUpdater.checkForUpdates().catch((reason) => {
        sendStatusToWindow({
            type: UpdateStatusType.UpdateError,
            message: 'Error in auto-updater.',
        });
    });
});

function createWindow() {
    win = new BrowserWindow({
        width: 930,
        height: 600,
        minWidth: 930,
        minHeight: 500,
        autoHideMenuBar: true,
        webPreferences: {
            nativeWindowOpen: true,
            nodeIntegration: true,
            enableRemoteModule: true,
        },
    });

    if (isDev) {
        win.loadURL('http://localhost:3000/index.html');
    } else {
        // 'build/index.html'
        win.loadURL(`file://${__dirname}/../index.html`);
    }

    win.on('closed', () => {
        win = null;
    });

    // Hot Reloading
    if (isDev) {
        // 'node_modules/.bin/electronPath'
        require('electron-reload')(__dirname, {
            electron: path.join(__dirname, '..', '..', 'node_modules', '.bin', 'electron'),
            forceHardReset: true,
            hardResetMethod: 'exit',
        });
    }

    // DevTools
    installExtension(REACT_DEVELOPER_TOOLS)
        .then((name) => console.log(`Added Extension:  ${name}`))
        .catch((err) => console.log('An error occurred: ', err));

    if (isDev) {
        win.webContents.openDevTools();
    }
}

app.on('ready', () => {
    createWindow();
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});

process.on('uncaughtException', (error) => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
