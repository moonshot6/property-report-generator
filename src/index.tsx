import React, { Suspense } from 'react';
import { Provider } from 'jotai';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { HashRouter } from 'react-router-dom';
import { ErrorBoundary, FallbackProps } from 'react-error-boundary';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Button, CircularProgress } from '@material-ui/core';

function ErrorFallback(fallbackProps: FallbackProps) {
    const reloadApplication = () => {
        window.location.reload();
    };

    const submitErrorReport = async () => {
        const name = encodeURIComponent(`Runtime Error: ${fallbackProps.error.name}`);
        const description = encodeURIComponent(
            `The application threw the following error: ${fallbackProps.error.message}`
        );
        const tags = encodeURIComponent('automated-reporting');

        const MAX_URL_LENGTH = 2000;
        const API_KEY = 'mLvnQ7XEV-RABmYn2K2j';

        const createTicketUrl = `https://gitlab.com/api/v4/projects/27554148/issues?title=${name}&description=${description}&labels=${tags}`;

        const response = await fetch(createTicketUrl, {
            method: 'POST',
            headers: {
                'PRIVATE-TOKEN': API_KEY,
            },
        });

        if (response.ok) {
            const ticketInfo = await response.json();
            const issueId = ticketInfo.iid;

            if (fallbackProps.error.stack && issueId) {
                let createCommentUrl = `https://gitlab.com/api/v4/projects/27554148/issues/${issueId}/notes?body=`;
                const componentStack = encodeURIComponent(
                    fallbackProps.error.stack.substr(0, MAX_URL_LENGTH - createCommentUrl.length)
                );

                createCommentUrl = `${createCommentUrl}${componentStack}`;

                // Add the component stack as a comment
                fetch(createCommentUrl, {
                    method: 'POST',
                    headers: {
                        'PRIVATE-TOKEN': API_KEY,
                    },
                });
            }
        }
    };

    React.useEffect(() => {
        submitErrorReport();
    }, []);

    return (
        <Alert severity={'error'}>
            <AlertTitle>Something went wrong with the application.</AlertTitle>
            <pre>{fallbackProps.error.message}</pre>
            <pre>{fallbackProps.error.stack}</pre>
            <Button onClick={reloadApplication}>Click here</Button> to try relaunching the
            application. An issue report will be submitted automatically.
        </Alert>
    );
}

ReactDOM.render(
    <ErrorBoundary
        FallbackComponent={ErrorFallback}
        onReset={() => {
            // reset the state of your app so the error doesn't happen again
        }}
    >
        <Provider>
            <HashRouter>
                <Suspense fallback={<CircularProgress />}>
                    <App />
                </Suspense>
            </HashRouter>
        </Provider>
    </ErrorBoundary>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
