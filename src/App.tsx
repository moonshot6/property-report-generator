import React, { Suspense } from 'react';
import { Route, Switch, useHistory, useLocation } from 'react-router-dom';
import { Backdrop, CircularProgress } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';

import { ConfigurationManager } from 'components/ConfigurationManager';
import { ClientEditingPage } from 'components/pages/editor/ClientEditingPage';
import { ImportDataDialog } from 'components/ImportDataDialog';
import { ClientReport } from 'components/pages/export/components/ClientReport';
import { ApplicationToolbar } from 'components/ApplicationToolbar';
import { ISimpleDialogListItem, SimpleDialogList } from 'components/SimpleListDialog';
import { AttachMoney, Timeline } from '@material-ui/icons';
import { useSeasonGroupManager } from './actions/Configuration';
import { PrintContainer } from './components/PrintContainer';
import { ImportFeesDialog } from './components/ImportFeesDialog';
import { VerifyDataDialog } from './components/VerifyDataDialog';

const dataImportOption: ISimpleDialogListItem = {
    key: 'importPropertyData',
    icon: <Timeline />,
    text: 'Import Property Rates/Occupancy',
};

const feesImportOption: ISimpleDialogListItem = {
    key: 'importFeesData',
    icon: <AttachMoney />,
    text: 'Import Property Fees',
};

function App() {
    const [isImportDialogOpen, setImportDialogOpen] = React.useState<boolean>(false);
    const [isPrintActive, setIsPrintActive] = React.useState<boolean>(false);
    const [isImportTypeDialogOpen, setIsImportTypeDialogOpen] = React.useState<boolean>(false);
    const [isImportFeesDialogOpen, setIsImportFeesDialogOpen] = React.useState<boolean>(false);
    const [isVerifyDialogOpen, setIsVerifyDialogOpen] = React.useState<boolean>(false);

    const history = useHistory();
    const location = useLocation();
    const { refreshSeasons } = useSeasonGroupManager();

    React.useEffect(() => {
        history.push('/export');
        //history.goBack();
    }, []);

    React.useEffect(() => {
        console.log(location);
    }, [location]);

    const buildImportTypeItems = () => {
        return [dataImportOption, feesImportOption];
    };

    const onImportButtonClicked = () => {
        setIsImportTypeDialogOpen(true);
    };

    const onImportTypeSelected = (selectedValue?: string) => {
        setIsImportTypeDialogOpen(false);
        setIsImportFeesDialogOpen(false);

        if (selectedValue === dataImportOption.key) {
            setImportDialogOpen(true);
        } else if (selectedValue === feesImportOption.key) {
            setIsImportFeesDialogOpen(true);
        }
    };

    const onGenerateReports = () => {
        history.push('/export');
    };

    const onPrintButtonClicked = () => {
        setIsVerifyDialogOpen(true);
    };

    const onCancelPrintClicked = () => {
        setIsPrintActive(false);
    };

    const onContinuePrint = () => {
        setIsVerifyDialogOpen(false);
        setIsPrintActive(true);
    };

    return (
        <>
            <ConfigurationManager />
            <ImportDataDialog
                show={isImportDialogOpen}
                onDialogClosed={() => {
                    setImportDialogOpen(false);
                    refreshSeasons();
                }}
            />
            <ImportFeesDialog
                isOpen={isImportFeesDialogOpen}
                onClose={() => {
                    setIsImportFeesDialogOpen(false);
                }}
            />
            <VerifyDataDialog
                isOpen={isVerifyDialogOpen}
                onClose={() => setIsVerifyDialogOpen(false)}
                onContinue={onContinuePrint}
            />
            <SnackbarProvider maxSnack={4}>
                <div>
                    <Suspense fallback={() => <Backdrop open={true} />}>
                        <ApplicationToolbar
                            onImportButtonClicked={onImportButtonClicked}
                            onPrintButtonClicked={onPrintButtonClicked}
                            onGenerateReportsButtonClicked={onGenerateReports}
                        />
                    </Suspense>
                    <Switch>
                        <Route path={'/export'}>
                            <Suspense fallback={<CircularProgress />}>
                                <PrintContainer
                                    print={isPrintActive}
                                    onPrintFinished={onCancelPrintClicked}
                                >
                                    <ClientReport />
                                </PrintContainer>
                            </Suspense>
                        </Route>
                        <Route path={'/'}>
                            <Suspense fallback={<CircularProgress />}>
                                <ClientEditingPage />
                            </Suspense>
                        </Route>
                    </Switch>
                    <SimpleDialogList
                        isOpen={isImportTypeDialogOpen}
                        items={buildImportTypeItems()}
                        onClose={onImportTypeSelected}
                    />
                </div>
            </SnackbarProvider>
        </>
    );
}

export default App;
