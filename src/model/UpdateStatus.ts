export enum UpdateStatusType {
    CheckingForUpdates,
    DownloadProgress,
    RestartingToUpdate,
    UpdateAvailable,
    UpdateDownloading,
    UpdateDownloaded,
    UpdateError,
    UpdateNotAvailble,
}

export interface IUpdateStatus {
    type: UpdateStatusType;
    message: string;
    downloadProgress?: number;
}
