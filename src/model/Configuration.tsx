export type ClientIDType = number;

interface IUniqueIdentifier {
    id?: number;
}

export interface IClientInfo extends IUniqueIdentifier {
    name: string;
}

export interface IDatedValue {
    date: Date;
    value: number;
}

export interface IDataPoint {
    label: string | number;
    value: number;
}

export interface IAdrData extends IUniqueIdentifier {
    clientId?: ClientIDType;
    year: number;
    monthData: IDatedValue[];
}

export interface IOccupancyData extends IUniqueIdentifier {
    clientId?: ClientIDType;
    year: number;
    occupancyData: IDatedValue[];
}

export interface IClientFee extends IUniqueIdentifier, IClientFeeDescription {
    clientId: ClientIDType;
    descriptionId?: number;
    name: string;
    value: number;
}

export interface IRevenueData extends IUniqueIdentifier {
    clientId?: ClientIDType;
    year: number;
    revenueData: IDatedValue[];
}

export interface IClientFeeDescription extends IUniqueIdentifier {
    description?: string;
}

export interface IApplicationConfiguration {
    programVersion: string;
}

export interface IDiagnosticsData {
    appData: IApplicationConfiguration;
    selectedClientId: number;
    clients: IClientInfo[];
    fees: IClientFee[];
    adrData: IAdrData[];
}

export interface ISeasonalRate {
    seasonId: number;
    rate: number;
}

export interface IDailyRatesData extends IUniqueIdentifier {
    clientId?: ClientIDType;
    rates: ISeasonalRate[];
}

export interface ISeasonGroup extends IUniqueIdentifier {
    name: string;
    assignedClientIds: ClientIDType[];
    startDate: Date;
    endDate: Date;
    minimumStayLength: number;
}
