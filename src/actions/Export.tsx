import { remote } from 'electron';
import fs from 'fs';

export const getSaveDirectory = async (): Promise<string> => {
    const { BrowserWindow, dialog } = remote;

    const result = await dialog.showOpenDialog({
        title: 'Select a directory to save your reports',
        properties: ['openDirectory'],
    });

    if (result.filePaths.length) {
        return result.filePaths[0];
    }

    return '';
};

export const printToPdf = async (fileSaveLocation: string): Promise<boolean | string> => {
    const { BrowserWindow, dialog } = remote;
    const allWindows = BrowserWindow.getAllWindows();

    if (allWindows.length) {
        const printResult = await allWindows[0].webContents.printToPDF({
            landscape: false,
            pageSize: 'Letter',
            marginsType: 0,
            printBackground: true,
        });

        fs.writeFileSync(fileSaveLocation, printResult);
    } else {
        return false;
    }

    return true;
};
