import { atom, useAtom } from 'jotai';

import {
    IClientInfo,
    IApplicationConfiguration,
    IClientFee,
    IAdrData,
    IDiagnosticsData,
    IOccupancyData,
    IRevenueData,
    IDailyRatesData,
    ISeasonGroup,
} from '../model/Configuration';

import packageInfo from '../../package.json';
import { db } from './ApplicationDatabase';

export const PROGRAM_VERSION = packageInfo.version;

export const configurationAtom = atom<IApplicationConfiguration>({
    programVersion: PROGRAM_VERSION,
});

interface ISettingsControls {
    isSettingsOpen: boolean;
    activePage: any;
}

export const settingsControlAtom = atom({
    isSettingsOpen: false,
    activePage: 0,
});

const fetchClients = async () => {
    return db.clients.toArray();
};

const fetchFees = async (clientId: number) => {
    const fees = await db.fees.where('clientId').anyOf([clientId]).toArray();

    /*return Promise.all(
        fees.map(async (fee) => {
            let description = '';
            if (fee.descriptionId) {
                const feeDescription = await db.feeDescriptions
                    .where('id')
                    .equals(fee.descriptionId)
                    .first();
                description = feeDescription?.description ?? '';
            }

            return {
                ...fee,
                description: description,
            } as IClientFee;
        })
    );*/
    return fees ?? [];
};

const fetchSeasonGroups = async () => {
    return db.seasonGroups.toArray();
};

const fetchAdr = async (clientId: number) => {
    return db.adrData.where('clientId').equals(clientId).toArray();
};

const fetchOccupancy = async (clientId: number) => {
    return db.occupancyData.where('clientId').equals(clientId).toArray();
};

const fetchRevenue = async (clientId: number) => {
    return db.revenueData.where('clientId').equals(clientId).toArray();
};

const fetchDailyRates = async (clientId: number) => {
    return db.dailyRatesData.where('clientId').equals(clientId).first();
};

const initialClientsAtom = atom<IClientInfo[]>(fetchClients);
const updatedClientsAtom = atom<IClientInfo[] | null>(null);
const refreshClientsAtom = atom(null, async (get, set) => {
    set(updatedClientsAtom, await fetchClients());
});

const clientsAtom = atom((get) => {
    return get(updatedClientsAtom) || get(initialClientsAtom);
});

const clientAtom = atom<IClientInfo | null>(null);
export const selectedClientAtom = atom<IClientInfo | null, number | undefined>(
    (get: any) => {
        return get(clientAtom);
    },
    async (get, set, update) => {
        if (update) {
            const client = await db.clients.where('id').equals(update).first();
            if (client) {
                set(clientAtom, client);
            }
        } else {
            set(clientAtom, null);
        }

        set(selectedClientFeesAtom, null);
        set(selectedClientAdrAtom, null);
        set(selectedClientOccupancyAtom, null);
        set(selectedClientRevenueAtom, null);
        set(selectedClientDailyRatesAtom, null);
    }
);

const feesAtom = atom<IClientFee[]>([]);
const selectedClientFeesAtom = atom<IClientFee[], unknown>(
    (get) => {
        return get(feesAtom);
    },
    async (get, set, update) => {
        const currentClient = await get(selectedClientAtom);
        if (currentClient && currentClient.id) {
            set(feesAtom, await fetchFees(currentClient.id));
        } else {
            return set(feesAtom, []);
        }
    }
);

const initialSeasonGroupsAtom = atom<ISeasonGroup[]>(fetchSeasonGroups);
const updatedSeasonGroupsAtom = atom<ISeasonGroup[] | null>(null);
const refreshSeasonGroups = atom(null, async (get, set) => {
    set(updatedSeasonGroupsAtom, await fetchSeasonGroups());
});

const seasonGroupsAtom = atom((get) => {
    return get(updatedSeasonGroupsAtom) || get(initialSeasonGroupsAtom);
});

const adrAtom = atom<IAdrData[]>([]);
export const selectedClientAdrAtom = atom<IAdrData[], null>(
    (get) => {
        return get(adrAtom);
    },
    async (get, set, update) => {
        const currentClient = await get(selectedClientAtom);
        if (currentClient && currentClient.id) {
            set(adrAtom, await fetchAdr(currentClient.id));
        } else {
            return set(adrAtom, []);
        }
    }
);

const occupancyAtom = atom<IOccupancyData[]>([]);
export const selectedClientOccupancyAtom = atom<IOccupancyData[], null>(
    (get) => {
        return get(occupancyAtom);
    },
    async (get, set, update) => {
        const currentClient = await get(selectedClientAtom);
        if (currentClient && currentClient.id) {
            set(occupancyAtom, await fetchOccupancy(currentClient.id));
        } else {
            return set(occupancyAtom, []);
        }
    }
);

const revenueAtom = atom<IRevenueData[]>([]);
export const selectedClientRevenueAtom = atom<IRevenueData[], null>(
    (get) => {
        return get(revenueAtom);
    },
    async (get, set, update) => {
        const currentClient = await get(selectedClientAtom);
        if (currentClient && currentClient.id) {
            set(revenueAtom, await fetchRevenue(currentClient.id));
        } else {
            return set(revenueAtom, []);
        }
    }
);

const dailyRatesAtom = atom<IDailyRatesData | undefined>(undefined);
export const selectedClientDailyRatesAtom = atom<IDailyRatesData | undefined, null>(
    (get) => {
        return get(dailyRatesAtom);
    },
    async (get, set, update) => {
        const currentClient = await get(selectedClientAtom);
        if (currentClient && currentClient.id) {
            set(dailyRatesAtom, await fetchDailyRates(currentClient.id));
        } else {
            return set(dailyRatesAtom, undefined);
        }
    }
);

export const useClientManager = () => {
    const [clientList] = useAtom(clientsAtom);
    const [, refreshClients] = useAtom(refreshClientsAtom);

    const addClient = async (client: IClientInfo): Promise<number> => {
        const matchingClient = await db.clients.where('name').equals(client.name).first();

        if (matchingClient && matchingClient.id) {
            return matchingClient.id;
        } else {
            const clientId = await db.clients.put({ ...client, id: undefined });
            refreshClients();

            return clientId;
        }
    };

    const addClientBulk = async (clients: IClientInfo[]): Promise<number[]> => {
        const ids = await db.clients.bulkPut(
            clients.map((client) => {
                return {
                    ...client,
                    id: undefined,
                };
            }),
            {
                allKeys: true,
            }
        );

        refreshClients();
        return ids;
    };

    const deleteClient = async (clientId: number): Promise<number> => {
        db.clients.delete(clientId);
        db.fees.where('clientId').equals(clientId).delete();
        db.adrData.where('clientId').equals(clientId).delete();

        refreshClients();

        return clientId;
    };

    return {
        clientList: clientList,
        addClient: addClient,
        deleteClient: deleteClient,
        addClientsBulk: addClientBulk,
        refreshClients: refreshClients,
    } as const;
};

export const useFeeManager = () => {
    const [feeList, refreshFees] = useAtom(selectedClientFeesAtom);

    const addFee = async (client: IClientInfo, fee: IClientFee): Promise<number> => {
        let newDescriptionId = undefined;

        if (client.id === undefined) {
            return -1;
        }

        if (fee.description) {
            if (fee.descriptionId) {
                newDescriptionId = await db.feeDescriptions.update(fee.descriptionId, {
                    description: fee.description,
                });
            } else {
                newDescriptionId = await db.feeDescriptions.put({
                    id: undefined,
                    description: fee.description,
                });
            }
        }

        const existingFee = await db.fees
            .where('name')
            .equals(fee.name)
            .and((x) => {
                return x.clientId === client.id;
            })
            .first();

        if (existingFee && existingFee.id) {
            const feeId = await db.fees.update(existingFee.id, {
                ...existingFee,
                value: fee.value,
            });

            refreshFees();

            return feeId;
        } else {
            const feeId = db.fees.put({
                ...fee,
                id: undefined,
                descriptionId: newDescriptionId,
                description: undefined,
                clientId: client.id,
            });

            refreshFees();

            return feeId;
        }
    };

    const updateFee = async (client: IClientInfo, fee: IClientFee): Promise<void> => {
        let newDescriptionId = undefined;

        if (fee.description) {
            if (fee.descriptionId) {
                newDescriptionId = await db.feeDescriptions.update(fee.descriptionId, {
                    description: fee.description,
                });
            } else {
                newDescriptionId = await db.feeDescriptions.put({
                    id: undefined,
                    description: fee.description,
                });
            }
        }

        if (fee.id) {
            db.fees.update(fee.id, {
                ...fee,
                descriptionId: newDescriptionId,
                description: undefined,
            });
        }

        refreshFees();
    };

    const deleteFee = async (clientId: number, feeId: number): Promise<number> => {
        const matchingFee = await db.fees.where('id').equals(feeId).first();
        if (matchingFee && matchingFee.id) {
            await db.fees.delete(matchingFee.id);

            if (matchingFee.descriptionId) {
                await db.feeDescriptions.delete(matchingFee.descriptionId);
            }
        }

        refreshFees();

        return feeId;
    };

    return {
        feeList: feeList,
        addFee: addFee,
        updateFee: updateFee,
        deleteFee: deleteFee,
        refreshFees: refreshFees,
    } as const;
};

export const useSeasonGroupManager = () => {
    const [seasonGroupsList] = useAtom(seasonGroupsAtom);
    const [, refreshSeasons] = useAtom(refreshSeasonGroups);

    const addSeasonGroup = async (seasonGroup: ISeasonGroup): Promise<number> => {
        const existingSeason = await db.seasonGroups.where('name').equals(seasonGroup.name).first();
        if (existingSeason) {
            // Return the existing season. Duplicate season names are not allowed.
            return existingSeason.id ?? -1;
        }

        const seasonGroupId = await db.seasonGroups.put({
            ...seasonGroup,
            id: undefined,
            assignedClientIds: [],
        });
        await refreshSeasons();

        return seasonGroupId;
    };

    const deleteSeasonGroup = async (seasonGroupId: number): Promise<number> => {
        await db.seasonGroups.delete(seasonGroupId);
        await refreshSeasons();

        return seasonGroupId;
    };

    const updateSeasonGroup = async (seasonGroup: ISeasonGroup): Promise<number> => {
        if (!seasonGroup.id) {
            return -1;
        }

        await db.seasonGroups.update(seasonGroup.id, {
            ...seasonGroup,
        });

        await refreshSeasons();

        return seasonGroup.id;
    };

    const addClientToGroup = async (seasonGroup: ISeasonGroup, clientId: number): Promise<void> => {
        if (!seasonGroup.id) {
            return;
        }

        await db.seasonGroups.update(seasonGroup.id, {
            ...seasonGroup,
            assignedClientIds: [
                ...seasonGroup.assignedClientIds.filter((id) => id !== clientId),
                clientId,
            ],
        });

        await refreshSeasons();
    };

    const removeClientFromGroup = async (
        seasonGroup: ISeasonGroup,
        clientId: number
    ): Promise<void> => {
        if (!seasonGroup.id) {
            return;
        }

        await db.seasonGroups.update(seasonGroup.id, {
            ...seasonGroup,
            assignedClientIds: seasonGroup.assignedClientIds.filter((id) => id !== clientId),
        });

        await refreshSeasons();
    };

    return {
        addClientToGroup: addClientToGroup,
        addSeasonGroup: addSeasonGroup,
        deleteSeasonGroup: deleteSeasonGroup,
        removeClientFromGroup: removeClientFromGroup,
        seasonGroupList: seasonGroupsList,
        updateSeasonGroup: updateSeasonGroup,
        refreshSeasons: refreshSeasons,
    } as const;
};

export const clearAllData = async (): Promise<void> => {
    await db.delete();
};

export const getUsageSizeBytes = async (): Promise<number | undefined> => {
    const storage = await navigator.storage.estimate();
    // @ts-ignore
    const usageDetails = storage.usageDetails;

    if (usageDetails) {
        return usageDetails.indexedDB ?? storage.usage;
    }

    return storage.usage;
};

export const getDiagnosticsData = async (): Promise<IDiagnosticsData> => {
    const [clients, fees, adrData] = await Promise.all([
        db.clients.toArray(),
        db.fees.toArray(),
        db.adrData.toArray(),
    ]);

    return {
        clients: clients,
        fees: fees,
        adrData: adrData,
        appData: {
            programVersion: PROGRAM_VERSION,
        },
        selectedClientId: -1,
    };
};
