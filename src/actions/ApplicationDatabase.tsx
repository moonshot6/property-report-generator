import Dexie from 'dexie';

import {
    IClientInfo,
    IClientFee,
    IAdrData,
    IOccupancyData,
    IClientFeeDescription,
    IRevenueData,
    IDailyRatesData,
    ISeasonGroup,
} from '../model/Configuration';

class PropertyReportGeneratorDatabase extends Dexie {
    clients: Dexie.Table<IClientInfo, number>;
    fees: Dexie.Table<IClientFee, number>;
    feeDescriptions: Dexie.Table<IClientFeeDescription, number>;
    adrData: Dexie.Table<IAdrData, number>;
    occupancyData: Dexie.Table<IOccupancyData, number>;
    revenueData: Dexie.Table<IRevenueData, number>;
    dailyRatesData: Dexie.Table<IDailyRatesData, number>;
    seasonGroups: Dexie.Table<ISeasonGroup, number>;

    constructor() {
        super('PropertyReportGeneratorDatabase');

        //
        // Define tables and indexes
        // (Here's where the implicit table props are dynamically created)
        //
        this.version(1).stores({
            clients: '++id, name',
            fees: '++id, clientId, descriptionId, name',
            feeDescriptions: '++id',
            adrData: '++id, clientId, year',
            occupancyData: '++id, clientId, year',
            revenueData: '++id, clientId, year',
            dailyRatesData: '++id, clientId',
            seasonGroups: '++id, name, assignedClientIds',
        });

        // The following lines are needed for it to work across typescript using babel-preset-typescript:
        this.clients = this.table('clients');
        this.fees = this.table('fees');
        this.feeDescriptions = this.table('feeDescriptions');
        this.seasonGroups = this.table('seasonGroups');

        this.adrData = this.table('adrData');
        this.occupancyData = this.table('occupancyData');
        this.revenueData = this.table('revenueData');
        this.dailyRatesData = this.table('dailyRatesData');
    }
}

export const db = new PropertyReportGeneratorDatabase();
