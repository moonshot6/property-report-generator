import parser from 'csv-parse/lib/sync';
import { IDatedValue } from '../model/Configuration';
import { ColumnOption } from 'csv-parse';

export interface IAverageRatesFileData {
    unit: string;
    adr: number;
    fees: IDatedValue[];
    adjustedPaidOccupancy: number;
    adjustedRevpar: number;
    guestCheckins: number;
    ownerCheckins: number;
}

export interface IOccupancyFileData {
    unit: string;
    adjustedPaidOccupancy: number;
    occupancy: IDatedValue[];
}

export const readAverageRatesFile = async (
    file: File,
    year: number
): Promise<IAverageRatesFileData[]> => {
    return new Promise<IAverageRatesFileData[]>(async (resolve, reject) => {
        try {
            const rawCsvText = await file.text();

            const rows = parser(rawCsvText, {
                columns: (header) => {
                    let fields = header;

                    for (let i = 0; i < fields.length; ++i) {
                        fields[i] = fields[i].toLowerCase();
                        fields[i] = fields[i].replace(/[ ]([\w])/g, (match: any, p1: any) => {
                            return p1.toUpperCase();
                        });
                        fields[i] = fields[i].replace(/[% ]/g, '');
                    }

                    return fields;
                },
                onRecord: (record: any) => {
                    const dataPoints: IDatedValue[] = [];

                    if (
                        record.unit === '' ||
                        record.unit === '[object Object]' ||
                        record.unit.includes('Source: Key Data')
                    ) {
                        return null;
                    }

                    try {
                        Object.keys(record).forEach((key) => {
                            const parsedDate = Date.parse(key);

                            if (!Number.isNaN(parsedDate)) {
                                // If the value assigned to this date is a percent, this is NOT an ADR file
                                if (!record[key].includes('$')) {
                                    throw 'Unexpected data format';
                                }

                                const date = new Date(parsedDate);
                                date.setFullYear(year);

                                dataPoints.push({
                                    date: date,
                                    value: Number.parseFloat(record[key].replace(/[$,]/g, '')),
                                });
                            }
                        });
                    } catch (exception) {
                        return null;
                    }

                    return {
                        unit: record.unit,
                        adr: Number.parseFloat(record.adr.replace(/[$,]/g, '')),
                        adjustedPaidOccupancy: Number.parseFloat(record.adjustedPaidOccupancy),
                        adjustedRevpar: Number.parseFloat(record.adr.replace(/[$,]/g, '')),
                        guestCheckins: Number.parseFloat(record.guestCheckins),
                        ownerCheckins: Number.parseFloat(record.ownerCheckins),
                        fees: dataPoints,
                    } as IAverageRatesFileData;
                },
                autoParseDate: true,
                skip_empty_lines: true,
                columns_duplicates_to_array: true,
            });

            const parsedData: IAverageRatesFileData[] = rows;

            if (parsedData.length === 0) return resolve([]);
            if (parsedData[0].adr === undefined || parsedData[0].unit === undefined) resolve([]);

            resolve(parsedData);
        } catch (e) {
            reject('The data provided could not be parsed.');
        }
    });
};

export const readOccupancyFile = async (
    file: File,
    year: number
): Promise<IOccupancyFileData[]> => {
    return new Promise<IOccupancyFileData[]>(async (resolve, reject) => {
        try {
            const rawCsvText = await file.text();

            const rows = parser(rawCsvText, {
                columns: (header) => {
                    let fields = header;

                    for (let i = 0; i < fields.length; ++i) {
                        fields[i] = fields[i].toLowerCase();
                        fields[i] = fields[i].replace(/[ ]([\w])/g, (match: any, p1: any) => {
                            return p1.toUpperCase();
                        });
                        fields[i] = fields[i].replace(/[% ]/g, '');
                    }

                    return fields;
                },
                onRecord: (record: any) => {
                    const dataPoints: IDatedValue[] = [];

                    if (
                        record.unit === '' ||
                        record.unit === '[object Object]' ||
                        record.unit.includes('Source: Key Data')
                    ) {
                        return null;
                    }

                    try {
                        Object.keys(record).forEach((key) => {
                            const parsedDate = Date.parse(key);

                            if (!Number.isNaN(parsedDate)) {
                                // If the value assigned to this date is a dollar amount, this is NOT an occupancy file
                                if (!record[key].includes('%')) {
                                    throw 'Unexpected data format';
                                }

                                const date = new Date(parsedDate);
                                date.setFullYear(year);

                                dataPoints.push({
                                    date: date,
                                    value: Number.parseFloat(record[key].replace(/%/, '')) / 100.0,
                                });
                            }
                        });
                    } catch (exception) {
                        return null;
                    }

                    return {
                        unit: record.unit,
                        adjustedPaidOccupancy: Number.parseFloat(record.adjustedPaidOccupancy),
                        occupancy: dataPoints,
                    } as IOccupancyFileData;
                },
                autoParseDate: true,
                skip_empty_lines: true,
                columns_duplicates_to_array: true,
            });

            const parsedData: IOccupancyFileData[] = rows;

            if (parsedData.length === 0) return resolve([]);
            if (parsedData[0].occupancy === undefined || parsedData[0].unit === undefined)
                resolve([]);

            resolve(parsedData);
        } catch (e) {
            reject('The data provided could not be parsed.');
        }
    });
};

export interface IDailyRates {
    name: string;
}

export interface IParsedFee {
    name: string;
}

const sumReducer = (accumulator: number, currentValue: string) => {
    let result = 0;

    if (currentValue.includes('$')) {
        result = Number.parseFloat(currentValue.replace(/[$,]/g, ''));
    }

    return accumulator + result;
};

const averageCurrencyValues = (values: string[] | string | undefined): number | undefined => {
    if (values === undefined) {
        return undefined;
    }

    if (Array.isArray(values)) {
        return values.reduce(sumReducer, 0) / values.length;
    } else {
        return Number.parseFloat(values.replace(/[$,]/g, ''));
    }
};

/**
 *  Reads a CSV file provided and generates an initial sample record to verify that fields are mapped properly.
 * @param file The file handle to read
 * @param onColumnsReady A callback issued after the file is initially read. The callback will provide the names of the columns obtained as well as the sample format to use when returning data.
 */
export const readDailyRatesFile = (files: File[]): Promise<IDailyRates[]> => {
    return new Promise<IDailyRates[]>(async (resolve, reject) => {
        try {
            const allRecords: IDailyRates[] = [];

            for (const file of files) {
                const rawCsvText = await file.text();
                allRecords.push(...parseSeasonalRates(rawCsvText));
            }

            resolve(allRecords);
        } catch (e) {
            reject('The data provided could not be parsed.');
        }
    });
};

const parseSeasonalRates = (csvText: string, fromLine?: number, toLine?: number): IDailyRates[] => {
    const filterObject = (obj: object, predicate: (value: any) => boolean) => {
        let result = {},
            key;

        for (key in obj) {
            // @ts-ignore
            if (obj.hasOwnProperty(key) && !predicate(key)) {
                // @ts-ignore
                result[key] = obj[key];
            }
        }

        return result;
    };

    return parser(csvText, {
        columns: (headers: any) => {
            let columns = headers as string[];

            if (columns.length >= 1) {
                columns[0] = 'unitName';
            }
            // Consolidate the similar columns into the groups identified in IDailyRates
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i] !== '') {
                    let j = i + 1;
                    while (j < columns.length && columns[j] === '') {
                        // The next column is most likely the same season as the last. Group it with this one.
                        columns[j] = columns[i];
                        ++j;
                    }
                }
            }

            return columns;
        },
        onRecord: (record: any, context) => {
            if (record.unitName === '') {
                return undefined;
            }

            const recordValues = Object.values(record);

            if (recordValues.includes('Sun-Thu') || recordValues.includes('Fri-Sat')) {
                // Skip the secondary column line that groups rates into weekly groups
                return undefined;
            }
            if (recordValues.includes('Standard')) {
                // Skip the fees if they are grouped in this document
                return undefined;
            }

            return {
                name: record.unitName,
                ...filterObject(
                    record,
                    (value) =>
                        value.includes('Pricing Logic') ||
                        value.includes('Rate Type') ||
                        value.includes('unitName')
                ),
            };
        },
        autoParseDate: true,
        skip_empty_lines: true,
        columns_duplicates_to_array: true,
        from_line: fromLine,
        to_line: toLine,
        trim: true,
    });
};

/**
 *  Reads a CSV file provided and generates an initial sample record to verify that fields are mapped properly.
 * @param file The file handle to read
 * @param onColumnsReady A callback issued after the file is initially read. The callback will provide the names of the columns obtained as well as the sample format to use when returning data.
 */
export const readFeesFile = (files: File[]): Promise<IParsedFee[]> => {
    return new Promise<IParsedFee[]>(async (resolve, reject) => {
        try {
            const allRecords: IParsedFee[] = [];

            for (const file of files) {
                const rawCsvText = await file.text();
                allRecords.push(...parseFees(rawCsvText));
            }

            resolve(allRecords);
        } catch (e) {
            reject('The data provided could not be parsed.');
        }
    });
};

const parseFees = (csvText: string, fromLine?: number, toLine?: number): IParsedFee[] => {
    const filterObject = (obj: object, predicate: (value: any) => boolean) => {
        let result = {},
            key;

        for (key in obj) {
            // @ts-ignore
            if (obj.hasOwnProperty(key) && !predicate(key)) {
                // @ts-ignore
                result[key] = obj[key];
            }
        }

        return result;
    };

    return parser(csvText, {
        columns: (headers: any) => {
            let columns = headers as string[];

            if (columns.length >= 1) {
                columns[0] = 'unitName';
            }
            // Consolidate the similar columns into the groups identified in IDailyRates
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i] !== '') {
                    let j = i + 1;
                    while (j < columns.length && columns[j] === '') {
                        // The next column is most likely the same season as the last. Group it with this one.
                        columns[j] = columns[i];
                        ++j;
                    }
                }
            }

            return columns;
        },
        onRecord: (record: any, context) => {
            if (record.unitName === '') {
                return undefined;
            }

            const recordValues = Object.values(record);

            if (recordValues.includes('Sun-Thu') || recordValues.includes('Fri-Sat')) {
                // Skip the secondary column line that groups rates into weekly groups
                return undefined;
            }
            if (recordValues.includes('Standard')) {
                // Skip the fees if they are grouped in this document
                return undefined;
            }

            return {
                name: record.unitName,
                ...filterObject(
                    record,
                    (value) =>
                        value.includes('Pricing Logic') ||
                        value.includes('Rate Type') ||
                        value.includes('unitName')
                ),
            };
        },
        autoParseDate: true,
        skip_empty_lines: true,
        columns_duplicates_to_array: true,
        from_line: fromLine,
        to_line: toLine,
        trim: true,
    });
};
