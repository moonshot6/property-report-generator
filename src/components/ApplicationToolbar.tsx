import React from 'react';
import appStyles from '../App.module.css';
import { AppBar, Button, IconButton, Toolbar, Typography } from '@material-ui/core';
import { ArrowBack as ArrowBackIcon, Print as PrintIcon } from '@material-ui/icons';
import { ApplicationSettings } from './settings/ApplicationSettings';
import { useHistory, useLocation } from 'react-router-dom';
import { useAtom } from 'jotai';
import { selectedClientAtom, useClientManager } from 'actions/Configuration';

interface IApplicationToolbarProps {
    onImportButtonClicked: () => void;
    onPrintButtonClicked: () => void;
    onGenerateReportsButtonClicked: () => void;
}

export const ApplicationToolbar: React.FC<IApplicationToolbarProps> = (props) => {
    const history = useHistory();
    const location = useLocation();
    const { clientList } = useClientManager();

    const onShowReportGenerator = () => {
        props.onGenerateReportsButtonClicked();
    };

    const onBackButtonClicked = () => {
        if (history.length > 1) {
            history.goBack();
        } else {
            history.push('/');
        }
    };

    const styles =
        location.pathname !== '/'
            ? {
                  paddingLeft: '2px',
              }
            : {};

    return (
        <div className={appStyles.hideContent}>
            <AppBar
                position='sticky'
                style={{
                    marginBottom: '10px',
                }}
            >
                <Toolbar style={styles}>
                    {location.pathname !== '/' && (
                        <IconButton
                            onClick={onBackButtonClicked}
                            style={{
                                color: 'white',
                            }}
                        >
                            <ArrowBackIcon />
                        </IconButton>
                    )}
                    <Typography variant='h6' style={{ flexGrow: 1 }}>
                        Property Report Generator
                    </Typography>
                    {location.pathname === '/' && (
                        <>
                            <Button color='inherit' onClick={props.onImportButtonClicked}>
                                Import Data
                            </Button>
                            <Button
                                color='inherit'
                                onClick={onShowReportGenerator}
                                disabled={clientList.length === 0}
                            >
                                Generate Reports
                            </Button>
                            <ApplicationSettings />
                        </>
                    )}
                    {location.pathname === '/export' && (
                        <IconButton onClick={props.onPrintButtonClicked}>
                            <PrintIcon
                                style={{
                                    color: 'white',
                                }}
                            />
                        </IconButton>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
};
