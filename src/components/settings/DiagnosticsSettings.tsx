import { Button, Grid, TextField, Tooltip, Zoom } from '@material-ui/core';
import { BugReport as BugReportIcon, Assignment as CopyButtonIcon } from '@material-ui/icons';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import React from 'react';
import { getDiagnosticsData } from 'actions/Configuration';

import { useSnackbar } from 'notistack';

export const DiagnosticsSettings: React.FC = (props) => {
    const [configRaw, setConfigJson] = React.useState<string | undefined>(undefined);
    const { enqueueSnackbar } = useSnackbar();

    const onGetDianosticsData = () => {
        getDiagnosticsData().then((currentConfig) => {
            setConfigJson(JSON.stringify(currentConfig, null, '  '));
        });
    };

    return (
        <Grid spacing={3}>
            <Grid
                item
                style={{
                    marginBottom: 20,
                }}
            >
                <Button
                    variant='contained'
                    color='secondary'
                    startIcon={<BugReportIcon />}
                    onClick={onGetDianosticsData}
                >
                    Copy Diagnostics Data
                </Button>
            </Grid>
            {configRaw && (
                <Grid
                    item
                    style={{
                        position: 'relative',
                    }}
                >
                    <TextField
                        fullWidth={true}
                        id='outlined-multiline-static'
                        multiline
                        rows={4}
                        value={configRaw}
                        disabled={true}
                        variant='outlined'
                        aria-selected={true}
                    />
                    <Tooltip
                        title={'Click to copy'}
                        arrow={true}
                        placement={'left'}
                        TransitionComponent={Zoom}
                    >
                        <CopyToClipboard
                            text={configRaw}
                            onCopy={() => {
                                enqueueSnackbar('Text copied to clipboard', {
                                    variant: 'success',
                                    anchorOrigin: {
                                        vertical: 'bottom',
                                        horizontal: 'right',
                                    },
                                });
                            }}
                        >
                            <Button
                                variant={'contained'}
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                    top: 0,
                                }}
                            >
                                <CopyButtonIcon />
                            </Button>
                        </CopyToClipboard>
                    </Tooltip>
                </Grid>
            )}
        </Grid>
    );
};
