import React from 'react';
import { clearAllData, getUsageSizeBytes } from 'actions/Configuration';
import {
    Badge,
    Button,
    CircularProgress,
    Dialog,
    DialogContent,
    DialogActions,
    DialogTitle,
    Grid,
    Typography,
} from '@material-ui/core';
import { Delete as DeleteIcon } from '@material-ui/icons';

export const DataCachingSettings: React.FC = (props) => {
    const [isConfirmDialogOpen, setIsConfirmDialogOpen] = React.useState(false);
    const [isClearingData, setIsClearingData] = React.useState(false);
    const [configSize, setConfigSize] = React.useState<string | undefined>(undefined);

    const onClearApplicationDataClicked = () => {
        setIsConfirmDialogOpen(true);
    };

    const onClearConfirmed = async () => {
        try {
            setIsClearingData(true);
            await clearAllData();
            window.location.reload();
        } catch (e) {
            setIsClearingData(false);
            window.alert('Unable to clear data: ' + e);
        }
    };

    React.useEffect(() => {
        getUsageSizeBytes().then((usageSizeBytes) => {
            if (usageSizeBytes) {
                if (usageSizeBytes < 1000) {
                    setConfigSize(`${usageSizeBytes} B`);
                } else if (usageSizeBytes > 1000 && usageSizeBytes < 1000000) {
                    setConfigSize(`${usageSizeBytes / 1000} KB`);
                } else {
                    setConfigSize(`${usageSizeBytes / 1000000} MB`);
                }
            } else {
                setConfigSize('unknown');
            }
        });
    }, []);

    return (
        <Grid spacing={3}>
            <Grid item>
                <Grid>
                    <Badge badgeContent={configSize} color='primary'>
                        <Button
                            variant='contained'
                            color='secondary'
                            startIcon={<DeleteIcon />}
                            onClick={onClearApplicationDataClicked}
                        >
                            Reset All Application Data
                        </Button>
                    </Badge>
                </Grid>
            </Grid>

            <Dialog
                open={isConfirmDialogOpen}
                onClose={() => setIsConfirmDialogOpen(false)}
                onEscapeKeyDown={() => setIsConfirmDialogOpen(false)}
                onBackdropClick={() => setIsConfirmDialogOpen(false)}
                aria-labelledby='max-width-dialog-title'
            >
                <DialogTitle id='max-width-dialog-title'>Confirm Action</DialogTitle>
                <DialogContent>
                    <Typography variant={'body1'}>
                        Are you sure you want to clear all data?
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant={'outlined'}
                        onClick={() => setIsConfirmDialogOpen(false)}
                        autoFocus={true}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant={'contained'}
                        onClick={onClearConfirmed}
                        disabled={isClearingData}
                    >
                        {isClearingData ? <CircularProgress size={40} /> : 'Yes'}
                    </Button>
                </DialogActions>
            </Dialog>
        </Grid>
    );
};
