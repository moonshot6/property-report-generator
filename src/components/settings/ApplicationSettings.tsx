import {
    Box,
    Dialog,
    DialogContent,
    DialogTitle,
    IconButton,
    Tab,
    Tabs,
    Typography,
} from '@material-ui/core';
import { Settings as SettingsIcon, Close as CloseIcon } from '@material-ui/icons';
import React from 'react';
import { DataCachingSettings } from './DataCachingSettings';
import { DiagnosticsSettings } from './DiagnosticsSettings';
import { SeasonGroupSettings } from './SeasonGroupsSettings';
import { useAtom } from 'jotai';
import { settingsControlAtom } from 'actions/Configuration';

interface ITabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

const TabPanel: React.FC<ITabPanelProps> = (props) => {
    const { children, value, index, ...other } = props;

    return (
        <div
            role='tabpanel'
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            style={{
                flexGrow: 1,
            }}
            {...other}
        >
            {value === index && (
                <Box paddingLeft={3} paddingRight={3} paddingBottom={3}>
                    {children}
                </Box>
            )}
        </div>
    );
};

const a11yProps = (index: any) => {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
};

export const ApplicationSettings = () => {
    const [appSettings, setAppSettings] = useAtom(settingsControlAtom);

    const closeSettings = () => {
        setAppSettings({
            ...appSettings,
            isSettingsOpen: false,
        });
    };

    const onSettingsButtonClicked = () => {
        setAppSettings({
            ...appSettings,
            isSettingsOpen: true,
        });
    };

    const onTabChanged = (event: React.ChangeEvent<{}>, newValue: number) => {
        setAppSettings({
            ...appSettings,
            activePage: newValue,
        });
    };

    return (
        <>
            <IconButton onClick={onSettingsButtonClicked}>
                <SettingsIcon
                    style={{
                        color: 'white',
                    }}
                />
            </IconButton>
            <Dialog
                fullWidth={true}
                maxWidth={'md'}
                open={appSettings.isSettingsOpen}
                onClose={() => closeSettings()}
                onEscapeKeyDown={() => closeSettings()}
                onBackdropClick={() => closeSettings()}
                aria-labelledby='max-width-dialog-title'
            >
                <DialogTitle id='max-width-dialog-title'>
                    <Typography variant='h6'>Application Settings</Typography>
                    <IconButton
                        aria-label='close'
                        onClick={() => closeSettings()}
                        style={{
                            position: 'absolute',
                            right: 10,
                            top: 5,
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent
                    style={{
                        paddingLeft: 0,
                    }}
                >
                    <div
                        style={{
                            flexGrow: 1,
                            display: 'flex',
                            height: '350px',
                        }}
                    >
                        <Tabs
                            orientation='vertical'
                            variant='scrollable'
                            onChange={onTabChanged}
                            value={appSettings.activePage}
                            style={{
                                borderRight: '1px solid black',
                            }}
                        >
                            <Tab label='Season Groups' {...a11yProps(0)} />
                            <Tab label='Data Caching' {...a11yProps(1)} />
                            <Tab label='Diagnostics' {...a11yProps(1)} />
                        </Tabs>
                        <TabPanel value={appSettings.activePage} index={0}>
                            <SeasonGroupSettings />
                        </TabPanel>
                        <TabPanel value={appSettings.activePage} index={1}>
                            <DataCachingSettings />
                        </TabPanel>
                        <TabPanel value={appSettings.activePage} index={2}>
                            <DiagnosticsSettings />
                        </TabPanel>
                    </div>
                </DialogContent>
            </Dialog>
        </>
    );
};
