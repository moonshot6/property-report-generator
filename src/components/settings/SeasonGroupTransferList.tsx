import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { FixedSizeList, ListChildComponentProps } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import { IClientInfo, ISeasonGroup } from 'model/Configuration';
import { useClientManager, useSeasonGroupManager } from 'actions/Configuration';

interface ISeasonGroupTransferList {
    seasonGroup: ISeasonGroup;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: 'auto',
        },
        paper: {
            height: 230,
            overflow: 'auto',
        },
        button: {
            margin: theme.spacing(0.5, 0),
        },
    })
);

const not = (a: IClientInfo[], b: IClientInfo[]) => {
    return a.filter((value) => b.indexOf(value) === -1);
};

const intersection = (a: IClientInfo[], b: IClientInfo[]) => {
    return a.filter((value) => b.indexOf(value) !== -1);
};

export const SeasonGroupTransferList: React.FC<ISeasonGroupTransferList> = (props) => {
    const { clientList } = useClientManager();
    const { updateSeasonGroup } = useSeasonGroupManager();

    const getSeasonGroupItems = (seasonGroup: ISeasonGroup) => {
        return clientList.filter((client) => {
            if (client.id) {
                return seasonGroup.assignedClientIds.includes(client.id);
            }

            return false;
        });
    };

    const classes = useStyles();
    const [checked, setChecked] = React.useState<IClientInfo[]>([]);
    const [left, setLeft] = React.useState<IClientInfo[]>(
        not(clientList, getSeasonGroupItems(props.seasonGroup))
    );
    const [right, setRight] = React.useState<IClientInfo[]>(getSeasonGroupItems(props.seasonGroup));

    const leftChecked = intersection(checked, left);
    const rightChecked = intersection(checked, right);

    React.useEffect(() => {
        setChecked([]);
        setLeft(not(clientList, getSeasonGroupItems(props.seasonGroup)));
        setRight(getSeasonGroupItems(props.seasonGroup));
    }, [props.seasonGroup]);

    React.useEffect(() => {
        // Update the selected season group with added/removed clients
        updateSeasonGroup({
            ...props.seasonGroup,
            assignedClientIds: right.map((value) => value.id as number),
        });
    }, [right]);

    const sortClientsAlphabetically = (a: IClientInfo, b: IClientInfo) => {
        return a.name > b.name ? 1 : -1;
    };

    const handleToggle = (value: IClientInfo) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const handleAllRight = () => {
        setRight(right.concat(left).sort(sortClientsAlphabetically));
        setLeft([]);
    };

    const handleCheckedRight = () => {
        setRight(right.concat(leftChecked).sort(sortClientsAlphabetically));
        setLeft(not(left, leftChecked).sort(sortClientsAlphabetically));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = () => {
        setLeft(left.concat(rightChecked).sort(sortClientsAlphabetically));
        setRight(not(right, rightChecked).sort(sortClientsAlphabetically));
        setChecked(not(checked, rightChecked));
    };

    const handleAllLeft = () => {
        setLeft(left.concat(right).sort(sortClientsAlphabetically));
        setRight([]);
    };

    const renderRow = (props: ListChildComponentProps) => {
        const { index, style, data } = props;

        return (
            <ListItem
                key={data[index].id}
                role='listitem'
                button
                onClick={handleToggle(data[index])}
                style={style}
            >
                <ListItemIcon>
                    <Checkbox
                        checked={checked.indexOf(data[index]) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{ 'aria-labelledby': data[index].name }}
                    />
                </ListItemIcon>
                <ListItemText id={data[index].name} secondary={data[index].name} />
            </ListItem>
        );
    };

    const customList = (items: IClientInfo[]) => (
        <Paper className={classes.paper}>
            <AutoSizer>
                {({ height, width }) => (
                    <FixedSizeList
                        height={height}
                        width={width}
                        itemSize={40}
                        itemCount={items.length}
                        itemData={items}
                    >
                        {renderRow}
                    </FixedSizeList>
                )}
            </AutoSizer>
        </Paper>
    );

    return (
        <Grid container spacing={2} justify='center' alignItems='center' className={classes.root}>
            <Grid item xs>
                {customList(left)}
            </Grid>
            <Grid item>
                <Grid container direction='column' alignItems='center'>
                    <Button
                        variant='outlined'
                        size='small'
                        className={classes.button}
                        onClick={handleAllRight}
                        disabled={left.length === 0}
                        aria-label='move all right'
                    >
                        Add All
                    </Button>
                    <Button
                        variant='outlined'
                        size='small'
                        className={classes.button}
                        onClick={handleCheckedRight}
                        disabled={leftChecked.length === 0}
                        aria-label='move selected right'
                    >
                        Add
                    </Button>
                    <Button
                        variant='outlined'
                        size='small'
                        className={classes.button}
                        onClick={handleCheckedLeft}
                        disabled={rightChecked.length === 0}
                        aria-label='move selected left'
                    >
                        Remove
                    </Button>
                    <Button
                        variant='outlined'
                        size='small'
                        className={classes.button}
                        onClick={handleAllLeft}
                        disabled={right.length === 0}
                        aria-label='move all left'
                    >
                        Remove All
                    </Button>
                </Grid>
            </Grid>
            <Grid item xs>
                {customList(right)}
            </Grid>
        </Grid>
    );
};
