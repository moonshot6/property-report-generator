import React, { Suspense } from 'react';
import {
    CircularProgress,
    createStyles,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    Typography,
} from '@material-ui/core';
import { AcUnit, Add, Delete, Eco, Edit, Save, WbSunny } from '@material-ui/icons';
import { SeasonGroupTransferList } from './SeasonGroupTransferList';
import { useSeasonGroupManager } from 'actions/Configuration';
import { SeasonGroupEditorDialog } from '../SeasonGroupEditorDialog';
import { SpeedDial, SpeedDialAction, SpeedDialIcon } from '@material-ui/lab';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { ISeasonGroup } from 'model/Configuration';
import { db } from 'actions/ApplicationDatabase';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 380,
            transform: 'translateZ(0px)',
            flexGrow: 1,
        },
        speedDial: {
            position: 'absolute',
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
    })
);

export const SeasonGroupSettings: React.FC = (props) => {
    const {
        seasonGroupList,
        addSeasonGroup,
        updateSeasonGroup,
        deleteSeasonGroup,
    } = useSeasonGroupManager();

    const classes = useStyles();

    const [selectedSeason, setSelectedSeason] = React.useState<string>('');
    const [selectedSeasonData, setSelectedSeasonData] = React.useState<ISeasonGroup | undefined>(
        undefined
    );
    const [isSeasonEditorDialogOpen, setIsSeasonEditorDialogOpen] = React.useState<boolean>(false);
    const [isAddingSeason, setIsAddingSeason] = React.useState<boolean>(false);
    const [isSpeedDialOpen, setIsSpeedDialOpen] = React.useState(false);

    React.useEffect(() => {
        try {
            setSelectedSeasonData(
                seasonGroupList.find((season) => season.id === Number.parseInt(selectedSeason))
            );
        } catch {
            setSelectedSeasonData(undefined);
        }
    }, [selectedSeason]);

    React.useEffect(() => {
        setSelectedSeason(seasonGroupList.length === 0 ? '' : selectedSeason);
    }, [seasonGroupList]);

    const handleSelectedSeasonChanged = (event: React.ChangeEvent<{ value: unknown }>) => {
        setSelectedSeason(event.target.value as string);
    };

    const onSeasonGroupDialogClosed = (seasonGroup?: ISeasonGroup) => {
        setIsSeasonEditorDialogOpen(false);
        if (seasonGroup) {
            if (isAddingSeason) {
                // Adding a new season group
                addSeasonGroup(seasonGroup).then((id: number) => {
                    setSelectedSeason(id.toString());
                });
            } else if (selectedSeasonData) {
                // Updating an existing season group
                updateSeasonGroup(seasonGroup).then((id: number) => {
                    setSelectedSeason(id.toString());
                });
            }
        }

        setIsAddingSeason(false);
    };

    const buildSeasonGroupList = () => {
        return seasonGroupList.map((seasonGroup) => {
            return <MenuItem value={seasonGroup.id}>{seasonGroup.name}</MenuItem>;
        });
    };

    const actions = React.useMemo(() => {
        let actions = [
            {
                icon: <Add />,
                name: 'Add',
                hidden: false,
                onClick: () => {
                    setIsAddingSeason(true);
                    setIsSeasonEditorDialogOpen(true);
                },
            },
        ];

        if (selectedSeason !== '') {
            actions.push({
                icon: <Edit />,
                name: 'Edit',
                hidden: selectedSeason === '',
                onClick: () => setIsSeasonEditorDialogOpen(true),
            });
            actions.push({
                icon: <Delete />,
                name: 'Delete',
                hidden: selectedSeason === '',
                onClick: () => {
                    deleteSeasonGroup(Number.parseInt(selectedSeason));
                    setSelectedSeason('');
                    setSelectedSeasonData(undefined);
                },
            });
        }

        return actions;
    }, [selectedSeason]);

    const fixedControls = React.useMemo(() => {
        return (
            <>
                {isSeasonEditorDialogOpen && (
                    <SeasonGroupEditorDialog
                        isOpen={isSeasonEditorDialogOpen}
                        defaultSeasonGroup={selectedSeasonData}
                        onClose={onSeasonGroupDialogClosed}
                    />
                )}
                <SpeedDial
                    ariaLabel='SpeedDial tooltip example'
                    className={classes.speedDial}
                    icon={<SpeedDialIcon />}
                    onClick={() => setIsSpeedDialOpen((isOpen) => !isOpen)}
                    open={isSpeedDialOpen}
                >
                    {actions.map((action) => (
                        <SpeedDialAction
                            hidden={action.hidden}
                            title={action.name}
                            key={action.name}
                            icon={action.icon}
                            tooltipTitle={action.name}
                            tooltipOpen
                            onClick={() => {
                                if (action.onClick) {
                                    action.onClick();
                                }
                            }}
                        />
                    ))}
                </SpeedDial>
            </>
        );
    }, [isSeasonEditorDialogOpen, selectedSeasonData, isSpeedDialOpen, actions]);

    if (seasonGroupList.length > 0) {
        return (
            <div>
                <Grid container>
                    <Grid item xs>
                        <FormControl variant='outlined' fullWidth={true}>
                            <InputLabel>Select A Season Group</InputLabel>
                            <Select
                                disabled={seasonGroupList.length === 0}
                                value={selectedSeason}
                                onChange={handleSelectedSeasonChanged}
                                label='Select A Season Group'
                            >
                                <MenuItem value=''>
                                    <em>None</em>
                                </MenuItem>
                                {buildSeasonGroupList()}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs>
                        {selectedSeasonData && (
                            <Suspense fallback={<CircularProgress />}>
                                <SeasonGroupTransferList seasonGroup={selectedSeasonData} />
                            </Suspense>
                        )}
                        {!selectedSeasonData && (
                            <Grid item xs>
                                <Paper
                                    style={{
                                        marginTop: '10px',
                                        paddingTop: '10px',
                                        paddingBottom: '10px',
                                    }}
                                >
                                    <Grid container>
                                        <Grid item xs />
                                        <Grid item>
                                            <WbSunny color={'error'} fontSize={'large'} />
                                        </Grid>
                                        <Grid item>
                                            <Eco
                                                style={{
                                                    color: 'green',
                                                }}
                                                fontSize={'large'}
                                            />
                                        </Grid>
                                        <Grid item>
                                            <AcUnit color={'primary'} fontSize={'large'} />
                                        </Grid>
                                        <Grid item xs />
                                    </Grid>
                                    <Grid container>
                                        <Grid item xs>
                                            <Typography variant={'body1'} align={'center'}>
                                                No season group selected
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        )}
                    </Grid>
                </Grid>
                {fixedControls}
            </div>
        );
    } else {
        return (
            <div>
                <Grid container>
                    <Grid item xs>
                        <Paper
                            style={{
                                marginTop: '10px',
                                paddingTop: '10px',
                                paddingBottom: '10px',
                            }}
                        >
                            <Grid container>
                                <Grid item xs />
                                <Grid item>
                                    <WbSunny color={'error'} fontSize={'large'} />
                                </Grid>
                                <Grid item>
                                    <Eco
                                        style={{
                                            color: 'green',
                                        }}
                                        fontSize={'large'}
                                    />
                                </Grid>
                                <Grid item>
                                    <AcUnit color={'primary'} fontSize={'large'} />
                                </Grid>
                                <Grid item xs />
                            </Grid>
                            <Grid container>
                                <Grid item xs>
                                    <Typography variant={'body1'} align={'center'}>
                                        No season groups found, please add one below by clicking the
                                        "plus" icon.
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
                {fixedControls}
            </div>
        );
    }
};
