import React from 'react';
import {
    Avatar,
    Dialog,
    DialogTitle,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
} from '@material-ui/core';

export interface ISimpleDialogListItem {
    key: string;
    icon?: React.ReactNode;
    text: string;
}

export interface ISimpleDialogList {
    title?: string;
    isOpen: boolean;
    items: ISimpleDialogListItem[];
    onClose: (value?: string) => void;
}

export const SimpleDialogList: React.FC<ISimpleDialogList> = (props) => {
    const handleClose = () => {
        props.onClose();
    };

    const handleListItemClick = (value: ISimpleDialogListItem) => {
        props.onClose(value.key);
    };

    return (
        <Dialog onClose={handleClose} aria-labelledby='simple-dialog-title' open={props.isOpen}>
            <DialogTitle id='simple-dialog-title'>{props.title ?? 'Select an option'}</DialogTitle>
            <List>
                {props.items.map((item) => (
                    <ListItem button onClick={() => handleListItemClick(item)} key={item.key}>
                        {item.icon && (
                            <ListItemAvatar>
                                <Avatar
                                    style={{
                                        color: '#414141',
                                    }}
                                >
                                    {item.icon}
                                </Avatar>
                            </ListItemAvatar>
                        )}
                        <ListItemText primary={item.text} />
                    </ListItem>
                ))}
            </List>
        </Dialog>
    );
};
