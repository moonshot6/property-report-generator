import React from 'react';
import { ipcRenderer } from 'electron';

import {
    CircularProgress,
    Dialog,
    DialogContent,
    DialogTitle,
    Grid,
    LinearProgress,
    Typography,
} from '@material-ui/core';
import { IUpdateStatus, UpdateStatusType } from '../model/UpdateStatus';

export const ConfigurationManager: React.FC = (props) => {
    const [isLoadingDialogOpen, setIsLoadingDialogOpen] = React.useState(false);
    const [isCheckingForUpdates, setIsCheckingForUpdates] = React.useState(false);
    const [isUpdating, setIsUpdating] = React.useState(false);
    const [updateMessage, setUpdateMessage] = React.useState('');
    const [updateProgress, setUpdateProgress] = React.useState(0);

    React.useEffect(() => {
        setIsLoadingDialogOpen(true);
        ipcRenderer.send('request-update');
    }, []);

    const onCheckingForUpdates = (status: IUpdateStatus) => {
        setIsCheckingForUpdates(true);
        setUpdateMessage(status.message);
    };

    const onNoUpdate = (status: IUpdateStatus) => {
        setIsCheckingForUpdates(false);
        setUpdateMessage(status.message);

        window.setTimeout(() => {
            setIsLoadingDialogOpen(false);
        }, 2000);
    };

    const onRestarting = (status: IUpdateStatus) => {
        setIsCheckingForUpdates(false);
        setUpdateMessage(status.message);
    };

    const onUpdateAvailable = (status: IUpdateStatus) => {
        setIsCheckingForUpdates(false);
        setIsUpdating(true);
    };

    const onUpdateDownloading = (status: IUpdateStatus) => {
        setUpdateMessage(status.message);
    };

    const onUpdateDownloaded = (status: IUpdateStatus) => {
        setUpdateMessage(status.message);
    };

    const onUpdateError = (status: IUpdateStatus) => {
        setIsCheckingForUpdates(false);
        window.setTimeout(() => {
            setIsLoadingDialogOpen(false);
        }, 2000);
    };

    const onDownloadProgress = (status: IUpdateStatus) => {
        setUpdateMessage(status.message);
        setUpdateProgress(status.downloadProgress ?? 0);
    };

    ipcRenderer.on('asynchronous-message', (event, args: IUpdateStatus) => {
        if (args.type !== undefined && args.message !== undefined) {
            switch (args.type) {
                case UpdateStatusType.CheckingForUpdates:
                    onCheckingForUpdates(args);
                    break;
                case UpdateStatusType.DownloadProgress:
                    onDownloadProgress(args);
                    break;
                case UpdateStatusType.RestartingToUpdate:
                    onRestarting(args);
                    break;
                case UpdateStatusType.UpdateAvailable:
                    onUpdateAvailable(args);
                    break;
                case UpdateStatusType.UpdateDownloading:
                    onUpdateDownloading(args);
                    break;
                case UpdateStatusType.UpdateDownloaded:
                    onUpdateDownloaded(args);
                    break;
                case UpdateStatusType.UpdateError:
                    onUpdateError(args);
                    break;
                case UpdateStatusType.UpdateNotAvailble:
                    onNoUpdate(args);
                    break;
            }
        }
    });

    const renderUpdateMessage = () => {
        if (isCheckingForUpdates && !isUpdating) {
            return (
                <Grid container>
                    <Grid item xs>
                        <Typography variant='h6'>Checking For Updates</Typography>
                    </Grid>
                </Grid>
            );
        } else if (isUpdating) {
            return (
                <Grid container>
                    <Grid item xs>
                        <Typography variant='h6'>Updating Application</Typography>
                        <Typography variant='body1'>{updateMessage}</Typography>
                        <LinearProgress value={updateProgress} />
                    </Grid>
                </Grid>
            );
        } else {
            return (
                <Grid container>
                    <Grid item xs>
                        <Typography variant='body1'>{updateMessage}</Typography>
                    </Grid>
                </Grid>
            );
        }
    };

    return (
        <Dialog
            fullWidth={true}
            open={isLoadingDialogOpen}
            aria-labelledby='max-width-dialog-title'
        >
            <DialogTitle id='max-width-dialog-title'>Starting Application</DialogTitle>
            <DialogContent>
                <Grid container>
                    <Grid item xs>
                        <Grid container>
                            <Grid item>
                                <CircularProgress size={20} style={{ marginRight: '10px' }} />
                            </Grid>
                            <Grid item xs>
                                <Typography variant='h5'>Please Wait...</Typography>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs>
                                {renderUpdateMessage()}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    );
};
