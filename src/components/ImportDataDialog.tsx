import React, { Suspense } from 'react';
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button,
    CircularProgress,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    LinearProgress,
    Paper,
    Step,
    StepLabel,
    Stepper,
    Tooltip,
    Typography,
} from '@material-ui/core';
import { ExpandMore as ExpandMoreIcon } from '@material-ui/icons';
import { DropZoneListArea } from './DropZoneListArea';
import {
    IAverageRatesFileData,
    IOccupancyFileData,
    readAverageRatesFile,
    readOccupancyFile,
    readDailyRatesFile,
    IDailyRates,
} from 'actions/Statistics';
import { db } from 'actions/ApplicationDatabase';
import { useClientManager } from 'actions/Configuration';
import { IClientInfo, IDatedValue, ISeasonalRate } from 'model/Configuration';

import sampleAverageRatesDataImage from 'images/sample-average-rates-data.png';
import sampleMonthlyAdrDataImage from 'images/monthly-adr-data.png';
import sampleOccupancyDataImage from 'images/occupany-rates-data.png';
import { IImportedColumn, MapSeasonsDialog } from './MapSeasonsDialog';

interface IImportDataDialog {
    show?: boolean;
    onDialogClosed: () => void;
}

interface IImportPageProps {
    onPageComplete: () => void;
    onPageErrors: () => void;
}

export const ImportDataDialog: React.FC<IImportDataDialog> = (props) => {
    const [isOpen, setIsOpen] = React.useState<boolean>(props.show ?? false);
    const [activeStep, setActiveStep] = React.useState(0);

    const [isPreviousButtonEnabled, setIsPreviousButtonEnabled] = React.useState<boolean>(true);
    const [isNextButtonEnabled, setIsNextButtonEnabled] = React.useState<boolean>(false);

    React.useEffect(() => {
        setIsOpen(props.show ?? false);
    }, [props.show]);

    const steps = React.useMemo(() => {
        return [
            'Monthly Average Rate Data',
            'Monthly Occupancy Data',
            'Monthly Revenue Data',
            'Daily Rates Data',
        ];
    }, []);

    const years = [
        new Date().getFullYear() - 1,
        new Date().getFullYear(),
        new Date().getFullYear() + 1,
    ];

    const AdrPageContent: React.FC<IImportPageProps> = (props) => {
        const { addClientsBulk } = useClientManager();

        const [validDrops, setValidDrops] = React.useState({
            firstYear: false,
            secondYear: false,
            thirdYear: false,
        });

        React.useEffect(() => {
            if (validDrops.firstYear && validDrops.secondYear && validDrops.thirdYear) {
                props.onPageComplete();
            }
        }, [validDrops]);

        const processAdrData = async (year: number, file: File[]): Promise<boolean> => {
            const properties = await readAverageRatesFile(file[0], year);

            if (properties.length === 0) {
                return false;
            }

            const addAndUpdateAsync = async (property: IAverageRatesFileData) => {
                // Get client ID from property name
                const matchingClient = await db.clients.where('name').equals(property.unit).first();

                if (matchingClient && matchingClient.id) {
                    // Check to see if the data already for this year already exists
                    const matchingYear = await db.adrData
                        .where('clientId')
                        .equals(matchingClient.id)
                        .and((x) => x.year === year)
                        .first();

                    if (matchingYear && matchingYear.id) {
                        matchingYear.monthData = property.fees;
                        db.adrData.update(matchingYear.id, {
                            ...matchingYear,
                        });
                    } else {
                        db.adrData.put({
                            year: year,
                            clientId: matchingClient.id,
                            monthData: property.fees,
                        });
                    }
                }
            };

            // Get all client ids from database
            let allClients = await Promise.all(
                properties.map(async (property) => {
                    const clientId = await db.clients.where('name').equals(property.unit).first();

                    return {
                        name: property.unit,
                        id: clientId?.id,
                    } as IClientInfo;
                })
            );

            // Find clients that are not in the database
            const unregisteredClients = allClients.filter((client) => {
                return client.id === undefined;
            });

            // Add all unregistered clients
            await addClientsBulk(unregisteredClients);

            properties.forEach((property) => addAndUpdateAsync(property));

            return true;
        };

        const renderer = (index: number) => {
            switch (index) {
                case 0:
                    return (
                        <>
                            <Typography variant='body1'>Upload {years[0]} ADR Data</Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                case 1:
                    return (
                        <>
                            <Typography variant='body1'>Upload {years[1]} ADR Data</Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                case 2:
                    return (
                        <>
                            <Typography variant='body1'>Upload {years[2]} ADR Data</Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                default:
                    return <></>;
            }
        };

        const onDropFilesReady = async (index: number, files: File[]) => {
            if (files.length === 0) {
                window.alert('Only one file can be provided for each year');
                return false;
            }

            const processingResult = await processAdrData(years[index], files);

            if (index === 0) {
                setValidDrops((prevState) => {
                    return {
                        ...prevState,
                        firstYear: processingResult,
                    };
                });
            } else if (index === 1) {
                setValidDrops((prevState) => {
                    return {
                        ...prevState,
                        secondYear: processingResult,
                    };
                });
            } else {
                setValidDrops((prevState) => {
                    return {
                        ...prevState,
                        thirdYear: processingResult,
                    };
                });
            }

            return processingResult;
        };

        return (
            <Grid container>
                <Typography
                    variant={'body2'}
                    style={{
                        width: '100%',
                        textAlign: 'center',
                    }}
                >
                    Please supply average monthly rate data for the last 3 years.
                </Typography>
                <DropZoneListArea
                    count={3}
                    dropContentRenderer={renderer}
                    onDropFilesReady={onDropFilesReady}
                />
                <Typography
                    variant={'body2'}
                    style={{
                        marginTop: '20px',
                    }}
                >
                    Example Data:
                </Typography>
                <Paper
                    variant={'elevation'}
                    elevation={2}
                    style={{
                        padding: '10px',
                    }}
                >
                    <img
                        src={sampleMonthlyAdrDataImage}
                        alt={'example-monthly-adr-data'}
                        style={{
                            width: '100%',
                        }}
                    />
                </Paper>
            </Grid>
        );
    };

    const OccupancyPageContent: React.FC<IImportPageProps> = (props) => {
        const { addClientsBulk } = useClientManager();

        const [validDrops, setValidDrops] = React.useState({
            firstYear: false,
            secondYear: false,
            thirdYear: false,
        });

        React.useEffect(() => {
            if (validDrops.firstYear && validDrops.secondYear && validDrops.thirdYear) {
                props.onPageComplete();
            }
        }, [validDrops]);

        const processOccupancyData = async (year: number, file: File[]): Promise<boolean> => {
            const properties = await readOccupancyFile(file[0], year);

            if (properties.length === 0) {
                return false;
            }

            const addAndUpdateAsync = async (property: IOccupancyFileData) => {
                // Get client ID from property name
                const matchingClient = await db.clients.where('name').equals(property.unit).first();

                if (matchingClient && matchingClient.id) {
                    // Check to see if the data already for this year already exists
                    const matchingYear = await db.occupancyData
                        .where('clientId')
                        .equals(matchingClient.id)
                        .and((x) => x.year === year)
                        .first();

                    if (matchingYear && matchingYear.id) {
                        matchingYear.occupancyData = property.occupancy;
                        db.occupancyData.update(matchingYear.id, {
                            ...matchingYear,
                        });
                    } else {
                        db.occupancyData.put({
                            year: year,
                            clientId: matchingClient.id,
                            occupancyData: property.occupancy,
                        });
                    }
                }
            };

            // Get all client ids from database
            let allClients = await Promise.all(
                properties.map(async (property) => {
                    const clientId = await db.clients.where('name').equals(property.unit).first();

                    return {
                        name: property.unit,
                        id: clientId?.id,
                    } as IClientInfo;
                })
            );

            // Find clients that are not in the database
            const unregisteredClients = allClients.filter((client) => {
                return client.id === undefined;
            });

            if (unregisteredClients.length > 0) {
                // Provide a warning that the ADR data uploaded and the occupancy data do not mention the same list of clients!

                // Add all unregistered clients
                await addClientsBulk(unregisteredClients);
            }

            properties.forEach((property) => addAndUpdateAsync(property));

            return true;
        };

        const renderer = (index: number) => {
            switch (index) {
                case 0:
                    return (
                        <>
                            <Typography variant='body1'>
                                Upload {years[0]} Occupancy Data
                            </Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                case 1:
                    return (
                        <>
                            <Typography variant='body1'>
                                Upload {years[1]} Occupancy Data
                            </Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                case 2:
                    return (
                        <>
                            <Typography variant='body1'>
                                Upload {years[2]} Occupancy Data
                            </Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                default:
                    return <></>;
            }
        };

        const onDropFilesReady = async (index: number, files: File[]) => {
            if (files.length === 0) {
                window.alert('Only one file can be provided for each year');
                return false;
            }

            const processingResult = await processOccupancyData(years[index], files);

            if (index === 0) {
                setValidDrops((prevState) => {
                    return {
                        ...prevState,
                        firstYear: processingResult,
                    };
                });
            } else if (index === 1) {
                setValidDrops((prevState) => {
                    return {
                        ...prevState,
                        secondYear: processingResult,
                    };
                });
            } else {
                setValidDrops((prevState) => {
                    return {
                        ...prevState,
                        thirdYear: processingResult,
                    };
                });
            }

            return processingResult;
        };

        return (
            <Grid container>
                <Typography
                    variant={'body2'}
                    style={{
                        width: '100%',
                        textAlign: 'center',
                    }}
                >
                    Please supply occupancy data for the last 3 years.
                </Typography>
                <DropZoneListArea
                    count={3}
                    dropContentRenderer={renderer}
                    onDropFilesReady={onDropFilesReady}
                />
                <Typography
                    variant={'body2'}
                    style={{
                        marginTop: '20px',
                    }}
                >
                    Example Data:
                </Typography>
                <Paper
                    variant={'elevation'}
                    elevation={2}
                    style={{
                        padding: '10px',
                    }}
                >
                    <img
                        src={sampleOccupancyDataImage}
                        alt={'example-occupancy-rates-data'}
                        style={{
                            width: '100%',
                        }}
                    />
                </Paper>
            </Grid>
        );
    };

    const RevenuePageContent: React.FC<IImportPageProps> = (props) => {
        const [isProcessing, setIsProcessing] = React.useState<boolean>(false);
        const [processingResults, setProcessingResults] = React.useState<string[]>([]);

        const processRevenueData = async (year: number): Promise<string[]> => {
            const results: string[] = [];

            // Get all clients
            const allClients = await db.clients.toArray();

            // Foreach client
            for (const client of allClients) {
                try {
                    if (client.id) {
                        // Get all ADR data
                        const clientAdrData = await db.adrData
                            .where('clientId')
                            .equals(client.id)
                            .and((x) => x.year === year)
                            .first();

                        // Get all occupancy data
                        const clientOccupancyData = await db.occupancyData
                            .where('clientId')
                            .equals(client.id)
                            .and((x) => x.year === year)
                            .first();

                        if (clientAdrData === undefined || clientOccupancyData === undefined) {
                            // Missing data for that year
                            throw new Error(
                                `Missing ${year} ${
                                    clientAdrData === undefined ? 'ADR Data' : 'Occupancy Data'
                                } for client \"${client.name}\"`
                            );
                        }

                        if (
                            clientAdrData.monthData.length !==
                            clientOccupancyData.occupancyData.length
                        ) {
                            throw new Error(
                                `${year} data for client \"${client.name}\" does not contain similar dates`
                            );
                        }

                        const revenueData = clientAdrData.monthData.map((rate) => {
                            const matchingOccupancy = clientOccupancyData.occupancyData.find(
                                (occupancy) => {
                                    return (
                                        occupancy.date.getMonth() === rate.date.getMonth() &&
                                        occupancy.date.getFullYear() === rate.date.getFullYear()
                                    );
                                }
                            );

                            if (!matchingOccupancy) {
                                throw new Error(
                                    `No ${rate.date.getFullYear()} occupancy data for client \"${
                                        client.name
                                    }\"`
                                );
                            }

                            const daysInMonth = new Date(year, rate.date.getMonth(), 0).getDate();

                            // Rate x (days/month * occupancy)
                            return {
                                date: rate.date,
                                value: rate.value * (daysInMonth * matchingOccupancy.value),
                            } as IDatedValue;
                        });

                        // If revenue data already exists, update it
                        const matchingRevenueData = await db.revenueData
                            .where('clientId')
                            .equals(client.id)
                            .and((x) => x.year === year)
                            .first();

                        if (matchingRevenueData && matchingRevenueData.id) {
                            await db.revenueData.update(matchingRevenueData.id, {
                                ...matchingRevenueData,
                                revenueData: revenueData,
                            });
                        } else {
                            await db.revenueData.put({
                                id: undefined,
                                year: year,
                                clientId: client.id,
                                revenueData: revenueData,
                            });
                        }
                    } else {
                        throw new Error(
                            `Client \"${client.name}\" does not have a unique ID in the database. This is most likely a bug that should be reported.`
                        );
                    }
                } catch (exception: any) {
                    results.push(exception.message);
                }
            }

            return results;
        };

        React.useEffect(() => {
            setIsProcessing(true);
            for (const year of years) {
                processRevenueData(year).then((results) => {
                    setProcessingResults((lastValue) => [...lastValue, ...results]);

                    if (year === years[years.length - 1]) {
                        setIsProcessing(false);

                        if (props.onPageComplete) {
                            props.onPageComplete();
                        }
                    }
                });
            }
        }, []);

        React.useEffect(() => {
            setIsPreviousButtonEnabled(!isProcessing);
        }, [isProcessing]);

        const ProcessingIndicator = () => {
            return (
                <Grid container>
                    <Typography
                        variant={'body1'}
                        style={{
                            width: '100%',
                            textAlign: 'center',
                        }}
                    >
                        Calculating and saving revenue data for{' '}
                        {`${years[0]}, ${years[1]}, ${years[2]}`}...
                    </Typography>
                    <Typography
                        variant={'caption'}
                        style={{
                            width: '100%',
                            textAlign: 'center',
                        }}
                    >
                        Please wait...
                    </Typography>
                    <Grid container>
                        <Grid
                            item
                            xs
                            style={{
                                marginTop: '10px',
                                textAlign: 'center',
                            }}
                        >
                            <CircularProgress />
                        </Grid>
                    </Grid>
                </Grid>
            );
        };

        const processingText = React.useMemo(() => {
            return (
                <ul>
                    {processingResults.map((value) => {
                        return (
                            <li>
                                <Typography variant={'body1'}>{value}</Typography>
                            </li>
                        );
                    })}
                </ul>
            );
        }, [processingResults]);

        const FinishedProcessingText = () => {
            return (
                <Grid container>
                    <Grid item xs>
                        <Typography
                            variant={'body1'}
                            style={{
                                width: '100%',
                                textAlign: 'center',
                            }}
                        >
                            Finished processing data. Click "Finish" when done.
                        </Typography>
                        {processingResults.length > 0 && (
                            <Accordion>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                                    <Typography>
                                        {processingResults.length} issues found.
                                    </Typography>
                                </AccordionSummary>
                                <AccordionDetails>{processingText}</AccordionDetails>
                            </Accordion>
                        )}
                    </Grid>
                </Grid>
            );
        };

        return (
            <Grid container>
                {isProcessing && <ProcessingIndicator />}
                {!isProcessing && <FinishedProcessingText />}
            </Grid>
        );
    };

    const DailyRatesPageContent: React.FC<IImportPageProps> = (props) => {
        const [isDataProvided, setIsDataProvided] = React.useState<boolean>(false);
        const [isMappingDialogOpen, setIsMappingDialogOpen] = React.useState<boolean>(false);
        const [mappingColumnNames, setMappingColumnNames] = React.useState<string[]>([]);
        const [dailyRatesData, setDailyRatesData] = React.useState<IDailyRates[]>();

        React.useEffect(() => {
            setIsMappingDialogOpen(false);
        }, []);

        React.useEffect(() => {
            if (isDataProvided) {
                props.onPageComplete();
            }
        }, [isDataProvided]);

        const processDailyRates = async (files: File[]): Promise<boolean> => {
            const properties = await readDailyRatesFile(files);
            setDailyRatesData(properties);

            const uniqueKeyNames: string[] = [];

            // Get all unique key names
            properties.forEach((value) => {
                const keys = Object.keys(value);

                keys.forEach((key) => {
                    if (!uniqueKeyNames.includes(key) && key !== 'name') {
                        uniqueKeyNames.push(key);
                    }
                });
            });

            // Show a dialog that will allow the USER to define the mapping to each season.
            setMappingColumnNames(uniqueKeyNames);
            setIsMappingDialogOpen(true);

            return properties.length !== 0;
        };

        const renderer = (index: number) => {
            switch (index) {
                case 0:
                    return (
                        <>
                            <Typography variant='body1'>
                                Upload {new Date().getFullYear()} Daily Rates Data
                            </Typography>
                            <Typography variant='caption'>
                                Only (.csv) files are supported
                            </Typography>
                        </>
                    );
                default:
                    return <></>;
            }
        };

        const onDropFilesReady = async (index: number, files: File[]) => {
            if (files.length === 0) {
                window.alert('No valid CSV files were found');
                return false;
            }

            const processingResult = await processDailyRates(files);

            if (index === 0) {
                setIsDataProvided(processingResult);
            }

            return processingResult;
        };

        const onDailyRatesMappingFinished = async (results?: Map<string, IImportedColumn[]>) => {
            setIsMappingDialogOpen(false);

            if (!results || !dailyRatesData) {
                // User skipped mapping data.
                return;
            }

            // For each client read in from the daily rates data
            for (const rateData of dailyRatesData) {
                // Find a client that matches the name
                const matchingClient = await db.clients.where('name').equals(rateData.name).first();

                if (matchingClient) {
                    const seasonalRates: ISeasonalRate[] = [];

                    for (let [seasonId, importedColumns] of results) {
                        // For each group of key (column) names assigned to a season
                        const matchingSeason = await db.seasonGroups
                            .where('id')
                            .equals(Number.parseInt(seasonId))
                            .first();

                        if (matchingSeason) {
                            const rates: number[] = [];

                            for (const column of importedColumns) {
                                const keyTyped = column.name as keyof typeof rateData;
                                const value = rateData[keyTyped];

                                if (Array.isArray(value)) {
                                    // Value is an array of numbers. Average them
                                    for (var i = 0; i < value.length; i++) {
                                        rates.push(
                                            Number.parseFloat(value[i].replace(/[$,]/g, ''))
                                        );
                                    }
                                } else {
                                    rates.push(Number.parseFloat(value.replace(/[$,]/g, '')));
                                }
                            }

                            let averageRate = 0;
                            for (const rate of rates) {
                                averageRate += rate;
                            }
                            averageRate = averageRate / rates.length;

                            seasonalRates.push({
                                rate: averageRate,
                                seasonId: matchingSeason.id ?? 0,
                            });

                            if (matchingSeason.id && matchingClient.id) {
                                const updates = {
                                    ...matchingSeason,
                                    assignedClientIds: [
                                        ...matchingSeason.assignedClientIds.filter(
                                            (id) => id !== matchingClient.id
                                        ),
                                        matchingClient.id,
                                    ],
                                };

                                // Ensure this client is associated with the current season
                                await db.seasonGroups.update(matchingSeason.id, updates);
                            }
                        }
                    }

                    // Check to see if this client already has dailyRatesData
                    if (matchingClient.id) {
                        const existingData = await db.dailyRatesData
                            .where('clientId')
                            .equals(matchingClient.id)
                            .first();

                        if (existingData && existingData.id) {
                            // Use the client's ID to associate each column to a seasonal rate
                            db.dailyRatesData.update(existingData.id, {
                                id: existingData.id,
                                clientId: matchingClient.id,
                                rates: seasonalRates,
                            });
                        } else {
                            // Use the client's ID to associate each column to a seasonal rate
                            db.dailyRatesData.add({
                                id: undefined,
                                clientId: matchingClient.id,
                                rates: seasonalRates,
                            });
                        }
                    }
                }
            }
        };

        return (
            <Grid container>
                <Typography
                    variant={'body2'}
                    style={{
                        width: '100%',
                        textAlign: 'center',
                    }}
                >
                    Please supply seasonal daily rates data for the year specified.
                </Typography>
                <Typography
                    variant={'caption'}
                    style={{
                        width: '100%',
                        textAlign: 'center',
                    }}
                >
                    The drop box below supports dropping multiple files at once.
                </Typography>
                <DropZoneListArea
                    count={1}
                    dropContentRenderer={renderer}
                    onDropFilesReady={onDropFilesReady}
                />
                <Typography
                    variant={'body2'}
                    style={{
                        marginTop: '20px',
                    }}
                >
                    Example Data:
                </Typography>
                <Paper
                    variant={'elevation'}
                    elevation={2}
                    style={{
                        padding: '10px',
                    }}
                >
                    <img
                        src={sampleAverageRatesDataImage}
                        alt={'example-daily-rates-data'}
                        style={{
                            width: '100%',
                        }}
                    />
                </Paper>
                <MapSeasonsDialog
                    isOpen={isMappingDialogOpen}
                    unmappedColumnNames={mappingColumnNames}
                    onMappingDone={onDailyRatesMappingFinished}
                />
            </Grid>
        );
    };

    const enableNextButton = () => {
        setIsNextButtonEnabled(true);
    };

    const disableNextButton = () => {
        setIsNextButtonEnabled(false);
    };

    const Fallback = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    flexGrow: 1,
                    flexDirection: 'column',
                }}
            >
                <LinearProgress />
                <Typography
                    variant={'body1'}
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Please wait... Processing Your Data...
                </Typography>
            </div>
        );
    };

    const adrPage = React.useMemo(() => {
        return (
            <Suspense fallback={<Fallback />}>
                <AdrPageContent
                    onPageComplete={enableNextButton}
                    onPageErrors={disableNextButton}
                />
            </Suspense>
        );
    }, []);

    const occupancyPage = React.useMemo(() => {
        return (
            <Suspense fallback={<Fallback />}>
                <OccupancyPageContent
                    onPageComplete={enableNextButton}
                    onPageErrors={disableNextButton}
                />
            </Suspense>
        );
    }, []);

    const revenuePage = React.useMemo(() => {
        return (
            <RevenuePageContent
                onPageComplete={enableNextButton}
                onPageErrors={disableNextButton}
            />
        );
    }, []);

    const weeklyRatesPage = React.useMemo(() => {
        return (
            <DailyRatesPageContent
                onPageComplete={enableNextButton}
                onPageErrors={disableNextButton}
            />
        );
    }, []);

    const getStepContent = (stepIndex: number) => {
        switch (stepIndex) {
            case 0:
                return adrPage;
            case 1:
                return occupancyPage;
            case 2:
                return revenuePage;
            case 3:
                return weeklyRatesPage;
            default:
                return 'Unknown stepIndex';
        }
    };

    const handleNext = () => {
        setIsNextButtonEnabled(false);
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleCancel = () => {
        setIsOpen(false);

        // [TODO]: Add warning if the page index is > 0.

        if (props.onDialogClosed) {
            props.onDialogClosed();
        }

        setActiveStep(0);
    };

    const handleBack = () => {
        if (steps[activeStep - 1] === 'Monthly Revenue Data') {
            // Skip over processing revenue data if navigating backwards.
            setActiveStep((prevActiveStep) => prevActiveStep - 2);
        } else {
            setActiveStep((prevActiveStep) => prevActiveStep - 1);
        }
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const handleFinish = () => {
        setIsOpen(false);
        if (props.onDialogClosed) {
            props.onDialogClosed();
        }
        setActiveStep(0);
    };

    return (
        <Dialog
            open={isOpen}
            fullWidth={true}
            disableBackdropClick={true}
            maxWidth={'md'}
            onClose={props.onDialogClosed}
        >
            <DialogTitle>Import Wizard</DialogTitle>
            <DialogContent>
                <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>

                <Container>
                    <Grid container>
                        <Grid item xs>
                            {activeStep === steps.length ? (
                                <div>
                                    <Typography>All steps completed</Typography>
                                </div>
                            ) : (
                                getStepContent(activeStep)
                            )}
                        </Grid>
                    </Grid>
                </Container>
            </DialogContent>
            <DialogActions>
                {activeStep === steps.length ? (
                    <div>
                        <Button onClick={handleReset}>Reset</Button>
                    </div>
                ) : (
                    <div
                        style={{
                            display: 'flex',
                        }}
                    >
                        <Button
                            style={{
                                marginRight: '5px',
                            }}
                            disabled={activeStep === steps.length - 1}
                            onClick={handleCancel}
                        >
                            Cancel
                        </Button>
                        {activeStep > 0 && (
                            <Button
                                variant='contained'
                                color='primary'
                                onClick={handleBack}
                                disabled={!isPreviousButtonEnabled}
                                style={{
                                    marginLeft: '5px',
                                    marginRight: '5px',
                                }}
                            >
                                Previous
                            </Button>
                        )}
                        <Tooltip
                            title={'Please supply all data before continuing'}
                            placement={'top'}
                            arrow={true}
                            disableHoverListener={isNextButtonEnabled}
                        >
                            <div>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={
                                        activeStep === steps.length - 1 ? handleFinish : handleNext
                                    }
                                    disabled={!isNextButtonEnabled}
                                >
                                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                </Button>
                            </div>
                        </Tooltip>
                    </div>
                )}
            </DialogActions>
        </Dialog>
    );
};
