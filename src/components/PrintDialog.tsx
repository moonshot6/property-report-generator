import React from 'react';
import {
    Button,
    CircularProgress,
    Container,
    Dialog,
    DialogContent,
    DialogTitle,
    Grid,
    Typography,
} from '@material-ui/core';

import styles from 'App.module.css';
import { IActivePrintInfo } from './PrintContainer';

interface IPrintDialogProps {
    printInfo?: IActivePrintInfo;
    isOpen?: boolean;
    onClosed?: () => void;
    onCancelClicked?: () => void;
}

export const PrintDialog: React.FC<IPrintDialogProps> = (props) => {
    const [isOpen, setIsOpen] = React.useState<boolean>(props.isOpen ?? false);

    React.useEffect(() => {
        setIsOpen(props.isOpen ?? false);
    }, [props.isOpen]);

    const onCancelButtonClicked = () => {
        if (props.onCancelClicked) {
            props.onCancelClicked();
        }
    };

    return (
        <Dialog
            open={isOpen}
            className={styles.hideContent}
            disableBackdropClick={true}
            maxWidth={'md'}
            onClose={props.onClosed}
        >
            <DialogTitle>Printing Reports</DialogTitle>
            <DialogContent>
                <Container>
                    <Grid container>
                        <Grid
                            item
                            xs
                            style={{
                                textAlign: 'center',
                            }}
                        >
                            <CircularProgress />
                            <Typography variant={'body1'}>
                                Please wait while exporting all reports...
                            </Typography>
                            {props.printInfo && (
                                <Typography variant={'caption'}>
                                    Printing {props.printInfo.name} ({props.printInfo.jobId} of{' '}
                                    {props.printInfo.maxPrintJobs})
                                </Typography>
                            )}
                        </Grid>
                    </Grid>
                    <Grid container justify={'center'}>
                        <Grid item xs>
                            <Button
                                variant={'outlined'}
                                onClick={onCancelButtonClicked}
                                style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                }}
                            >
                                Cancel
                            </Button>
                        </Grid>
                    </Grid>
                </Container>
            </DialogContent>
        </Dialog>
    );
};
