import React, { Suspense } from 'react';
import { DragDropContext, Droppable, Draggable, DropResult } from 'react-beautiful-dnd';

import {
    Button,
    Chip,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    FormControl,
    Grid,
    InputAdornment,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    Tooltip,
    Typography,
} from '@material-ui/core';
import { useSeasonGroupManager } from '../actions/Configuration';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Add } from '@material-ui/icons';
import { SeasonGroupEditorDialog } from './SeasonGroupEditorDialog';
import { ISeasonGroup } from '../model/Configuration';

const grid = 8;

const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle,
});

const getUnmappedListStyle = (isDraggingOver: boolean) => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: grid,
    listStyle: 'none',
    height: '200px',
    overflow: 'auto',
});

const getMappedListStyle = (isDraggingOver: boolean) => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: grid,
    listStyle: 'none',
    flexGrow: 1,
    maxHeight: '250px',
    overflow: 'auto',
});

export interface IImportedColumn {
    id: string;
    seasonId?: number;
    name: string;
}

interface IMapSeasonsDialogProps {
    isOpen: boolean;
    unmappedColumnNames: string[];
    onMappingDone?: (mappedColumns?: Map<string, IImportedColumn[]>) => void;
}

const generateUniqueIds = (columnNames: string[]) => {
    return columnNames.map((value, index) => {
        return {
            id: `column-${index}-${Date.now()}`,
            name: value,
        } as IImportedColumn;
    });
};

export const MapSeasonsDialog: React.FC<IMapSeasonsDialogProps> = (props) => {
    const MapSeasonsDialogContent = () => {
        const { seasonGroupList, addSeasonGroup } = useSeasonGroupManager();

        const [isConfirmationWindowOpen, setIsConfirmationWindowOpen] = React.useState<boolean>(
            false
        );

        const [isSeasonGroupEditorOpen, setIsSeasonGroupEditorOpen] = React.useState<boolean>(
            false
        );

        const [unmappedColumns, setUnmappedColumns] = React.useState<IImportedColumn[]>(
            generateUniqueIds(props.unmappedColumnNames)
        );
        const [mappedColumns, setMappedColumns] = React.useState<Map<string, IImportedColumn[]>>(
            new Map<string, IImportedColumn[]>()
        );
        const [selectedSeason, setSelectedSeason] = React.useState<string>('');

        React.useEffect(() => {
            if (seasonGroupList.length) {
                if (seasonGroupList[0].id) {
                    setSelectedSeason(seasonGroupList[0].id.toString());
                }
            }

            setUnmappedColumns(generateUniqueIds(props.unmappedColumnNames));
            setMappedColumns(new Map<string, IImportedColumn[]>());
            setIsConfirmationWindowOpen(false);
            setIsSeasonGroupEditorOpen(false);
        }, [props.unmappedColumnNames]);

        React.useEffect(() => {
            if (seasonGroupList.length >= 1) {
                if (seasonGroupList[seasonGroupList.length - 1].id) {
                    // @ts-ignore
                    setSelectedSeason(seasonGroupList[seasonGroupList.length - 1].id.toString());
                }
            }
        }, [seasonGroupList]);

        const getCurrentSeasonColumns = (): IImportedColumn[] => {
            return mappedColumns.get(selectedSeason.toString()) ?? [];
        };

        const sendResults = (columns?: Map<string, IImportedColumn[]>) => {
            if (props.onMappingDone) {
                props.onMappingDone(columns);
            }
        };

        const onCancelMappings = () => {
            sendResults(undefined);
        };

        const onSaveMappings = () => {
            if (unmappedColumns.length) {
                setIsConfirmationWindowOpen(true);
            } else {
                sendResults(mappedColumns);
            }
        };

        const onSeasonGroupEditorClosed = (seasonGroup?: ISeasonGroup) => {
            if (seasonGroup) {
                addSeasonGroup(seasonGroup);
            }

            setIsSeasonGroupEditorOpen(false);
        };

        const onDragEnd = (result: DropResult) => {
            const { source, destination } = result;

            // dropped outside the list
            if (!destination) {
                return;
            }

            const sourceId = source.droppableId;
            const destinationId = destination.droppableId;
            const selectedSeasonId = selectedSeason.toString();

            if (sourceId !== destinationId) {
                if (sourceId === 'unmappedBox') {
                    // Mapping a column to a season
                    const sourceClone = Array.from(unmappedColumns);
                    const destinationClone = Array.from(mappedColumns.get(selectedSeasonId) ?? []);

                    const [removed] = sourceClone.splice(source.index, 1);

                    destinationClone.splice(destination.index, 0, removed);

                    setUnmappedColumns([...sourceClone]);
                    setMappedColumns(
                        new Map(mappedColumns.set(selectedSeasonId, destinationClone))
                    );
                } else {
                    // Dissociating a column from a season
                    const sourceClone = Array.from(mappedColumns.get(selectedSeasonId) ?? []);
                    const destinationClone = Array.from(unmappedColumns);

                    const [removed] = sourceClone.splice(source.index, 1);

                    destinationClone.splice(destination.index, 0, removed);

                    setUnmappedColumns([...destinationClone]);
                    setMappedColumns(new Map(mappedColumns.set(selectedSeasonId, sourceClone)));
                }
            }
        };

        return (
            <Dialog open={props.isOpen} maxWidth={'md'} fullWidth={true}>
                <DialogTitle>Map Imported Data</DialogTitle>
                <DialogContent dividers>
                    <DragDropContext onDragEnd={onDragEnd}>
                        <Grid container spacing={2}>
                            <Grid item xs>
                                <div style={{ display: 'flex', flexDirection: 'column' }}>
                                    <Alert severity={'info'}>
                                        <AlertTitle>Assign Data Columns to Seasons</AlertTitle>
                                        Drag <b>column names</b> from the left to the season box on
                                        the right to map them to that season.
                                    </Alert>
                                    <Droppable key={0} droppableId={'unmappedBox'}>
                                        {(provided, snapshot) => (
                                            <Paper
                                                component='ul'
                                                ref={provided.innerRef}
                                                style={getUnmappedListStyle(
                                                    snapshot.isDraggingOver
                                                )}
                                                {...provided.droppableProps}
                                            >
                                                {unmappedColumns.map((value, index, array) => {
                                                    return (
                                                        <Draggable
                                                            key={value.id}
                                                            draggableId={value.id}
                                                            index={index}
                                                        >
                                                            {(provided, snapshot) => (
                                                                <li key={value.id}>
                                                                    <Chip
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                        label={`${value.name}`}
                                                                        style={getItemStyle(
                                                                            snapshot.isDragging,
                                                                            provided.draggableProps
                                                                                .style
                                                                        )}
                                                                    />
                                                                </li>
                                                            )}
                                                        </Draggable>
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </Paper>
                                        )}
                                    </Droppable>
                                </div>
                            </Grid>
                            <Grid item xs style={{ display: 'flex', flexDirection: 'column' }}>
                                <Grid container>
                                    <Grid item xs>
                                        <FormControl fullWidth={true}>
                                            <InputLabel shrink>Select a target season</InputLabel>
                                            <Select
                                                value={selectedSeason}
                                                disabled={selectedSeason === ''}
                                                onChange={(event) => {
                                                    setSelectedSeason(event.target.value as string);
                                                }}
                                                displayEmpty
                                            >
                                                {seasonGroupList.map((season) => {
                                                    return (
                                                        <MenuItem key={season.id} value={season.id}>
                                                            {season.name}
                                                        </MenuItem>
                                                    );
                                                })}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid
                                        item
                                        style={{
                                            marginTop: 'auto',
                                        }}
                                    >
                                        <Tooltip title={'Add a new season'}>
                                            <Button
                                                variant={'outlined'}
                                                style={{
                                                    marginLeft: '5px',
                                                }}
                                                onClick={() => setIsSeasonGroupEditorOpen(true)}
                                            >
                                                <Add fontSize={'small'} />
                                            </Button>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                <Droppable
                                    key={0}
                                    droppableId={'mappedBox'}
                                    isDropDisabled={selectedSeason === ''}
                                >
                                    {(provided, snapshot) => (
                                        <Paper
                                            component='ul'
                                            ref={provided.innerRef}
                                            style={getMappedListStyle(snapshot.isDraggingOver)}
                                            {...provided.droppableProps}
                                        >
                                            {getCurrentSeasonColumns().map(
                                                (value, index, array) => {
                                                    return (
                                                        <Draggable
                                                            key={value.id}
                                                            draggableId={value.id}
                                                            index={index}
                                                        >
                                                            {(provided, snapshot) => (
                                                                <li key={value.id}>
                                                                    <Chip
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                        label={`${value.name}`}
                                                                        style={getItemStyle(
                                                                            snapshot.isDragging,
                                                                            provided.draggableProps
                                                                                .style
                                                                        )}
                                                                    />
                                                                </li>
                                                            )}
                                                        </Draggable>
                                                    );
                                                }
                                            )}
                                            {provided.placeholder}
                                        </Paper>
                                    )}
                                </Droppable>
                            </Grid>
                        </Grid>
                    </DragDropContext>
                    <SeasonGroupEditorDialog
                        isOpen={isSeasonGroupEditorOpen}
                        onClose={onSeasonGroupEditorClosed}
                    />
                    <Dialog open={isConfirmationWindowOpen}>
                        <DialogTitle>Unmapped Fields Remain</DialogTitle>
                        <DialogContent>
                            <Alert severity={'warning'}>
                                <AlertTitle>
                                    There are still fields that have not been mapped to a rental
                                    season. Unmapped field data will not be imported!
                                </AlertTitle>
                                Are you sure you want to continue?
                            </Alert>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                autoFocus
                                onClick={() => setIsConfirmationWindowOpen(false)}
                                color='primary'
                            >
                                No
                            </Button>
                            <Button
                                autoFocus
                                onClick={() => {
                                    setIsConfirmationWindowOpen(false);
                                    sendResults(mappedColumns);
                                }}
                                color='primary'
                            >
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={onCancelMappings} color='primary'>
                        Cancel Import
                    </Button>
                    <Button autoFocus onClick={onSaveMappings} color='primary'>
                        Save And Close
                    </Button>
                </DialogActions>
            </Dialog>
        );
    };

    return (
        <Suspense fallback={<CircularProgress />}>
            <MapSeasonsDialogContent />
        </Suspense>
    );
};
