import React from 'react';
import { DropEvent, FileRejection, useDropzone } from 'react-dropzone';
import {
    CircularProgress,
    Dialog,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from '@material-ui/core';

const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 5,
    borderColor: '#d2d2d2',
    borderStyle: 'dashed',
    backgroundColor: '#f5f4f4',
    color: '#757575',
    outline: 'none',
    transition: 'border .24s ease-in-out',
};

const activeStyle = {
    borderColor: '#2196f3',
};

const acceptStyle = {
    borderColor: '#00e676',
};

const rejectStyle = {
    borderColor: '#ff1744',
};

interface IDropZoneProps {
    isDialogOpen?: boolean;
    dialogTitle?: string;
    dialogMessage?: string;
    showLoadingIndicator?: boolean;
    preventManualClose?: boolean;
    dropZoneRenderer?: () => React.ReactNode;
    onFileReady?: (file: File[]) => Promise<boolean | string | undefined>;
}

export const CsvDropZone: React.FC<IDropZoneProps> = (props) => {
    const [dialogProps, setDialogProps] = React.useState<IDropZoneProps>({
        isDialogOpen: props.isDialogOpen ?? false,
        dialogTitle: props.dialogTitle ?? '',
        dialogMessage: props.dialogMessage ?? undefined,
        showLoadingIndicator: props.showLoadingIndicator ?? false,
        preventManualClose: props.preventManualClose ?? false,
    });

    const onFileDropRejected = (files: FileRejection[], event: DropEvent) => {
        setDialogProps({
            isDialogOpen: true,
            dialogTitle: 'File Type Invalid',
            dialogMessage: `The file type provided is not supported. Please provide a CSV file.`,
            showLoadingIndicator: false,
        });
    };

    const onFileDropAccepted = async (files: File[], event: DropEvent) => {
        if (files.length === 0) {
            setDialogProps({
                isDialogOpen: true,
                dialogTitle: 'No Files Read',
                showLoadingIndicator: true,
                preventManualClose: false,
            });
        }

        setDialogProps({
            isDialogOpen: true,
            dialogTitle: 'Processing Files',
            showLoadingIndicator: true,
            preventManualClose: true,
        });

        if (props.onFileReady) {
            const readResult = await props.onFileReady(files);
            if (readResult === true) {
                setDialogProps({
                    isDialogOpen: false,
                    dialogTitle: 'Processing Files',
                    showLoadingIndicator: true,
                    preventManualClose: true,
                });
            } else {
                setDialogProps({
                    isDialogOpen: true,
                    dialogTitle: 'Failed To Read File',
                    preventManualClose: false,
                    dialogMessage: String(readResult),
                });
            }
        }
    };

    const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
        accept: '.csv, application/vnd.ms-excel, text/csv',
        onDropAccepted: onFileDropAccepted,
        onDropRejected: onFileDropRejected,
        multiple: false,
    });

    const style = React.useMemo(
        () => ({
            ...baseStyle,
            ...(isDragActive ? activeStyle : {}),
            ...(isDragAccept ? acceptStyle : {}),
            ...(isDragReject ? rejectStyle : {}),
        }),
        [isDragActive, isDragReject, isDragAccept]
    );

    return (
        <div className='container'>
            {/** @ts-ignore */}
            <div {...getRootProps({ style })}>
                <input {...getInputProps()} />
                {props.dropZoneRenderer && props.dropZoneRenderer()}
                {!props.dropZoneRenderer && (
                    <>
                        <p
                            style={{
                                marginTop: 0,
                            }}
                        >
                            Drop Files Here
                        </p>
                        <i>Only (.csv) files supported</i>
                    </>
                )}
            </div>
            <Dialog
                fullWidth={true}
                open={dialogProps.isDialogOpen ?? false}
                onEscapeKeyDown={
                    !dialogProps.preventManualClose
                        ? () => {
                              setDialogProps({
                                  ...dialogProps,
                                  isDialogOpen: false,
                              });
                          }
                        : undefined
                }
                onBackdropClick={
                    !dialogProps.preventManualClose
                        ? () => {
                              setDialogProps({
                                  ...dialogProps,
                                  isDialogOpen: false,
                              });
                          }
                        : undefined
                }
                aria-labelledby='max-width-dialog-title'
            >
                <DialogTitle id='max-width-dialog-title'>
                    {' '}
                    {dialogProps.showLoadingIndicator && (
                        <CircularProgress size={20} style={{ marginRight: '10px' }} />
                    )}
                    {dialogProps.dialogTitle}
                </DialogTitle>
                {dialogProps.dialogMessage && (
                    <DialogContent>
                        <DialogContentText>{dialogProps.dialogMessage}</DialogContentText>
                    </DialogContent>
                )}
            </Dialog>
        </div>
    );
};
