import { ISeasonGroup } from 'model/Configuration';
import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { SeasonSelectionPage } from './SeasonSelectionPage';

export interface ILinkSeasonDialogProps {
    isOpen?: boolean;
    onClosed?: () => void;
    onSubmit?: (clientData: ISeasonGroup) => void;
    onAddNew?: () => void;
}

export const LinkSeasonDialog: React.FC<ILinkSeasonDialogProps> = (props) => {
    const onAddSeasonSelected = () => {
        if (props.onAddNew) {
            props.onAddNew();
        }
    };

    const onExistingSeasonSelected = (season: ISeasonGroup) => {
        if (props.onSubmit && season) {
            props.onSubmit(season);
        }
    };

    return (
        <Dialog
            fullWidth={true}
            open={props.isOpen ?? false}
            aria-labelledby='max-width-dialog-title'
            onEscapeKeyDown={props.onClosed}
        >
            <DialogTitle id='max-width-dialog-title'>Link Existing Season</DialogTitle>
            <DialogContent
                style={{
                    overflowY: 'hidden',
                }}
            >
                <SeasonSelectionPage
                    onSeasonSelected={onExistingSeasonSelected}
                    onSeasonAdded={onAddSeasonSelected}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClosed}>Cancel</Button>
            </DialogActions>
        </Dialog>
    );
};
