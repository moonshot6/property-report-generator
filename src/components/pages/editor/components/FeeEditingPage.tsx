import React from 'react';
import { IClientFee } from 'model/Configuration';
import { Grid, TextField } from '@material-ui/core';

export interface IFeeEditingPageProps {
    defaultFee?: IClientFee;
    selectedClientId?: number;
    onFeeDataChanged?: (clientData: IClientFee) => void;
}

interface IClientFeeFields {
    name: string;
    value: string;
    description?: string;
}

const DEFAULT_CLIENT_FEE: IClientFeeFields = {
    name: '',
    value: '',
};

const getDefaultFeeFields = (defaultfee?: IClientFee) => {
    if (defaultfee) {
        if (defaultfee.descriptionId) {
            return {
                name: String(defaultfee.name),
                value: String(defaultfee.value),
                description: String(defaultfee.description),
            };
        } else {
            return {
                name: String(defaultfee.name),
                value: String(defaultfee.value),
            };
        }
    } else {
        return DEFAULT_CLIENT_FEE;
    }
};

export const FeeEditingPage: React.FC<IFeeEditingPageProps> = (props) => {
    const [feeData, setFeeData] = React.useState<IClientFeeFields>(
        getDefaultFeeFields(props.defaultFee)
    );

    React.useEffect(() => {
        setFeeData(getDefaultFeeFields(props.defaultFee));
    }, [props.defaultFee]);

    React.useEffect(() => {
        onSubmitted();
    }, [feeData]);

    const onSubmitted = () => {
        if (props.onFeeDataChanged && props.selectedClientId) {
            props.onFeeDataChanged({
                ...props.defaultFee,
                name: feeData.name,
                clientId: props.selectedClientId,
                description: feeData.description,
                value: Number.parseFloat(feeData.value),
            });
        }
    };

    const onInputKeyUp = (event: React.KeyboardEvent<HTMLDivElement>) => {
        if (event.key === 'Enter') {
            onSubmitted();
        }
    };

    return (
        <>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <TextField
                        id='outlined-basic'
                        label='Please enter a name for the fee'
                        variant='outlined'
                        fullWidth={true}
                        autoFocus={true}
                        value={feeData.name}
                        onChange={(event) => {
                            setFeeData({
                                ...feeData,
                                name: event.currentTarget.value,
                            });
                        }}
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id='outlined-basic'
                        label='Enter a value'
                        variant='outlined'
                        fullWidth={true}
                        value={feeData.value}
                        onChange={(event) => {
                            if (/^(?:[\d]+(?:[.]?[\d]*))?$/gm.test(event.currentTarget.value)) {
                                setFeeData({
                                    ...feeData,
                                    value: event.currentTarget.value,
                                });
                            }
                        }}
                    />
                </Grid>
            </Grid>
            <Grid
                container
                style={{
                    marginTop: '10px',
                }}
            >
                <Grid item xs>
                    <TextField
                        label='Add a description for this fee'
                        multiline
                        fullWidth={true}
                        rows={4}
                        defaultValue={feeData.description}
                        variant='outlined'
                        onKeyUp={onInputKeyUp}
                        onChange={(event) => {
                            if (/^(?:[\d]+(?:[.]?[\d]*))?$/gm.test(event.currentTarget.value)) {
                                setFeeData({
                                    ...feeData,
                                    description: event.currentTarget.value,
                                });
                            }
                        }}
                    />
                </Grid>
            </Grid>
        </>
    );
};
