import { Button, Grid, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React from 'react';
import { useAtom } from 'jotai';
import { ClientInfoDialog } from './ClientInfoDialog';
import { IClientInfo } from 'model/Configuration';
import { selectedClientAtom, useClientManager } from 'actions/Configuration';
import { Autocomplete } from '@material-ui/lab';

export const ClientList: React.FC = (props) => {
    const [isAddDialogOpen, setIsAddDialogOpen] = React.useState(false);
    const { clientList, addClient } = useClientManager();
    const [selectedClient, setSelectedClient] = useAtom<IClientInfo | null, number | undefined>(
        selectedClientAtom
    );

    const onAddIconClicked = () => {
        setIsAddDialogOpen(true);
    };

    const onClientAdded = async (clientData: IClientInfo) => {
        const newClientId = await addClient(clientData);
        setSelectedClient(newClientId);

        setIsAddDialogOpen(false);
    };

    React.useEffect(() => {
        if (selectedClient === undefined || (selectedClient === null && clientList.length > 0)) {
            setSelectedClient(clientList[0].id);
        }
    }, []);

    return (
        <div style={{ padding: 5, flexGrow: 1, display: 'flex' }}>
            <Grid container>
                <Grid item xs>
                    <Autocomplete
                        options={clientList}
                        disabled={clientList.length === 0}
                        getOptionLabel={(option) => option.name}
                        value={selectedClient}
                        onChange={(event, value) => {
                            if (value && value.id) {
                                setSelectedClient(value.id);
                            } else if (value === null) {
                                setSelectedClient(undefined);
                            }
                        }}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                size={'small'}
                                label='Enter or select a client'
                                variant='filled'
                            />
                        )}
                    />
                </Grid>
                <Grid
                    style={{
                        marginTop: 'auto',
                        marginBottom: 'auto',
                    }}
                >
                    <Button
                        variant='contained'
                        color='default'
                        startIcon={<AddIcon />}
                        onClick={onAddIconClicked}
                        style={{
                            height: '45px',
                            borderTopLeftRadius: 0,
                            borderBottomLeftRadius: 0,
                        }}
                    >
                        Add Client
                    </Button>
                </Grid>
            </Grid>

            <ClientInfoDialog
                isOpen={isAddDialogOpen}
                onClosed={() => setIsAddDialogOpen(false)}
                onSubmit={onClientAdded}
            />
        </div>
    );
};
