import React from 'react';
import { IClientFee } from 'model/Configuration';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Typography,
} from '@material-ui/core';

import { FeeSelectionPage } from './FeeSelectionPage';
import { FeeEditingPage } from './FeeEditingPage';
import { useAtom } from 'jotai';
import { selectedClientAtom } from '../../../../actions/Configuration';
import { Alert, AlertTitle } from '@material-ui/lab';

export interface IAddFeeDialogDialogProps {
    defaultFee?: IClientFee;
    isOpen?: boolean;
    onClosed?: () => void;
    onSubmit?: (clientData: IClientFee) => void;
}

export const AddFeeDialog: React.FC<IAddFeeDialogDialogProps> = (props) => {
    const [selectedClient] = useAtom(selectedClientAtom);
    const [feeData, setFeeData] = React.useState<IClientFee | undefined>(undefined);
    const [isEditOpen, setIsEditOpen] = React.useState<boolean>(false);

    const onAddButtonClicked = () => {
        if (props.onSubmit && feeData !== undefined) {
            props.onSubmit(feeData);
        }
    };

    const onFeeDataChanged = (feeData: IClientFee) => {
        setFeeData(feeData);
    };

    const onNewFeeClicked = () => {
        setIsEditOpen(true);
    };

    const onExistingFeeSelected = (fee: IClientFee) => {
        if (props.onSubmit && fee !== undefined) {
            props.onSubmit(fee);
        }
    };

    React.useEffect(() => {
        if (props.isOpen) {
            setIsEditOpen(props.defaultFee !== undefined);
        }
    }, [props.isOpen]);

    const renderDialogBody = () => {
        if (!selectedClient) {
            return (
                <Alert>
                    <AlertTitle>No Active Client Selected</AlertTitle>
                    <Typography variant={'body2'}>
                        A fee may not be added while a client is not selected. This is most likely
                        an error in the application.
                    </Typography>
                </Alert>
            );
        }

        if (isEditOpen) {
            return (
                <FeeEditingPage
                    defaultFee={props.defaultFee}
                    selectedClientId={selectedClient.id}
                    onFeeDataChanged={onFeeDataChanged}
                />
            );
        } else {
            return (
                <FeeSelectionPage
                    onFeeSelected={onExistingFeeSelected}
                    onNewFee={onNewFeeClicked}
                />
            );
        }
    };

    const renderDialogActions = () => {
        if (!selectedClient) {
            return <Button onClick={props.onClosed}>Cancel</Button>;
        }

        if (!isEditOpen) {
            return (
                <>
                    <Button onClick={props.onClosed}>Cancel</Button>
                    <Button onClick={onAddButtonClicked}>
                        {props.defaultFee === undefined ? 'Add' : 'Save'}
                    </Button>
                </>
            );
        } else {
            return (
                <>
                    <Button onClick={props.onClosed}>Cancel</Button>
                </>
            );
        }
    };

    return (
        <Dialog
            fullWidth={true}
            open={props.isOpen ?? false}
            aria-labelledby='max-width-dialog-title'
            onEscapeKeyDown={props.onClosed}
        >
            <DialogTitle id='max-width-dialog-title'>
                {props.defaultFee === undefined ? 'Add Fee' : 'Modify Fee'}
            </DialogTitle>
            <DialogContent
                style={{
                    overflowY: 'hidden',
                }}
            >
                {renderDialogBody()}
            </DialogContent>
            <DialogActions>{renderDialogActions()}</DialogActions>
        </Dialog>
    );
};
