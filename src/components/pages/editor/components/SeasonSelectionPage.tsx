import React, { Suspense } from 'react';
import {
    Avatar,
    Button,
    Card,
    CardActions,
    CardContent,
    Chip,
    FormControl,
    FormHelperText,
    Grid,
    LinearProgress,
    MenuItem,
    Select,
    Typography,
} from '@material-ui/core';
import { AddCircle } from '@material-ui/icons';
import { ISeasonGroup } from 'model/Configuration';
import { selectedClientAtom, useSeasonGroupManager } from 'actions/Configuration';
import { useAtom } from 'jotai';

interface ISeasonSelectionPage {
    onSeasonSelected?: (selectedSeason: ISeasonGroup) => void;
    onSeasonAdded?: () => void;
}

export const SeasonSelectionPage: React.FC<ISeasonSelectionPage> = (props) => {
    const { seasonGroupList } = useSeasonGroupManager();

    const [selectedClient] = useAtom(selectedClientAtom);
    const [existingSeasons, setExistingSeasons] = React.useState<React.ReactNodeArray | undefined>(
        undefined
    );
    const [selectedSeasonId, setSelectedSeasonId] = React.useState<number | undefined>(undefined);

    React.useEffect(() => {
        setSelectedSeasonId(undefined);
        setExistingSeasons(undefined);
        buildExistingSeasonList();
    }, [selectedClient]);

    const onAddExistingClicked = async () => {
        if (selectedSeasonId) {
            const existingSeason = seasonGroupList.find((season) => season.id === selectedSeasonId);

            if (existingSeason && props.onSeasonSelected) {
                props.onSeasonSelected(existingSeason);
            }
        }
    };

    const onAddNewClicked = () => {
        if (props.onSeasonAdded) {
            props.onSeasonAdded();
        }
    };

    const buildExistingSeasonList = () => {
        if (seasonGroupList.length && selectedClient && selectedClient.id) {
            const nonLinkedSeasons = seasonGroupList.filter(
                (season) => !season.assignedClientIds.includes(selectedClient.id as number)
            );

            if (nonLinkedSeasons.length) {
                setExistingSeasons(
                    nonLinkedSeasons.map((season) => {
                        return (
                            <MenuItem value={season.id}>
                                <Grid container spacing={3}>
                                    <Grid
                                        item
                                        xs
                                        style={{
                                            marginTop: 'auto',
                                            marginBottom: 'auto',
                                        }}
                                    >
                                        <Typography>{season.name}</Typography>
                                    </Grid>
                                    <Grid item spacing={2}>
                                        <Chip
                                            color={'primary'}
                                            variant={'outlined'}
                                            avatar={
                                                <Avatar
                                                    style={{ width: '45px', borderRadius: '10px' }}
                                                >
                                                    From:
                                                </Avatar>
                                            }
                                            label={season.startDate.toLocaleDateString()}
                                            style={{
                                                textAlign: 'right',
                                            }}
                                        />
                                        <Chip
                                            color={'primary'}
                                            variant={'outlined'}
                                            avatar={
                                                <Avatar
                                                    style={{ width: '30px', borderRadius: '10px' }}
                                                >
                                                    To:
                                                </Avatar>
                                            }
                                            label={season.endDate.toLocaleDateString()}
                                            style={{
                                                textAlign: 'right',
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </MenuItem>
                        );
                    })
                );
            }
        } else {
            return setExistingSeasons([<></>]);
        }
    };

    const Fallback = () => {
        return <LinearProgress />;
    };

    return (
        <Grid container spacing={3}>
            <Grid item xs={6}>
                <Card>
                    <CardContent>
                        <Suspense fallback={<Fallback />}>
                            <Typography variant={'subtitle2'}>From Existing Season</Typography>
                            <FormControl fullWidth={true}>
                                <Select
                                    displayEmpty
                                    variant={'standard'}
                                    value={selectedSeasonId}
                                    disabled={existingSeasons === undefined}
                                    renderValue={(value) => {
                                        const match = seasonGroupList.find(
                                            (season) => season.id === value
                                        );
                                        if (match) {
                                            return <>{match.name}</>;
                                        }
                                        return <></>;
                                    }}
                                    onChange={(event) => {
                                        setSelectedSeasonId(event.target.value as number);
                                    }}
                                >
                                    <MenuItem value='' disabled>
                                        Select an existing season
                                    </MenuItem>

                                    {existingSeasons === undefined ? <Fallback /> : existingSeasons}
                                </Select>
                                <FormHelperText>Select an existing season</FormHelperText>
                            </FormControl>
                        </Suspense>
                    </CardContent>
                    <CardActions>
                        <Button
                            variant={'contained'}
                            disabled={selectedSeasonId === undefined}
                            style={{
                                marginTop: 'auto',
                                marginLeft: 'auto',
                            }}
                            onClick={onAddExistingClicked}
                        >
                            <AddCircle
                                style={{
                                    color: 'rgb(159,210,127)',
                                    marginRight: '5px',
                                }}
                            />
                            <Typography variant={'subtitle2'}>Link Existing</Typography>
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
            <div
                style={{
                    position: 'absolute',
                    top: '50%',
                    left: '47%',
                    zIndex: 100,
                    padding: '7px 10px',
                    borderRadius: '100px',
                    backgroundColor: 'rgba(171,171,171)',
                }}
            >
                <Typography variant={'body1'}>
                    <b>OR</b>
                </Typography>
            </div>
            <Grid
                item
                xs={6}
                style={{
                    display: 'flex',
                }}
            >
                <Card
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        flexGrow: 1,
                    }}
                >
                    <CardContent
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            flexGrow: 1,
                        }}
                    >
                        <Typography variant={'subtitle2'}>Create A New Season</Typography>
                    </CardContent>
                    <CardActions>
                        <Button
                            variant={'contained'}
                            onClick={onAddNewClicked}
                            style={{
                                marginTop: 'auto',
                                marginLeft: 'auto',
                            }}
                        >
                            <AddCircle
                                style={{
                                    color: 'rgb(159,210,127)',
                                    marginRight: '5px',
                                }}
                            />
                            <Typography variant={'subtitle2'}>Add New Season</Typography>
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
        </Grid>
    );
};
