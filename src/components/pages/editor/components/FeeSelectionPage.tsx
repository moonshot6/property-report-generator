import React, { Suspense } from 'react';
import { db } from 'actions/ApplicationDatabase';
import {
    Button,
    Card,
    CardActions,
    CardContent,
    Chip,
    FormControl,
    FormHelperText,
    Grid,
    LinearProgress,
    MenuItem,
    Select,
    Typography,
} from '@material-ui/core';
import { AddCircle } from '@material-ui/icons';
import { IClientFee } from 'model/Configuration';

interface IFeeSelectionPageProps {
    onFeeSelected: (selectedFee: IClientFee) => void;
    onNewFee: () => void;
}

export const FeeSelectionPage: React.FC<IFeeSelectionPageProps> = (props) => {
    const [existingFees, setExistingFees] = React.useState<React.ReactNodeArray | undefined>(
        undefined
    );
    const [selectedFeeId, setSelectedFeeId] = React.useState<number | undefined>(undefined);

    React.useEffect(() => {
        setSelectedFeeId(undefined);
        setExistingFees(undefined);
        buildExistingFeeList();
    }, []);

    const onAddExistingClicked = async () => {
        if (selectedFeeId) {
            const existingFee = await db.fees.where('id').equals(selectedFeeId).first();

            if (existingFee && props.onFeeSelected) {
                props.onFeeSelected(existingFee);
            }
        }
    };

    const buildExistingFeeList = async () => {
        const existingFees = await db.fees.toArray();

        if (existingFees.length) {
            setExistingFees(
                existingFees.map((fee) => {
                    return (
                        <MenuItem value={fee.id}>
                            <Grid container>
                                <Grid
                                    item
                                    xs
                                    style={{
                                        marginTop: 'auto',
                                        marginBottom: 'auto',
                                    }}
                                >
                                    <Typography>{fee.name}</Typography>
                                </Grid>
                                <Grid item>
                                    <Chip
                                        color={'primary'}
                                        variant={'outlined'}
                                        label={`$${fee.value}`}
                                        style={{
                                            textAlign: 'right',
                                        }}
                                    />
                                </Grid>
                            </Grid>
                        </MenuItem>
                    );
                })
            );
        } else {
            return setExistingFees([<></>]);
        }
    };

    const Fallback = () => {
        return <LinearProgress />;
    };

    return (
        <Grid container spacing={3}>
            <Grid item xs={6}>
                <Card>
                    <CardContent>
                        <Suspense fallback={<Fallback />}>
                            <Typography variant={'subtitle2'}>From Existing Fee</Typography>
                            <FormControl fullWidth={true}>
                                <Select
                                    displayEmpty
                                    variant={'standard'}
                                    value={selectedFeeId}
                                    onChange={(event) => {
                                        setSelectedFeeId(event.target.value as number);
                                    }}
                                >
                                    <MenuItem value='' disabled>
                                        Select an existing fee
                                    </MenuItem>

                                    {existingFees === undefined ? <Fallback /> : existingFees}
                                </Select>
                                <FormHelperText>Select an existing fee</FormHelperText>
                            </FormControl>
                        </Suspense>
                    </CardContent>
                    <CardActions>
                        <Button
                            variant={'contained'}
                            disabled={selectedFeeId === undefined}
                            style={{
                                marginTop: 'auto',
                                marginLeft: 'auto',
                            }}
                            onClick={onAddExistingClicked}
                        >
                            <AddCircle
                                style={{
                                    color: 'rgb(159,210,127)',
                                    marginRight: '5px',
                                }}
                            />
                            <Typography variant={'subtitle2'}>Add Existing</Typography>
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
            <div
                style={{
                    position: 'absolute',
                    top: '50%',
                    left: '47%',
                    zIndex: 100,
                    padding: '7px 10px',
                    borderRadius: '100px',
                    backgroundColor: 'rgba(171,171,171)',
                }}
            >
                <Typography variant={'body1'}>
                    <b>OR</b>
                </Typography>
            </div>
            <Grid
                item
                xs={6}
                style={{
                    display: 'flex',
                }}
            >
                <Card
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        flexGrow: 1,
                    }}
                >
                    <CardContent
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            flexGrow: 1,
                        }}
                    >
                        <Typography variant={'subtitle2'}>Create A New Fee</Typography>
                    </CardContent>
                    <CardActions>
                        <Button
                            variant={'contained'}
                            style={{
                                marginTop: 'auto',
                                marginLeft: 'auto',
                            }}
                            onClick={props.onNewFee}
                        >
                            <AddCircle
                                style={{
                                    color: 'rgb(159,210,127)',
                                    marginRight: '5px',
                                }}
                            />
                            <Typography variant={'subtitle2'}>Add New Fee</Typography>
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
        </Grid>
    );
};
