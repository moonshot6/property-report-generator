import {
    Button,
    Chip,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    InputAdornment,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TableRow,
    TextField,
    Tooltip,
    Typography,
} from '@material-ui/core';
import React from 'react';
import { useAtom } from 'jotai';
import { IClientInfo, IDailyRatesData, ISeasonGroup } from 'model/Configuration';
import {
    selectedClientAtom,
    selectedClientDailyRatesAtom,
    settingsControlAtom,
    useSeasonGroupManager,
} from 'actions/Configuration';
import BuildIcon from '@material-ui/icons/Build';
import CloseIcon from '@material-ui/icons/Close';
import EditIcon from '@material-ui/icons/Edit';
import ErrorIcon from '@material-ui/icons/Error';
import LinkIcon from '@material-ui/icons/Link';
import LinkOffIcon from '@material-ui/icons/LinkOff';
import { LinkSeasonDialog } from './LinkSeasonDialog';
import { db } from '../../../../actions/ApplicationDatabase';

export const ClientSeasonEditor = () => {
    const [selectedClient] = useAtom<IClientInfo | null, number | undefined>(selectedClientAtom);
    const [, updateDailyRates] = useAtom<IDailyRatesData | undefined, null>(
        selectedClientDailyRatesAtom
    );
    const [, setAppSettings] = useAtom(settingsControlAtom);
    const { seasonGroupList, removeClientFromGroup, addClientToGroup } = useSeasonGroupManager();

    const [isSeasonDialogOpen, setIsSeasonDialogOpen] = React.useState<boolean>(false);

    const onAddToSeasonGroup = (seasonGroup: ISeasonGroup) => {
        if (seasonGroup.id && selectedClient && selectedClient.id) {
            addClientToGroup(seasonGroup, selectedClient.id);
            setIsSeasonDialogOpen(false);
        }
    };

    const onAddNewSeasonGroup = () => {
        setIsSeasonDialogOpen(false);
        setAppSettings({
            activePage: 0,
            isSettingsOpen: true,
        });
    };

    const selectedClientSeasonList = React.useMemo(() => {
        if (selectedClient && selectedClient.id) {
            return seasonGroupList.filter((group) => {
                return group.assignedClientIds.includes(selectedClient.id as number);
            });
        }
        return [];
    }, [selectedClient, seasonGroupList]);

    const SeasonTableRow: React.FC<{ season: ISeasonGroup }> = (props) => {
        const [seasonRate, setSeasonRate] = React.useState<number | undefined>(undefined);
        const [
            isEditSeasonDailyRateDialogOpen,
            setIsSeasonDailyRateEditorOpen,
        ] = React.useState<boolean>(false);
        const [seasonRateMissing, setSeasonRateMissing] = React.useState<boolean>(false);
        const [seasonRateMissingText, setSeasonRateMissingText] = React.useState<string>('');

        React.useEffect(() => {
            getSeasonDailyRate().then((rate) => {
                setSeasonRate(rate);

                if (rate === undefined) {
                    // No season rate data for this row!!!
                    setSeasonRateMissing(true);
                }
            });
        }, []);

        React.useEffect(() => {
            if (seasonRate) {
                setSeasonRateMissingText(seasonRate.toFixed(2).toString());
            }
        }, [seasonRate]);

        const getSeasonDailyRate = async () => {
            if (selectedClient && selectedClient.id) {
                const dailyRatesData = await db.dailyRatesData
                    .where('clientId')
                    .equals(selectedClient.id)
                    .first();

                if (dailyRatesData) {
                    const seasonRate = dailyRatesData.rates.find(
                        (seasonRatesData) => seasonRatesData.seasonId === props.season.id
                    );

                    if (seasonRate) {
                        return seasonRate.rate;
                    }
                }
            }

            return undefined;
        };

        const onFixSeasonGroupIssuesClicked = async () => {
            setIsSeasonDailyRateEditorOpen(true);
        };

        const onSeasonGroupRateEditorClosed = () => {
            setIsSeasonDailyRateEditorOpen(false);
        };

        const onSaveSeasonRateClicked = async () => {
            onSeasonGroupRateEditorClosed();

            if (selectedClient && selectedClient.id && props.season.id) {
                const seasonRates = await db.dailyRatesData
                    .where('clientId')
                    .equals(selectedClient.id)
                    .first();

                const newRate = Number.parseInt(seasonRateMissingText);

                if (seasonRates && seasonRates.id) {
                    const rateIndex = seasonRates.rates.findIndex(
                        (seasonRatesData) => seasonRatesData.seasonId === props.season.id
                    );

                    if (rateIndex >= 0) {
                        seasonRates.rates[rateIndex].rate = newRate;
                    } else {
                        seasonRates.rates.push({
                            seasonId: props.season.id,
                            rate: newRate,
                        });
                    }

                    await db.dailyRatesData.update(seasonRates.id, {
                        ...seasonRates,
                    });

                    setSeasonRate(newRate);
                    setSeasonRateMissing(false);
                } else {
                    // No season rates exist for this client, add one
                    db.dailyRatesData.add({
                        id: undefined,
                        clientId: selectedClient.id,
                        rates: [
                            {
                                seasonId: props.season.id,
                                rate: newRate,
                            },
                        ],
                    });

                    setSeasonRate(newRate);
                    setSeasonRateMissing(false);
                }
            }

            updateDailyRates(null);
        };

        const onRemoveFromSeasonGroupClicked = async () => {
            if (selectedClient && selectedClient.id && props.season.id) {
                removeClientFromGroup(props.season, selectedClient.id);
                updateDailyRates(null);
            }
        };

        return (
            <TableRow>
                <TableCell component='th' scope='row'>
                    <Typography variant='body1'>{props.season.name}</Typography>
                </TableCell>
                <TableCell align='right'>{
                    props.season.startDate.toLocaleDateString(undefined, {
                        month: 'short',
                        day: 'numeric',
                    })}</TableCell>
                <TableCell align='right'>{
                    props.season.endDate.toLocaleDateString(undefined, {
                        month: 'short',
                        day: 'numeric'
                    })}</TableCell>
                <TableCell align='right'>{`${props.season.minimumStayLength} night(s)`}</TableCell>
                <TableCell align='right'>
                    {!seasonRate && !seasonRateMissing && (
                        <Chip label={<CircularProgress size={15} style={{ marginTop: '5px' }} />} />
                    )}
                    {!seasonRate && seasonRateMissing && (
                        <Chip
                            avatar={<ErrorIcon style={{ color: 'red' }} />}
                            label={'Data Missing'}
                        />
                    )}
                    {seasonRate && <Chip variant={'default'} label={`$${seasonRate.toFixed(2)}`} />}
                </TableCell>
                <TableCell align={'right'}>
                    {seasonRateMissing && (
                        <Tooltip title={'Fix issues'}>
                            <IconButton size={'small'} onClick={onFixSeasonGroupIssuesClicked}>
                                <BuildIcon />
                            </IconButton>
                        </Tooltip>
                    )}
                    {!seasonRateMissing && (
                        <Tooltip title={'Edit rate'}>
                            <IconButton size={'small'} onClick={onFixSeasonGroupIssuesClicked}>
                                <EditIcon />
                            </IconButton>
                        </Tooltip>
                    )}
                    <Tooltip title={'Remove from group'}>
                        <IconButton size={'small'} onClick={onRemoveFromSeasonGroupClicked}>
                            <LinkOffIcon />
                        </IconButton>
                    </Tooltip>
                </TableCell>
                <Dialog
                    open={isEditSeasonDailyRateDialogOpen}
                    onClose={onSeasonGroupRateEditorClosed}
                    disableBackdropClick={true}
                    fullWidth={true}
                    maxWidth={'xs'}
                >
                    <DialogTitle>
                        <Typography variant='h6'>Set Season Daily Rate</Typography>
                        <IconButton
                            aria-label='close'
                            style={{ position: 'absolute', right: 5, top: 5 }}
                            onClick={onSeasonGroupRateEditorClosed}
                        >
                            <CloseIcon />
                        </IconButton>
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            label={`Enter a daily rate for ${props.season.name}`}
                            fullWidth
                            margin='normal'
                            value={seasonRateMissingText}
                            onChange={(event) => {
                                if (/^([\d]+([.][\d]{0,})?)?$/.test(event.currentTarget.value)) {
                                    setSeasonRateMissingText(event.currentTarget.value);
                                }
                            }}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            InputProps={{
                                startAdornment: <InputAdornment position='start'>$</InputAdornment>,
                            }}
                            variant='outlined'
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button color='primary' onClick={onSeasonGroupRateEditorClosed}>
                            Cancel
                        </Button>
                        <Button
                            color='primary'
                            onClick={onSaveSeasonRateClicked}
                            disabled={!seasonRateMissingText || seasonRateMissingText.length === 0}
                        >
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </TableRow>
        );
    };

    const renderFields = () => {
        if (selectedClient === undefined) {
            return (
                <TableRow>
                    <TableCell component='th' scope='row' colSpan={5}>
                        <Typography variant='caption'>
                            Please select a client from the list above.
                        </Typography>
                    </TableCell>
                </TableRow>
            );
        }

        if (selectedClientSeasonList.length === 0) {
            return (
                <TableRow>
                    <TableCell component='th' scope='row' colSpan={5}>
                        <Typography variant='caption'>
                            No associated seasons found for client. Click the "Link Season" button
                            to pick one.
                        </Typography>
                    </TableCell>
                </TableRow>
            );
        }

        return selectedClientSeasonList.map((season: ISeasonGroup, index: number) => {
            return <SeasonTableRow season={season} />;
        });
    };

    return (
        <>
            <TableContainer component={Paper}>
                <Table size='small' aria-label='a dense table'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Season Name</TableCell>
                            <TableCell align='right'>Start Date</TableCell>
                            <TableCell align='right'>End Date</TableCell>
                            <TableCell align='right'>Minimum Stay</TableCell>
                            <TableCell align='right'>Daily Rate</TableCell>
                            <TableCell align='right'>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>{renderFields()}</TableBody>
                    <TableFooter>
                        <TableRow>
                            <TableCell colSpan={6} align={'right'}>
                                <Button
                                    disabled={selectedClient === undefined}
                                    variant='contained'
                                    color='default'
                                    startIcon={<LinkIcon />}
                                    onClick={() => {
                                        setIsSeasonDialogOpen(true);
                                    }}
                                >
                                    Link Season
                                </Button>
                            </TableCell>
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <LinkSeasonDialog
                isOpen={isSeasonDialogOpen}
                onClosed={() => setIsSeasonDialogOpen(false)}
                onSubmit={onAddToSeasonGroup}
                onAddNew={onAddNewSeasonGroup}
            />
        </>
    );
};
