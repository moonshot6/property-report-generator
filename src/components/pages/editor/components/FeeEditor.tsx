import React from 'react';
import { useAtom } from 'jotai';
import { IClientFee, IClientInfo } from 'model/Configuration';
import { selectedClientAtom, useFeeManager } from 'actions/Configuration';
import {
    Button,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
} from '@material-ui/core';
import QueueIcon from '@material-ui/icons/Queue';
import DeleteIcon from '@material-ui/icons/Delete';
import { Edit as EditIcon } from '@material-ui/icons';
import { AddFeeDialog } from './AddFeeDialog';

export const FeeEditor: React.FC = (props) => {
    const { feeList, addFee, updateFee, deleteFee } = useFeeManager();
    const [selectedClient] = useAtom<IClientInfo | null, number>(selectedClientAtom);

    const [feeToModify, setFeeToModify] = React.useState<IClientFee | undefined>(undefined);
    const [isAddFeeDialogOpen, setIsFeeDialogOpen] = React.useState<boolean>(false);

    const renderFields = () => {
        if (selectedClient === undefined) {
            return (
                <TableRow>
                    <TableCell component='th' scope='row' colSpan={3}>
                        <Typography variant='caption'>
                            Please select a client from the list above.
                        </Typography>
                    </TableCell>
                </TableRow>
            );
        }

        if (feeList.length === 0) {
            return (
                <TableRow>
                    <TableCell component='th' scope='row' colSpan={3}>
                        <Typography variant='caption'>
                            No fees found for client. Click the "Add Fee" button to create one.
                        </Typography>
                    </TableCell>
                </TableRow>
            );
        }

        return feeList.map((fee: IClientFee, index: number) => {
            const onEditFeeClicked = () => {
                setFeeToModify(fee);
                setIsFeeDialogOpen(true);
            };

            const onFeeDeleted = async () => {
                if (selectedClient && selectedClient.id && fee.id) {
                    await deleteFee(selectedClient.id, fee.id);
                }
            };

            return (
                <TableRow>
                    <TableCell component='th' scope='row'>
                        <Typography variant='body1'>{fee.name}</Typography>
                    </TableCell>
                    <TableCell align='right'>{`$${fee.value}`}</TableCell>
                    <TableCell align={'right'}>
                        <Tooltip title={'Edit this item'}>
                            <IconButton size={'small'} onClick={onEditFeeClicked}>
                                <EditIcon />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title={'Delete this item'}>
                            <IconButton size={'small'} onClick={onFeeDeleted}>
                                <DeleteIcon />
                            </IconButton>
                        </Tooltip>
                    </TableCell>
                </TableRow>
            );
        });
    };

    const onFeeAdded = async (fee: IClientFee) => {
        if (selectedClient) {
            await addFee(selectedClient, fee);
        }

        setIsFeeDialogOpen(false);
    };

    const onFeeModified = async (fee: IClientFee) => {
        setFeeToModify(undefined);

        if (selectedClient !== null) {
            await updateFee(selectedClient, fee);
        }

        setIsFeeDialogOpen(false);
    };

    return (
        <>
            <TableContainer component={Paper}>
                <Table size='small' aria-label='a dense table'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Fee Name</TableCell>
                            <TableCell align='right'>Value</TableCell>
                            <TableCell align='right'>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>{renderFields()}</TableBody>
                    <TableFooter>
                        <TableRow>
                            <TableCell colSpan={3} align={'right'}>
                                <Button
                                    disabled={selectedClient === undefined}
                                    variant='contained'
                                    color='default'
                                    startIcon={<QueueIcon />}
                                    onClick={() => {
                                        setFeeToModify(undefined);
                                        setIsFeeDialogOpen(true);
                                    }}
                                >
                                    Add Fee
                                </Button>
                            </TableCell>
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <AddFeeDialog
                defaultFee={feeToModify}
                isOpen={isAddFeeDialogOpen}
                onSubmit={feeToModify === undefined ? onFeeAdded : onFeeModified}
                onClosed={() => setIsFeeDialogOpen(false)}
            />
        </>
    );
};
