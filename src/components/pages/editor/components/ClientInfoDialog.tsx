import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
} from '@material-ui/core';

import { IClientInfo } from '../../../../model/Configuration';

export interface IClientInfoDialogProps {
    isOpen?: boolean;

    onClosed?: () => void;
    onSubmit?: (clientData: IClientInfo) => void;
}

const DEFAULT_CLIENT_DATA: IClientInfo = {
    id: -1,
    name: '',
};

export const ClientInfoDialog: React.FC<IClientInfoDialogProps> = (props) => {
    const [clientData, setClientData] = React.useState<IClientInfo>(DEFAULT_CLIENT_DATA);

    React.useEffect(() => {
        setClientData(DEFAULT_CLIENT_DATA);
    }, [props.isOpen]);

    const onAddButtonClicked = () => {
        if (props.onSubmit) {
            props.onSubmit(clientData);
        }
    };

    const onInputKeyUp = (event: React.KeyboardEvent<HTMLDivElement>) => {
        if (event.key === 'Enter') {
            onAddButtonClicked();
        }
    };

    return (
        <Dialog
            fullWidth={true}
            open={props.isOpen ?? false}
            aria-labelledby='max-width-dialog-title'
            onEscapeKeyDown={props.onClosed}
        >
            <DialogTitle id='max-width-dialog-title'>Add Client</DialogTitle>
            <DialogContent>
                <TextField
                    id='outlined-basic'
                    label='Please enter a client name'
                    variant='outlined'
                    fullWidth={true}
                    autoFocus={true}
                    value={clientData.name}
                    onKeyUp={onInputKeyUp}
                    onChange={(event) => {
                        setClientData({
                            ...clientData,
                            name: event.currentTarget.value,
                        });
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClosed}>Cancel</Button>
                <Button onClick={onAddButtonClicked}>Add</Button>
            </DialogActions>
        </Dialog>
    );
};
