import React, { Suspense } from 'react';
import {
    Card,
    CircularProgress,
    Container,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Alert, AlertTitle, Skeleton } from '@material-ui/lab';
import { ClientList } from './components/ClientList';
import { ClientSeasonEditor } from './components/ClientSeasonEditor';
import { FeeEditor } from './components/FeeEditor';
import {
    selectedClientAtom,
    useClientManager,
    useFeeManager,
    useSeasonGroupManager,
} from 'actions/Configuration';
import { useAtom } from 'jotai';

export const ClientEditingPage: React.FC = (props) => {
    const { clientList } = useClientManager();
    const [selectedClient] = useAtom(selectedClientAtom);

    const ClientListFallback = () => {
        return (
            <Card
                style={{
                    marginBottom: 10,
                }}
            >
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                    }}
                >
                    <Skeleton
                        animation={'wave'}
                        variant={'rect'}
                        width='80%'
                        height={50}
                        style={{
                            marginRight: 10,
                        }}
                    />
                    <Skeleton animation={'wave'} variant={'rect'} width='20%' height={50} />
                </div>
            </Card>
        );
    };

    const FeedEditorFallback = () => {
        return (
            <TableContainer component={Paper}>
                <Table size='small' aria-label='a dense table'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Fee Name</TableCell>
                            <TableCell align='right'>Value</TableCell>
                            <TableCell align='right'>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableCell>
                            <Skeleton variant={'rect'} />;
                        </TableCell>
                        <TableCell>
                            <Skeleton variant={'rect'} />;
                        </TableCell>
                        <TableCell>
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                }}
                            >
                                <Skeleton variant={'circle'} />
                                <Skeleton variant={'circle'} />
                            </div>
                        </TableCell>
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TableCell colSpan={3} align={'right'}>
                                <Skeleton variant={'rect'} />
                            </TableCell>
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        );
    };

    return (
        <>
            <Grid
                container
                spacing={2}
                style={{
                    paddingLeft: '10px',
                    paddingRight: '10px',
                }}
            >
                <Grid item sm={12}>
                    <Suspense fallback={<ClientListFallback />}>
                        <ClientList />
                    </Suspense>
                    {selectedClient && (
                        <>
                            <Suspense fallback={<CircularProgress />}>
                                <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                                    <ClientSeasonEditor />
                                </div>
                            </Suspense>
                            <Suspense fallback={<FeedEditorFallback />}>
                                <FeeEditor />
                            </Suspense>
                        </>
                    )}
                    {clientList.length === 0 && (
                        <Alert severity={'warning'}>
                            <AlertTitle>No clients found in database</AlertTitle>
                            Import clients by clicking the "Import Data" button at the top of the
                            window.
                        </Alert>
                    )}
                </Grid>
            </Grid>
        </>
    );
};
