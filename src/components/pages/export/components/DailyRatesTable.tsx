import React, { ReactNodeArray } from 'react';
import { useAtom } from 'jotai';
import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TableRow,
    Typography,
} from '@material-ui/core';
import {
    selectedClientAtom,
    selectedClientDailyRatesAtom,
    useSeasonGroupManager,
} from 'actions/Configuration';
import { db } from 'actions/ApplicationDatabase';
import { IDailyRatesData } from 'model/Configuration';

export const DailyRatesTable: React.FC = (props) => {
    const [selectedClient] = useAtom(selectedClientAtom);
    const [dailyRates] = useAtom<IDailyRatesData | undefined>(selectedClientDailyRatesAtom);
    const [dailyRatesRows, setDailyRatesRows] = React.useState<React.ReactNodeArray>([]);

    React.useEffect(() => {
        renderDailyRatesRows().then((rows) => {
            setDailyRatesRows(rows);
        });
    }, []);

    const renderDailyRatesRows = async () => {
        return new Promise<ReactNodeArray>(async (resolve, reject) => {
            const results: ReactNodeArray = [];

            if (dailyRates && selectedClient && selectedClient.id) {
                for (const rateInfo of dailyRates.rates) {
                    const selectedClientId = selectedClient.id;

                    const seasonData = await db.seasonGroups
                        .where('id')
                        .equals(rateInfo.seasonId)
                        .and((x) => x.assignedClientIds.includes(selectedClientId))
                        .first();

                    if (seasonData) {
                        results.push(
                            <TableRow>
                                <TableCell width={160}>
                                    <Typography variant={'body1'}>{seasonData?.name}</Typography>
                                    <Typography
                                        variant={'caption'}
                                    >{`(${seasonData?.startDate.toLocaleDateString()} - ${seasonData?.endDate.toLocaleDateString()})`}</Typography>
                                </TableCell>
                                <TableCell align={'center'}>{`$${rateInfo.rate.toFixed(
                                    2
                                )}`}</TableCell>
                                <TableCell
                                    align={'center'}
                                >{`${seasonData?.minimumStayLength} night(s)`}</TableCell>
                            </TableRow>
                        );
                    }
                }
            }

            resolve(results);
        });
    };

    return (
        <TableContainer
            component={Paper}
            style={{
                marginTop: '20px',
            }}
        >
            <Table size='small'>
                <TableHead>
                    <TableRow>
                        <TableCell align={'center'} colSpan={3}>
                            Seasonal Rates
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align={'left'}>Season</TableCell>
                        <TableCell align={'center'}>Average Daily Rate</TableCell>
                        <TableCell align={'center'}>Nightly Minimum</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>{dailyRatesRows}</TableBody>
            </Table>
        </TableContainer>
    );
};
