import React from 'react';
import { Bar, Line } from 'react-chartjs-2';
import { useAtom } from 'jotai';
import { selectedClientAdrAtom } from 'actions/Configuration';

export type AdrData = { x: Date; y: number | string };

interface IADRChartProps {}

const MONTH_NAMES = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

const SERIES_COLORS = ['#6CFF4F', '#FF984F', '#4F8AFF'];

export const ADRChart: React.FC<IADRChartProps> = (props) => {
    const [adrData] = useAtom(selectedClientAdrAtom);

    const getMonthLabels = () => {
        if (adrData.length > 0) {
            return adrData[0].monthData.map((dataPoint) => {
                return MONTH_NAMES[dataPoint.date.getMonth()];
            });
        }
    };

    const getDatasets = () => {
        const last1PlusProjected = adrData.filter((adrDataItem) => {
            const minYear = new Date().getFullYear() - 1;
            const maxYear = new Date().getFullYear() + 1;

            return adrDataItem.year >= minYear && adrDataItem.year <= maxYear;
        });

        return last1PlusProjected.map((data, index) => {
            const flatData = data.monthData.map((dataPoint) => {
                return { x: dataPoint.date, y: dataPoint.value } as AdrData;
            });

            return {
                backgroundColor: SERIES_COLORS[index],
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                label: data.year,
                order: 0,
                data: flatData,
            };
        });
    };

    return (
        <div style={{ width: '100%' }}>
            <Bar
                data={{
                    labels: getMonthLabels(),
                    datasets: getDatasets(),
                }}
                plugins={[
                    {
                        beforeInit: (chart: any, options: any) => {
                            chart.legend.afterFit = function () {
                                this.height = this.height + 10;
                            };
                        },
                    },
                ]}
                options={{
                    devicePixelRatio: 2,
                    animation: {
                        duration: 0,
                    },
                    plugins: {
                        // Change options for ALL labels of THIS CHART
                        datalabels: {
                            formatter: (value: any) => {
                                return `$${value.y.toFixed(0)}`;
                            },
                            display: 'auto',
                            anchor: 'end',
                            align: 'top',
                            offset: 5,
                        },
                    },
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    lineHeight: 2,
                                    callback: function (value: string) {
                                        return '$' + value;
                                    },
                                },
                            },
                        ],
                    },
                }}
            />
        </div>
    );
};
