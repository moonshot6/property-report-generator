import logo from 'images/taylormade-banner.png';
import React from 'react';
import { Card, CardContent, Container, Grid, Typography } from '@material-ui/core';
import { ADRChart } from './ADRChart';
import { OccupancyChart } from './OccupancyChart';
import { RevenueChart } from './RevenueChart';

import styles from 'App.module.css';
import { DailyRatesTable } from './DailyRatesTable';
import { ClientReportHeader } from './ClientReportHeader';
import { ClientReportFeesTable } from './ClientReportFeesTable';
import { Alert } from '@material-ui/lab';

export const ClientReport: React.FC = (props) => {
    return (
        <Container style={{ width: 900 }}>
            <Grid container>
                <Grid item xs>
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}
                    >
                        <img
                            alt={'Taylor Made Logo'}
                            src={logo}
                            style={{
                                width: '100%',
                                display: 'block',
                                marginLeft: 'auto',
                                marginRight: 'auto',
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <ClientReportHeader />
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <Card>
                        <CardContent>
                            <Typography variant={'body1'}>Average Rates for Property</Typography>
                            <Typography variant={'caption'}>Last 2 years and projected</Typography>
                            <ADRChart />
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={6}>
                    <Card>
                        <CardContent>
                            <Typography variant={'body1'}>
                                Average Occupancy for Property
                            </Typography>
                            <Typography variant={'caption'}>Last 2 years and projected</Typography>
                            <OccupancyChart />
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Grid container spacing={2} className={styles.pageDivide}>
                <Grid item xs>
                    <Card>
                        <CardContent>
                            <Typography variant={'body1'}>Average Revenue for Property</Typography>
                            <Typography variant={'caption'}>Last 2 years and projected</Typography>
                            <RevenueChart />
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Grid container spacing={1}>
                <Grid item xs>
                    <Grid container spacing={1}>
                        <Grid item>
                            <ClientReportFeesTable />
                        </Grid>
                        <Grid item xs>
                            <DailyRatesTable />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <Alert severity={'warning'}>
                        <Typography variant={'caption'}>
                            *Rates will vary based upon market demand. Most holidays are three-night
                            minimums if in a season other than summer. Holidays will have a longer
                            length of stay and a higher base rate than the season that it falls into
                            based on demand. (i.e. higher rates during holiday periods)
                        </Typography>
                    </Alert>
                </Grid>
            </Grid>
        </Container>
    );
};
