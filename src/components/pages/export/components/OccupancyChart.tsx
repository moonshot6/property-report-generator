import React from 'react';
import { Line } from 'react-chartjs-2';
import { useAtom } from 'jotai';
import { selectedClientOccupancyAtom } from 'actions/Configuration';
import { IDatedValue } from 'model/Configuration';

export type OccupancyData = { x: Date; y: number | string };

const MONTH_NAMES = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

const SERIES_COLORS = ['#6CFF4F', '#FF984F', '#4F8AFF'];

export const OccupancyChart: React.FC = (props) => {
    const [occupancyData] = useAtom(selectedClientOccupancyAtom);

    const getMonthLabels = () => {
        if (occupancyData.length > 0) {
            return occupancyData[0].occupancyData.map((dataPoint) => {
                return MONTH_NAMES[dataPoint.date.getMonth()];
            });
        }
    };

    const getDatasets = () => {
        const minimumPoints: OccupancyData[] = [];
        const mediumPoints: OccupancyData[] = [];
        const maxPoints: OccupancyData[] = [];

        if (occupancyData.length === 0) {
            return [];
        }

        const last1PlusProjected = occupancyData.filter((occupancyYear) => {
            const minYear = new Date().getFullYear() - 1;
            const maxYear = new Date().getFullYear() + 1;

            return occupancyYear.year >= minYear && occupancyYear.year <= maxYear;
        });

        // Find the largest data set
        let longestDataPointIndex = 0;
        for (let i = 0; i < last1PlusProjected.length; ++i) {
            if (last1PlusProjected[i].occupancyData.length > longestDataPointIndex) {
                longestDataPointIndex = last1PlusProjected[i].occupancyData.length;
            }
        }

        for (let dataPointIndex = 0; dataPointIndex < longestDataPointIndex; ++dataPointIndex) {
            const dataPoints: IDatedValue[] = [];

            for (let recordIndex = 0; recordIndex < last1PlusProjected.length; ++recordIndex) {
                if (dataPointIndex < last1PlusProjected[recordIndex].occupancyData.length) {
                    dataPoints.push(last1PlusProjected[recordIndex].occupancyData[dataPointIndex]);
                }
            }

            // Sort the data points from smallest to largest
            dataPoints.sort((a: IDatedValue, b: IDatedValue) => {
                return a.value - b.value;
            });

            // Add minimum min/ave/max points
            if (dataPoints.length === 1) {
                // Add only the middle point
                mediumPoints.push({
                    x: dataPoints[0].date,
                    y: dataPoints[0].value * 100,
                });
            } else if (dataPoints.length === 2) {
                minimumPoints.push({
                    x: dataPoints[0].date,
                    y: dataPoints[0].value * 100,
                });
                maxPoints.push({
                    x: dataPoints[1].date,
                    y: dataPoints[1].value * 100,
                });
            } else if (dataPoints.length === 3) {
                minimumPoints.push({
                    x: dataPoints[0].date,
                    y: dataPoints[0].value * 100,
                });
                mediumPoints.push({
                    x: dataPoints[1].date,
                    y: dataPoints[1].value * 100,
                });
                maxPoints.push({
                    x: dataPoints[2].date,
                    y: dataPoints[2].value * 100,
                });
            }
        }

        // Create the datasets
        return [
            {
                backgroundColor: 'transparent',
                borderColor: SERIES_COLORS[0],
                borderWidth: 1,
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                label: 'low',
                order: 0,
                data: minimumPoints,
            },
            {
                backgroundColor: 'transparent',
                borderColor: SERIES_COLORS[1],
                borderWidth: 1,
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                label: 'medium',
                order: 0,
                data: mediumPoints,
            },
            {
                backgroundColor: 'transparent',
                borderColor: SERIES_COLORS[2],
                borderWidth: 1,
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                label: 'high',
                order: 0,
                data: maxPoints,
            },
        ];
    };

    return (
        <div style={{ width: '100%' }}>
            <Line
                data={{
                    labels: getMonthLabels(),
                    datasets: getDatasets(),
                }}
                plugins={[
                    {
                        beforeInit: (chart: any, options: any) => {
                            chart.legend.afterFit = function () {
                                this.height = this.height + 10;
                            };
                        },
                    },
                ]}
                options={{
                    devicePixelRatio: 2,
                    animation: {
                        duration: 0,
                    },
                    plugins: {
                        // Change options for ALL labels of THIS CHART
                        datalabels: {
                            formatter: (value: any) => {
                                return `${value.y.toFixed(0)}%`;
                            },
                            display: 'auto',
                            align: 'top',
                            offset: 5,
                        },
                    },
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    min: 0,
                                    max: 100,
                                    lineHeight: 2,
                                    callback: function (value: string) {
                                        return value + '%';
                                    },
                                },
                            },
                        ],
                    },
                }}
            />
        </div>
    );
};
