import React from 'react';
import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { useFeeManager } from 'actions/Configuration';

export const ClientReportFeesTable = () => {
    const { feeList } = useFeeManager();

    const buildTableRows = () => {
        return feeList.map((fee) => {
            return (
                <TableRow>
                    <TableCell>{fee.name}</TableCell>
                    <TableCell>${fee.value}</TableCell>
                </TableRow>
            );
        });
    };

    return (
        <TableContainer
            component={Paper}
            style={{
                marginTop: '20px',
            }}
        >
            <Table size='small'>
                <TableHead>
                    <TableRow>
                        <TableCell align={'center'} colSpan={2}>
                            Property Fees
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell align={'left'}>Name</TableCell>
                        <TableCell align={'center'}>Rate</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>{buildTableRows()}</TableBody>
            </Table>
        </TableContainer>
    );
};
