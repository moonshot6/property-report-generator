import React from 'react';
import { Grid, Typography } from '@material-ui/core';

import { useAtom } from 'jotai';
import { IClientInfo } from 'model/Configuration';
import { selectedClientAtom } from 'actions/Configuration';

export const ClientReportHeader: React.FC = () => {
    const [selectedClient] = useAtom<IClientInfo | null>(selectedClientAtom);
    return (
        <Grid container spacing={2}>
            <Grid item xs>
                <Typography
                    variant={'h6'}
                    style={{
                        marginTop: '10px',
                        textAlign: 'center',
                    }}
                >
                    {selectedClient?.name}
                </Typography>
            </Grid>
        </Grid>
    );
};
