import React from 'react';
import { Line } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { useAtom } from 'jotai';
import { selectedClientRevenueAtom } from 'actions/Configuration';

export type RevenueData = { x: Date; y: number | string };

const MONTH_NAMES = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

const SERIES_COLORS = ['#6CFF4F', '#FF984F', '#4F8AFF'];

export const RevenueChart: React.FC = (props) => {
    const [revenueData] = useAtom(selectedClientRevenueAtom);

    const getMonthLabels = () => {
        if (revenueData.length > 0) {
            return revenueData[0].revenueData.map((dataPoint) => {
                return MONTH_NAMES[dataPoint.date.getMonth()];
            });
        }
    };

    const getDatasets = () => {
        const last1PlusProjected = revenueData.filter((revenueDataItem) => {
            const minYear = new Date().getFullYear() - 1;
            const maxYear = new Date().getFullYear() + 1;

            return revenueDataItem.year >= minYear && revenueDataItem.year <= maxYear;
        });

        return last1PlusProjected.map((data, index) => {
            const flatData = data.revenueData.map((dataPoint) => {
                return { x: dataPoint.date, y: dataPoint.value } as RevenueData;
            });

            return {
                backgroundColor: 'transparent',
                borderColor: SERIES_COLORS[index],
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                label: data.year,
                order: 0,
                data: flatData,
            };
        });
    };

    return (
        <div>
            <Line
                height={125}
                plugins={[
                    ChartDataLabels,
                    {
                        beforeInit: (chart: any, options: any) => {
                            chart.legend.afterFit = function () {
                                this.height = this.height + 10;
                            };
                        },
                    },
                ]}
                data={{
                    labels: getMonthLabels(),
                    datasets: getDatasets(),
                }}
                options={{
                    devicePixelRatio: 2,
                    animation: {
                        duration: 0,
                    },
                    plugins: {
                        // Change options for ALL labels of THIS CHART
                        datalabels: {
                            formatter: (value: any) => {
                                return `$${value.y.toFixed(2)}`;
                            },
                            display: 'auto',
                            align: 'top',
                            offset: 5,
                        },
                    },
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    lineHeight: 2,
                                    callback: function (value: string) {
                                        return '$' + value;
                                    },
                                },
                            },
                        ],
                    },
                }}
            />
        </div>
    );
};
