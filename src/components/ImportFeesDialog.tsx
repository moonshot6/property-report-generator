import React, { Suspense } from 'react';

import {
    Button,
    Checkbox,
    Chip,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    Grid,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    TextField,
    Tooltip,
    Typography,
} from '@material-ui/core';
import { useClientManager, useFeeManager } from '../actions/Configuration';
import { CsvDropZone } from './CsvDropZone';
import sampleFeeDataImage from '../images/sample-fee-data.png';
import { Alert, AlertTitle } from '@material-ui/lab';
import { IParsedFee, readFeesFile } from '../actions/Statistics';
import { db } from '../actions/ApplicationDatabase';
import { IClientFee, IClientInfo } from '../model/Configuration';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import { Add } from '@material-ui/icons';
import { IImportedColumn } from './MapSeasonsDialog';

interface IImportFeesDialogProps {
    isOpen: boolean;
    onClose?: () => void;
}

const addClient = async (client: IClientInfo): Promise<number> => {
    const matchingClient = await db.clients.where('name').equals(client.name).first();

    if (matchingClient && matchingClient.id) {
        return matchingClient.id;
    } else {
        return db.clients.put({ ...client, id: undefined });
    }
};

const addFee = async (client: IClientInfo, fee: IClientFee): Promise<number> => {
    let newDescriptionId = undefined;

    if (client.id === undefined) {
        return -1;
    }

    if (fee.description) {
        if (fee.descriptionId) {
            newDescriptionId = await db.feeDescriptions.update(fee.descriptionId, {
                description: fee.description,
            });
        } else {
            newDescriptionId = await db.feeDescriptions.put({
                id: undefined,
                description: fee.description,
            });
        }
    }

    const existingFee = await db.fees
        .where('name')
        .equals(fee.name)
        .and((x) => {
            return x.clientId === client.id;
        })
        .first();

    if (existingFee && existingFee.id) {
        const feeId = await db.fees.update(existingFee.id, {
            ...existingFee,
            value: fee.value,
        });

        return feeId;
    } else {
        const feeId = db.fees.put({
            ...fee,
            id: undefined,
            descriptionId: newDescriptionId,
            description: undefined,
            clientId: client.id,
        });

        return feeId;
    }
};

const grid = 8;

const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle,
});

const getUnmappedListStyle = (isDraggingOver: boolean) => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: grid,
    listStyle: 'none',
    height: '200px',
    overflow: 'auto',
});

const getMappedListStyle = (isDraggingOver: boolean) => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: grid,
    listStyle: 'none',
    flexGrow: 1,
    maxHeight: '250px',
    overflow: 'auto',
});

export interface IImportedFee {
    id: string;
    name: string;
}

const generateUniqueIds = (columnNames: string[]) => {
    return columnNames.map((value, index) => {
        return {
            id: `fee-${index}-${Date.now()}`,
            name: value,
        } as IImportedFee;
    });
};

interface IGroupAndSelectFeesPageProps {
    parsedFees: any;
    currentFeeList: IClientFee[];
    onFeeGroupsUpdated: (groups: Map<string, IImportedFee[]>) => void;
}

const GroupAndSelectFeesPage: React.FC<IGroupAndSelectFeesPageProps> = (props) => {
    const getFeeNames = () => {
        return Object.keys(props.parsedFees[0]).filter((x) => x !== 'name');
    };

    const [tempFeeList, setTempFeeList] = React.useState<string[]>(
        props.currentFeeList.map((fee) => fee.name)
    );

    const [ungroupedFees, setUngroupedFees] = React.useState<IImportedFee[]>(
        generateUniqueIds(getFeeNames())
    );
    const [groupedFees, setGroupedFees] = React.useState<Map<string, IImportedFee[]>>(
        new Map<string, IImportedFee[]>()
    );

    const [selectedFeeGroup, setSelectedFeeGroup] = React.useState<string>('');
    const [feeInputValue, setFeeInputValue] = React.useState<string>('');
    const [isFeeGroupEditorOpen, setIsFeeGroupEditorOpen] = React.useState<boolean>(false);

    const doesFeeGroupAlreadyExist = (name: string) => {
        if (name === '') {
            return false;
        }

        return (
            tempFeeList.find((fee) => {
                return fee.toLowerCase() === name.toLowerCase().trimEnd();
            }) !== undefined
        );
    };

    const getFeesFromActiveGroup = (): IImportedColumn[] => {
        return groupedFees.get(selectedFeeGroup.toString()) ?? [];
    };

    const onDragEnd = (result: DropResult) => {
        const { source, destination } = result;

        // dropped outside the list
        if (!destination) {
            return;
        }

        const sourceId = source.droppableId;
        const destinationId = destination.droppableId;
        const selectedSeasonId = selectedFeeGroup.toString();

        if (sourceId !== destinationId) {
            if (sourceId === 'unmappedBox') {
                // Mapping a column to a season
                const sourceClone = Array.from(ungroupedFees);
                const destinationClone = Array.from(groupedFees.get(selectedSeasonId) ?? []);

                const [removed] = sourceClone.splice(source.index, 1);

                destinationClone.splice(destination.index, 0, removed);

                setUngroupedFees([...sourceClone]);
                setGroupedFees(new Map(groupedFees.set(selectedSeasonId, destinationClone)));
            } else {
                // Dissociating a column from a season
                const sourceClone = Array.from(groupedFees.get(selectedSeasonId) ?? []);
                const destinationClone = Array.from(ungroupedFees);

                const [removed] = sourceClone.splice(source.index, 1);

                destinationClone.splice(destination.index, 0, removed);

                setUngroupedFees([...destinationClone]);
                setGroupedFees(new Map(groupedFees.set(selectedSeasonId, sourceClone)));
            }
        }
    };

    React.useEffect(() => {
        if (props.currentFeeList.length) {
            if (props.currentFeeList[0].id) {
                setSelectedFeeGroup(props.currentFeeList[0].id.toString());
            }
        }

        setUngroupedFees(generateUniqueIds(getFeeNames()));
        setGroupedFees(new Map<string, IImportedColumn[]>());
        setFeeInputValue('');
        setIsFeeGroupEditorOpen(false);
    }, []);

    React.useEffect(() => {
        if (isFeeGroupEditorOpen) {
            setFeeInputValue('');
        }
    }, [isFeeGroupEditorOpen]);

    React.useEffect(() => {
        if (props.onFeeGroupsUpdated) {
            props.onFeeGroupsUpdated(groupedFees);
        }
    }, [groupedFees]);

    return (
        <Grid container>
            <Grid item xs>
                <DragDropContext onDragEnd={onDragEnd}>
                    <Grid container spacing={2}>
                        <Grid item xs>
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                <Alert severity={'info'}>
                                    <AlertTitle>Group Your Fees</AlertTitle>
                                    Drag <b>fee names</b> from the left to the box on the right to
                                    group them.
                                </Alert>
                                <Droppable key={0} droppableId={'unmappedBox'}>
                                    {(provided, snapshot) => (
                                        <Paper
                                            component='ul'
                                            ref={provided.innerRef}
                                            style={getUnmappedListStyle(snapshot.isDraggingOver)}
                                            {...provided.droppableProps}
                                        >
                                            {ungroupedFees.map((value, index, array) => {
                                                return (
                                                    <Draggable
                                                        key={value.id}
                                                        draggableId={value.id}
                                                        index={index}
                                                    >
                                                        {(provided, snapshot) => (
                                                            <li key={value.id}>
                                                                <Chip
                                                                    ref={provided.innerRef}
                                                                    {...provided.draggableProps}
                                                                    {...provided.dragHandleProps}
                                                                    label={`${value.name}`}
                                                                    style={getItemStyle(
                                                                        snapshot.isDragging,
                                                                        provided.draggableProps
                                                                            .style
                                                                    )}
                                                                />
                                                            </li>
                                                        )}
                                                    </Draggable>
                                                );
                                            })}
                                            {provided.placeholder}
                                        </Paper>
                                    )}
                                </Droppable>
                            </div>
                        </Grid>
                        <Grid item xs style={{ display: 'flex', flexDirection: 'column' }}>
                            <Grid container>
                                <Grid item xs>
                                    <FormControl fullWidth={true}>
                                        <InputLabel shrink>Select a fee group name</InputLabel>
                                        <Select
                                            value={selectedFeeGroup}
                                            disabled={selectedFeeGroup === ''}
                                            onChange={(event) => {
                                                setSelectedFeeGroup(event.target.value as string);
                                            }}
                                            displayEmpty
                                        >
                                            {tempFeeList.map((fee) => {
                                                return (
                                                    <MenuItem key={fee} value={fee}>
                                                        {fee}
                                                    </MenuItem>
                                                );
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid
                                    item
                                    style={{
                                        marginTop: 'auto',
                                    }}
                                >
                                    <Tooltip title={'Add a new Fee Group'}>
                                        <Button
                                            variant={'outlined'}
                                            style={{
                                                marginLeft: '5px',
                                            }}
                                            onClick={() => setIsFeeGroupEditorOpen(true)}
                                        >
                                            <Add fontSize={'small'} />
                                        </Button>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                            <Droppable
                                key={0}
                                droppableId={'mappedBox'}
                                isDropDisabled={selectedFeeGroup === ''}
                            >
                                {(provided, snapshot) => (
                                    <Paper
                                        component='ul'
                                        ref={provided.innerRef}
                                        style={getMappedListStyle(snapshot.isDraggingOver)}
                                        {...provided.droppableProps}
                                    >
                                        {getFeesFromActiveGroup().map((value, index, array) => {
                                            return (
                                                <Draggable
                                                    key={value.id}
                                                    draggableId={value.id}
                                                    index={index}
                                                >
                                                    {(provided, snapshot) => (
                                                        <li key={value.id}>
                                                            <Chip
                                                                ref={provided.innerRef}
                                                                {...provided.draggableProps}
                                                                {...provided.dragHandleProps}
                                                                label={`${value.name}`}
                                                                style={getItemStyle(
                                                                    snapshot.isDragging,
                                                                    provided.draggableProps.style
                                                                )}
                                                            />
                                                        </li>
                                                    )}
                                                </Draggable>
                                            );
                                        })}
                                        {provided.placeholder}
                                    </Paper>
                                )}
                            </Droppable>
                        </Grid>
                    </Grid>
                </DragDropContext>
                <Dialog open={isFeeGroupEditorOpen} fullWidth maxWidth={'sm'}>
                    <DialogTitle>Add A New Fee Group</DialogTitle>
                    <DialogContent>
                        {doesFeeGroupAlreadyExist(feeInputValue) && (
                            <Alert severity={'error'}>
                                <AlertTitle>Fee Group Already Exists</AlertTitle>
                                <Typography variant={'body1'}>
                                    Please enter a unique name for this fee group. Existing fee
                                    group names cannot be used.
                                </Typography>
                            </Alert>
                        )}
                        <TextField
                            placeholder={'Enter a name for this fee group'}
                            fullWidth
                            autoFocus
                            value={feeInputValue}
                            error={doesFeeGroupAlreadyExist(feeInputValue)}
                            onKeyUp={(event) => {
                                if (event.key.toLowerCase() === 'enter') {
                                    if (!doesFeeGroupAlreadyExist(feeInputValue)) {
                                        setIsFeeGroupEditorOpen(false);
                                        setSelectedFeeGroup(feeInputValue);
                                        setTempFeeList([...tempFeeList, feeInputValue]);
                                    }
                                }
                            }}
                            onChange={(event) => {
                                setFeeInputValue(event.currentTarget.value);
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setIsFeeGroupEditorOpen(false)} color='primary'>
                            Cancel
                        </Button>
                        <Button
                            disabled={
                                feeInputValue === '' || doesFeeGroupAlreadyExist(feeInputValue)
                            }
                            onClick={() => {
                                setIsFeeGroupEditorOpen(false);
                                setSelectedFeeGroup(feeInputValue);
                                setTempFeeList([...tempFeeList, feeInputValue]);
                            }}
                            color='primary'
                        >
                            Submit
                        </Button>
                    </DialogActions>
                </Dialog>
            </Grid>
        </Grid>
    );
};

export const ImportFeesDialog: React.FC<IImportFeesDialogProps> = (props) => {
    const ImportFeesContent = () => {
        const [parsedFees, setParsedFees] = React.useState<IParsedFee[]>([]);
        const [ignoreZeroFee, setIgnoreZeroFee] = React.useState<boolean>(false);
        const [useAverageFee, setUseAverageFee] = React.useState<boolean>(false);
        const { clientList, refreshClients } = useClientManager();
        const { feeList, refreshFees } = useFeeManager();

        const [finalGroupedFees, setFinalGroupFees] = React.useState<Map<string, IImportedFee[]>>(
            new Map<string, IImportedFee[]>()
        );

        const [isConfirmationWindowOpen, setIsConfirmationWindowOpen] = React.useState<boolean>(
            false
        );

        const [processStatus, setProcessStatus] = React.useState<string>('');
        const [cancelProcess, setCancelProcess] = React.useState<boolean>(false);
        const [isProcessing, setIsProcessing] = React.useState<boolean>(false);
        const cancelStatus = React.useRef(cancelProcess);

        const DropFeesPage = () => {
            return (
                <Grid container>
                    <Grid item xs>
                        <Typography
                            variant={'body2'}
                            style={{
                                marginTop: '20px',
                            }}
                        >
                            Example Data:
                        </Typography>
                        <Paper
                            variant={'elevation'}
                            elevation={2}
                            style={{
                                padding: '10px',
                            }}
                        >
                            <img
                                src={sampleFeeDataImage}
                                alt={'example-monthly-adr-data'}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Paper>
                        <Alert severity={'info'} style={{ marginTop: '10px' }}>
                            <AlertTitle>Imported Data Note:</AlertTitle>
                            <Typography variant={'body2'}>
                                For each fee imported, only the first column of raw data will be
                                used ("Standard" in the example above).
                            </Typography>
                            <Typography variant={'body2'}>
                                To average all subsequent columns into one, click the box below
                                before dropping your data.:
                            </Typography>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        defaultChecked={useAverageFee}
                                        onChange={(event, checked) => {
                                            setUseAverageFee(checked);
                                        }}
                                        name='checkedF'
                                    />
                                }
                                label='Average all fee data'
                            />
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        defaultChecked={ignoreZeroFee}
                                        onChange={(event, checked) => {
                                            setIgnoreZeroFee(checked);
                                        }}
                                        name='checkedF'
                                    />
                                }
                                label='Ignore fees with $0 value'
                            />
                        </Alert>
                        <CsvDropZone
                            onFileReady={async (files) => {
                                readFeesFile(files).then((fees) => {
                                    setParsedFees(fees);
                                });
                                return true;
                            }}
                            dropZoneRenderer={() => (
                                <Typography>
                                    Please upload a CSV file containing fees data.
                                </Typography>
                            )}
                        />
                    </Grid>
                </Grid>
            );
        };

        const getAverageValue = (values: number[], excludeZero?: undefined): number => {
            let averageValue = 0;

            if (values.length === 0) {
                return averageValue;
            }

            if (excludeZero) {
                const nonZeroValues = values.filter((x) => x !== 0);

                return nonZeroValues.reduce((a, b) => a + b) / nonZeroValues.length;
            }

            return values.reduce((a, b) => a + b) / values.length;
        };

        const processAndAddFees = async () => {
            setIsProcessing(true);

            // Add selected fees for each property/unit in the CSV file
            for (let i = 0; i < parsedFees.length; i++) {
                let propertyData = parsedFees[i];
                const propertyName = propertyData.name;

                setProcessStatus(`Adding fees for ${propertyName} (${i} of ${parsedFees.length})`);

                // For each group, add all individual fees together
                const allGroupNames = finalGroupedFees.keys();

                for (let feeGroupName of allGroupNames) {
                    if (cancelStatus.current) {
                        setCancelProcess(false);
                        setIsProcessing(false);
                        return;
                    }

                    const feeGroupItems =
                        finalGroupedFees.get(feeGroupName)?.map((group) => group.name) ?? [];

                    let feeTotal = 0;

                    feeGroupItems.forEach((feeName) => {
                        // @ts-ignore
                        const feeValues: number[] | undefined = propertyData[feeName].map((x) =>
                            Number.parseFloat(x.replace('$', ''))
                        );

                        if (feeValues && feeValues.length) {
                            let feeValue = 0;

                            // Get average fee if the user requested it.
                            if (useAverageFee) {
                                feeValue = getAverageValue(feeValues);
                            }

                            // Use the first fee column
                            feeValue = feeValues[0];

                            if (ignoreZeroFee && feeValue === 0) {
                                // Skip this fee
                            } else {
                                feeTotal += feeValue;
                            }
                        }
                    });

                    const matchingClient = clientList.find((x) => x.name === propertyName);

                    if (matchingClient && matchingClient.id) {
                        await addFee(matchingClient, {
                            value: feeTotal,
                            clientId: matchingClient.id,
                            name: feeGroupName,
                        });
                    } else {
                        const newClientId = await addClient({
                            id: undefined,
                            name: propertyName,
                        });
                        await addFee(
                            { name: propertyName, id: newClientId },
                            {
                                value: feeTotal,
                                clientId: newClientId,
                                name: feeGroupName,
                            }
                        );
                    }
                }
            }

            refreshClients();
            refreshFees();

            setIsProcessing(false);

            if (props.onClose) {
                props.onClose();
            }
        };

        const getDialogHeader = () => {
            if (parsedFees.length === 0) {
                return 'Import Property Fees';
            } else {
                return 'Select Fees To Import';
            }
        };

        const onFeeGroupsReady = async () => {
            await processAndAddFees();
        };

        return (
            <Dialog open={props.isOpen} maxWidth={'md'} fullWidth={true}>
                <DialogTitle>{getDialogHeader()}</DialogTitle>
                <DialogContent dividers>
                    {parsedFees.length === 0 && <DropFeesPage />}
                    {parsedFees.length > 0 && (
                        <GroupAndSelectFeesPage
                            currentFeeList={feeList}
                            parsedFees={parsedFees}
                            onFeeGroupsUpdated={(groups) => setFinalGroupFees(groups)}
                        />
                    )}
                    <Dialog open={isProcessing} maxWidth={'sm'} fullWidth={true}>
                        <DialogTitle>Processing Fees</DialogTitle>
                        <DialogContent dividers>
                            <Grid container>
                                <Grid
                                    item
                                    xs
                                    style={{
                                        textAlign: 'center',
                                    }}
                                >
                                    <CircularProgress />
                                    <Typography variant={'body1'}>
                                        Please wait... {processStatus}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                autoFocus
                                color='primary'
                                onClick={() => setCancelProcess(true)}
                            >
                                Cancel
                            </Button>
                        </DialogActions>
                    </Dialog>
                    <Dialog open={isConfirmationWindowOpen}>
                        <DialogTitle>Unmapped Fees Remain</DialogTitle>
                        <DialogContent>
                            <Alert severity={'warning'}>
                                <AlertTitle>
                                    There are still fees that have not been mapped to a group.
                                    Unmapped fees will not be imported!
                                </AlertTitle>
                                Are you sure you want to continue?
                            </Alert>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                autoFocus
                                onClick={() => setIsConfirmationWindowOpen(false)}
                                color='primary'
                            >
                                No
                            </Button>
                            <Button
                                autoFocus
                                onClick={() => {
                                    setIsConfirmationWindowOpen(false);
                                    onFeeGroupsReady();
                                }}
                                color='primary'
                            >
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus color='primary' onClick={props.onClose}>
                        Cancel Import
                    </Button>
                    <Button
                        autoFocus
                        color='primary'
                        disabled={finalGroupedFees.size === 0}
                        onClick={() => {
                            setIsConfirmationWindowOpen(true);
                        }}
                    >
                        Save And Close
                    </Button>
                </DialogActions>
            </Dialog>
        );
    };

    return (
        <Suspense fallback={<CircularProgress />}>
            <ImportFeesContent />
        </Suspense>
    );
};
