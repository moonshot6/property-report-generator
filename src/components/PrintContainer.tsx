import React from 'react';
import path from 'path';
import { getSaveDirectory, printToPdf } from '../actions/Export';
import { useAtom } from 'jotai';
import { selectedClientAtom, useClientManager } from '../actions/Configuration';
import { PrintDialog } from './PrintDialog';

interface IPrintContainerProps {
    print: boolean;
    onPrintFinished?: () => void;
}

export interface IActivePrintInfo {
    name: string;
    jobId: number;
    outputDirectory: string;
    maxPrintJobs: number;
}

export const PrintContainer: React.FC<IPrintContainerProps> = (props) => {
    const [activePrintInfo, setActivePrintInfo] = React.useState<IActivePrintInfo | undefined>(
        undefined
    );
    const [isPrintDialogOpen, setIsPrintDialogOpen] = React.useState<boolean>(false);
    const [isPrintCancelled, setIsPrintCancelled] = React.useState<boolean>(false);
    const [selectedClient, setSelectedClient] = useAtom(selectedClientAtom);
    const { clientList } = useClientManager();

    React.useEffect(() => {
        if (activePrintInfo) {
            setSelectedClient(clientList[activePrintInfo.jobId].id);
        }
    }, [activePrintInfo]);

    React.useEffect(() => {
        if (activePrintInfo) {
            if (props.print) {
                // Ignore, we won't restart an in-progress print operation
            } else {
                // Cancel the active print
                setIsPrintCancelled(true);
            }
        } else {
            if (props.print) {
                // No print active, start one
                setIsPrintDialogOpen(true);
                getSaveDirectory().then((directoryPath) => {
                    if (directoryPath.length === 0) {
                        // No directory selected!
                        setIsPrintDialogOpen(false);
                        return;
                    }

                    setActivePrintInfo({
                        name: clientList[0].name,
                        jobId: 0,
                        outputDirectory: directoryPath,
                        maxPrintJobs: clientList.length,
                    });
                });
            }
        }
    }, [props.print]);

    React.useLayoutEffect(() => {
        if (activePrintInfo) {
            const printDoc = () => {
                const printPath = path.join(
                    activePrintInfo.outputDirectory,
                    clientList[activePrintInfo.jobId].name + '.pdf'
                );
                printToPdf(printPath).then(() => {
                    if (activePrintInfo.jobId + 1 >= clientList.length) {
                        setIsPrintDialogOpen(false);
                        setActivePrintInfo(undefined);
                        return;
                    }

                    setActivePrintInfo({
                        ...activePrintInfo,
                        jobId: ++activePrintInfo.jobId,
                        name: clientList[activePrintInfo.jobId].name,
                    });
                });
            };

            if (activePrintInfo.jobId >= clientList.length) {
                setIsPrintDialogOpen(false);
                setActivePrintInfo(undefined);
                setIsPrintCancelled(false);

                if (props.onPrintFinished) {
                    props.onPrintFinished();
                }

                return;
            }

            if (!isPrintCancelled) {
                window.setTimeout(printDoc, 100);
            } else {
                setIsPrintDialogOpen(false);
                setActivePrintInfo(undefined);
                setIsPrintCancelled(false);

                if (props.onPrintFinished) {
                    props.onPrintFinished();
                }
            }
        }
    }, [selectedClient]);

    const onCancelPrintClicked = () => {
        setIsPrintCancelled(true);
    };

    return (
        <>
            {props.children}
            <PrintDialog
                isOpen={isPrintDialogOpen}
                printInfo={activePrintInfo}
                onCancelClicked={onCancelPrintClicked}
            />
        </>
    );
};
