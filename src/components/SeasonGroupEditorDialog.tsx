import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    TextField,
} from '@material-ui/core';
import { Delete, Save } from '@material-ui/icons';
import DateFnsUtils from '@date-io/date-fns';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';

import { ISeasonGroup } from 'model/Configuration';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

interface ISeasonGroupEditorDialogProps {
    isOpen: boolean;
    defaultSeasonGroup?: ISeasonGroup;
    onClose: (group?: ISeasonGroup) => void;
}

interface ISeasonGroupFormFields {
    name: string;
    startDate: Date;
    endDate: Date;
    minimumStayLength: string;
}

const emptyIfUndefined = (season?: ISeasonGroup): ISeasonGroupFormFields => {
    if (season) {
        return {
            name: season.name,
            startDate: season.startDate,
            endDate: season.endDate,
            minimumStayLength: season.minimumStayLength.toString(),
        };
    }

    return {
        name: '',
        endDate: new Date(),
        startDate: new Date(),
        minimumStayLength: '1',
    };
};

export const SeasonGroupEditorDialog: React.FC<ISeasonGroupEditorDialogProps> = (props) => {
    const nameInputRef = React.useRef<HTMLInputElement>(null);

    const [seasonGroupData, setSeasonGroupData] = React.useState<ISeasonGroupFormFields>(
        emptyIfUndefined(props.defaultSeasonGroup)
    );

    React.useEffect(() => {
        if (props.isOpen) {
            if (nameInputRef) {
                if (nameInputRef.current) {
                    nameInputRef.current.select();
                }
            }
        }
    }, [props.isOpen]);

    React.useEffect(() => {
        setSeasonGroupData(emptyIfUndefined(props.defaultSeasonGroup));
    }, [props.defaultSeasonGroup]);

    const validateGroupData = (): boolean => {
        return (
            seasonGroupData.name.length > 0 &&
            seasonGroupData.startDate &&
            seasonGroupData.endDate &&
            seasonGroupData.minimumStayLength.length > 0
        );
    };

    const onClose = () => {
        if (validateGroupData()) {
            props.onClose({
                id: props.defaultSeasonGroup?.id,
                assignedClientIds: props.defaultSeasonGroup?.assignedClientIds ?? [],
                name: seasonGroupData.name,
                minimumStayLength: Number.parseInt(seasonGroupData.minimumStayLength),
                startDate: seasonGroupData.startDate,
                endDate: seasonGroupData.endDate,
            });
        }
    };

    const getDialogTitle = () => {
        if (props.defaultSeasonGroup) {
            return 'Edit Season Group';
        } else {
            return 'Add A New Season Group';
        }
    };

    const onNameChanged = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setSeasonGroupData({
            ...seasonGroupData,
            name: event.currentTarget.value,
        });
    };

    const onMinStayChanged = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        if (/^[\d]{0,}$/.test(event.currentTarget.value)) {
            setSeasonGroupData({
                ...seasonGroupData,
                minimumStayLength: event.currentTarget.value,
            });
        }
    };

    const onStartDateChanged = (date: MaterialUiPickersDate | null) => {
        if (date) {
            setSeasonGroupData({
                ...seasonGroupData,
                startDate: date,
            });
        }
    };

    const onEndDateChanged = (date: MaterialUiPickersDate | null) => {
        if (date) {
            setSeasonGroupData({
                ...seasonGroupData,
                endDate: date,
            });
        }
    };

    return (
        <Dialog open={props.isOpen} onClose={onClose} fullWidth={true} maxWidth={'sm'}>
            <DialogTitle>{getDialogTitle()}</DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs>
                        <TextField
                            required
                            autoFocus
                            id='season-group-name'
                            label='Season Group Name'
                            inputRef={nameInputRef}
                            fullWidth={true}
                            variant='outlined'
                            value={seasonGroupData.name}
                            onChange={onNameChanged}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            required
                            id='min-length-stay'
                            label='Minimum Nights'
                            fullWidth={true}
                            variant='outlined'
                            value={seasonGroupData.minimumStayLength}
                            onChange={onMinStayChanged}
                        />
                    </Grid>
                </Grid>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container spacing={1}>
                        <Grid item xs>
                            <KeyboardDatePicker
                                autoOk
                                disableToolbar
                                variant='inline'
                                format='MM/dd/yyyy'
                                margin='normal'
                                id='start-date-picker-inline'
                                label='Start Date'
                                value={seasonGroupData.startDate}
                                onChange={onStartDateChanged}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                        <Grid item xs>
                            <KeyboardDatePicker
                                autoOk
                                disableToolbar
                                variant='inline'
                                format='MM/dd/yyyy'
                                margin='normal'
                                id='end-date-picker-inline'
                                label='End Date'
                                value={seasonGroupData.endDate}
                                onChange={onEndDateChanged}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                    </Grid>
                </MuiPickersUtilsProvider>
            </DialogContent>
            <DialogActions>
                <Button
                    variant='contained'
                    color='default'
                    size='medium'
                    startIcon={<Delete />}
                    onClick={() => props.onClose && props.onClose(undefined)}
                    style={{
                        marginTop: 'auto',
                    }}
                >
                    Cancel
                </Button>
                <Button
                    disabled={!validateGroupData()}
                    variant='contained'
                    color='primary'
                    size='medium'
                    startIcon={<Save />}
                    onClick={onClose}
                    style={{
                        marginTop: 'auto',
                    }}
                >
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    );
};
