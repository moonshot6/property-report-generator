import React, { Suspense } from 'react';
import {
    Box,
    Button,
    CircularProgress,
    Collapse,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
} from '@material-ui/core';
import ErrorCircleIcon from '@material-ui/icons/ErrorOutline';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import { useClientManager } from '../actions/Configuration';
import { db } from '../actions/ApplicationDatabase';

interface IVerifyDataDialogProps {
    isOpen: boolean;
    onContinue?: () => void;
    onClose?: () => void;
}

enum ValidationErrorType {
    ADRData = 'ADR',
    DailyRateData = 'Daily Rates',
    Fees = 'Fees',
    OccupancyData = 'Occupancy',
    RevenueData = 'Revenue',
    UnusedSeason = 'Unused Season',
    Other = 'Other',
}

enum Severity {
    Low = 'Minor',
    Medium = 'Medium',
    High = 'Major',
}

interface IValidationInfo {
    errorType: ValidationErrorType;
    severity: Severity;
    message: string;
}

interface IPropertyValidationInfo {
    propertyName: string;
    issues: IValidationInfo[];
}

export const VerifyDataDialog: React.FC<IVerifyDataDialogProps> = (props) => {
    const [validationData, setValidationData] = React.useState<IPropertyValidationInfo[]>([]);
    const [isValidationComplete, setIsValidationComplete] = React.useState<boolean>(false);
    const [errorMessage, setErrorMessage] = React.useState<string | undefined>(undefined);
    const [currentPropertyIndex, setCurrentPropertyIndex] = React.useState<number>(0);
    const { clientList } = useClientManager();

    const validate = React.useCallback(async (): Promise<void> => {
        setIsValidationComplete(false);
        setErrorMessage(undefined);
        setValidationData([]);

        const localValidationData: Map<string, IPropertyValidationInfo> = new Map<
            string,
            IPropertyValidationInfo
        >();

        const addEntry = (propertyName: string, info: IValidationInfo) => {
            if (localValidationData.has(propertyName)) {
                localValidationData.get(propertyName)?.issues.push(info);
            } else {
                localValidationData.set(propertyName, {
                    propertyName: propertyName,
                    issues: [info],
                });
            }
        };

        if (clientList.length === 0) {
            addEntry('System', {
                errorType: ValidationErrorType.Other,
                severity: Severity.High,
                message: 'No client data to export!!!',
            });
            setErrorMessage('No client data to export. Please try restarting this application.');
            setValidationData(Array.from(localValidationData.values()));
            return;
        }

        // Validate that every client has fees
        let averageFeeCountPerClient = 0;

        for (const client of clientList) {
            if (client && client.id) {
                const feeCount = await db.fees.where('clientId').equals(client.id).count();
                averageFeeCountPerClient += feeCount;
            }
        }

        averageFeeCountPerClient = averageFeeCountPerClient / clientList.length;

        for (let i = 0; i < clientList.length; i++) {
            const client = clientList[i];
            setCurrentPropertyIndex(i);

            if (client && client.id) {
                const feesForClient = await db.fees.where('clientId').equals(client.id).toArray();

                // Check to see if a client has less fees than the average (possible missing data).
                if (feesForClient.length === 0) {
                    addEntry(client.name, {
                        errorType: ValidationErrorType.Fees,
                        severity: Severity.Medium,
                        message: 'No fees for this client.',
                    });
                } else if (feesForClient.length < averageFeeCountPerClient) {
                    addEntry(client.name, {
                        errorType: ValidationErrorType.Fees,
                        severity: Severity.Low,
                        message: 'The number of fees for this client is less than the average.',
                    });
                }

                // Check to see if a client has fees with a value of zero.
                for (const fee of feesForClient) {
                    if (fee.name === '') {
                        addEntry(client.name, {
                            errorType: ValidationErrorType.Fees,
                            severity: Severity.High,
                            message: `A fee is missing a name.`,
                        });
                    }

                    if (fee.value === 0) {
                        addEntry(client.name, {
                            errorType: ValidationErrorType.Fees,
                            severity: Severity.Medium,
                            message: `The fee ${fee.name} has a value of 0.`,
                        });
                    }
                }

                const TODAY = new Date();
                const LAST_THREE_YEARS: number[] = [
                    TODAY.getFullYear() - 2,
                    TODAY.getFullYear() - 1,
                    TODAY.getFullYear(),
                ];

                // Validate that every client has ADR data
                for (const year of LAST_THREE_YEARS) {
                    const clientAdrData = await db.adrData
                        .where('clientId')
                        .equals(client.id)
                        .and((x) => {
                            return x.year === year;
                        })
                        .first();

                    if (!clientAdrData) {
                        addEntry(client.name, {
                            errorType: ValidationErrorType.ADRData,
                            severity: Severity.High,
                            message: `Client is missing ADR data for ${year}!`,
                        });
                    }
                }

                // Validate that ADR has a matching Occupancy Value
                for (const year of LAST_THREE_YEARS) {
                    const clientOccupancyData = await db.occupancyData
                        .where('clientId')
                        .equals(client.id)
                        .and((x) => {
                            return x.year === year;
                        })
                        .first();

                    if (!clientOccupancyData) {
                        addEntry(client.name, {
                            errorType: ValidationErrorType.OccupancyData,
                            severity: Severity.High,
                            message: `Client is missing Occupancy data for ${year}!`,
                        });
                    }
                }

                // Validate that every client has Revenue data
                for (const year of LAST_THREE_YEARS) {
                    const clientRevenueData = await db.revenueData
                        .where('clientId')
                        .equals(client.id)
                        .and((x) => {
                            return x.year === year;
                        })
                        .first();

                    if (!clientRevenueData) {
                        addEntry(client.name, {
                            errorType: ValidationErrorType.RevenueData,
                            severity: Severity.High,
                            message: `Client is missing revenue data for ${year}!`,
                        });
                    }
                }

                // Validate that every client has daily rates
                const clientDailyRates = await db.dailyRatesData
                    .where('clientId')
                    .equals(client.id)
                    .first();

                if (!clientDailyRates) {
                    addEntry(client.name, {
                        errorType: ValidationErrorType.DailyRateData,
                        severity: Severity.High,
                        message: `Client is missing daily rates data!`,
                    });
                } else {
                    // Validate each season has a rate
                    for (const rate of clientDailyRates.rates) {
                        if (rate.rate === 0 || Number.isNaN(rate.rate)) {
                            const season = await db.seasonGroups
                                .where('id')
                                .equals(rate.seasonId)
                                .first();

                            if (season) {
                                addEntry(client.name, {
                                    errorType: ValidationErrorType.DailyRateData,
                                    severity: Severity.High,
                                    message: `The rate for ${season.name} is either 0 or NaN. This is most likely the result of invalid data. Please try reimporting.`,
                                });
                            } else {
                                addEntry(client.name, {
                                    errorType: ValidationErrorType.DailyRateData,
                                    severity: Severity.High,
                                    message: `A rate is mapped to an invalid season.`,
                                });
                            }
                        }
                    }

                    // Warn about unused seasons in a client
                    const allSeasons = await db.seasonGroups.toArray();

                    for (const season of allSeasons) {
                        if (!season.assignedClientIds.includes(client.id)) {
                            addEntry(client.name, {
                                errorType: ValidationErrorType.DailyRateData,
                                severity: Severity.Medium,
                                message: `The client does not have a rate for ${season.name}.`,
                            });
                        }
                    }
                }
            } else {
                addEntry(client.name, {
                    errorType: ValidationErrorType.Other,
                    severity: Severity.High,
                    message:
                        'Client does not have an identifier! This may cause application wide issues',
                });
            }
        }

        setErrorMessage(undefined);
        setValidationData(Array.from(localValidationData.values()));
        setIsValidationComplete(true);
        return;
    }, [clientList]);

    React.useEffect(() => {
        if (props.isOpen) {
            validate();
        }
    }, [validate, props.isOpen]);

    const ValidationStatus = () => {
        if (errorMessage) {
            return (
                <>
                    <ErrorCircleIcon fontSize={'large'} />
                    <Typography variant={'body1'}>
                        An error occurred while processing your request
                    </Typography>
                    <Typography variant={'caption'}>{errorMessage}</Typography>
                </>
            );
        } else {
            return (
                <>
                    <Typography variant={'body1'}>
                        Please wait while your reports are checked for completion...
                    </Typography>
                    <Typography variant={'caption'}>
                        Currently verifying <i>{clientList[currentPropertyIndex].name}</i> (
                        {currentPropertyIndex + 1} of {clientList.length})
                    </Typography>
                </>
            );
        }
    };

    const ValidationResultsContent = () => {
        if (validationData.length > 0) {
            return (
                <TableContainer component={Paper}>
                    <Table aria-label='collapsible table dense' size={'small'}>
                        <TableHead>
                            <TableRow>
                                <TableCell colSpan={6} align={'center'}>
                                    <Typography variant={'h6'}>
                                        {validationData.length} issues were found during validation.
                                    </Typography>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableHead>
                            <TableRow>
                                <TableCell />
                                <TableCell>Property Name</TableCell>
                                <TableCell>Minor Errors</TableCell>
                                <TableCell>Medium Errors</TableCell>
                                <TableCell>Major Errors</TableCell>
                                <TableCell>Total Errors</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {validationData.map((value) => {
                                return (
                                    <ErrorRow
                                        propertyName={value.propertyName}
                                        errors={value.issues}
                                    />
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            );
        } else {
            return (
                <Grid container>
                    <Grid
                        item
                        xs
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        <CheckCircleIcon fontSize={'large'} style={{ color: 'green' }} />
                        <Typography variant={'body1'}>No Issues Found</Typography>
                    </Grid>
                </Grid>
            );
        }
    };

    const ErrorRow: React.FC<{ propertyName: string; errors: IValidationInfo[] }> = (props) => {
        const [open, setOpen] = React.useState(false);

        const getLowErrorsCount = () => {
            const count = props.errors.filter((x) => x.severity === Severity.Low).length;

            if (count == 0) {
                return <p style={{ color: '#c3c3c3' }}>{count}</p>;
            } else {
                return (
                    <b
                        style={{
                            color: 'white',
                            backgroundColor: '#5a4d27',
                            padding: '3px 10px',
                            borderRadius: '4px',
                        }}
                    >
                        {count}
                    </b>
                );
            }
        };

        const getMediumErrorsCount = () => {
            const count = props.errors.filter((x) => x.severity === Severity.Medium).length;

            if (count == 0) {
                return <p style={{ color: '#c3c3c3' }}>{count}</p>;
            } else {
                return (
                    <b
                        style={{
                            color: 'white',
                            backgroundColor: '#a75701',
                            padding: '3px 10px',
                            borderRadius: '4px',
                        }}
                    >
                        {count}
                    </b>
                );
            }
        };

        const getHighErrorsCount = () => {
            const count = props.errors.filter((x) => x.severity === Severity.High).length;

            if (count == 0) {
                return <p style={{ color: '#c3c3c3' }}>{count}</p>;
            } else {
                return (
                    <b
                        style={{
                            color: 'white',
                            backgroundColor: '#720303',
                            padding: '3px 10px',
                            borderRadius: '4px',
                        }}
                    >
                        {count}
                    </b>
                );
            }
        };

        const getTotalErrorsCount = () => {
            const count = props.errors.length;

            if (count == 0) {
                return <p style={{ color: '#c3c3c3' }}>{count}</p>;
            } else {
                return (
                    <b
                        style={{
                            color: 'white',
                            backgroundColor: 'black',
                            padding: '3px 10px',
                            borderRadius: '4px',
                        }}
                    >
                        {count}
                    </b>
                );
            }
        };

        const getErrorTypeCell = (error: IValidationInfo) => {
            return <i>{error.errorType}</i>;
        };

        const getErrorSeverityCell = (error: IValidationInfo) => {
            switch (error.severity) {
                case Severity.Low:
                    return (
                        <b
                            style={{
                                color: 'white',
                                backgroundColor: '#5a4d27',
                                padding: '3px 10px',
                                borderRadius: '4px',
                            }}
                        >
                            Minor
                        </b>
                    );
                case Severity.Medium:
                    return (
                        <b
                            style={{
                                color: 'white',
                                backgroundColor: '#a75701',
                                padding: '3px 10px',
                                borderRadius: '4px',
                            }}
                        >
                            Medium
                        </b>
                    );
                case Severity.High:
                    return (
                        <b
                            style={{
                                color: 'white',
                                backgroundColor: '#720303',
                                padding: '3px 10px',
                                borderRadius: '4px',
                            }}
                        >
                            Major
                        </b>
                    );
                default:
                    return <b>Other</b>;
            }
        };

        return (
            <React.Fragment>
                <TableRow>
                    <TableCell>
                        <IconButton
                            aria-label='expand row'
                            size='small'
                            onClick={() => setOpen(!open)}
                        >
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    </TableCell>
                    <TableCell component='th' scope='row'>
                        {props.propertyName}
                    </TableCell>
                    <TableCell>{getLowErrorsCount()}</TableCell>
                    <TableCell>{getMediumErrorsCount()}</TableCell>
                    <TableCell>{getHighErrorsCount()}</TableCell>
                    <TableCell>{getTotalErrorsCount()}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout='auto' unmountOnExit={true}>
                            <Box margin={1}>
                                <Typography variant='h6' gutterBottom component='div'>
                                    Validation Errors
                                </Typography>
                                <Table size='small' aria-label='purchases'>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Type</TableCell>
                                            <TableCell>Severity</TableCell>
                                            <TableCell align='right'>Recommendation</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {props.errors.map((error, index) => (
                                            <TableRow key={index}>
                                                <TableCell component='th' scope='row'>
                                                    {getErrorTypeCell(error)}
                                                </TableCell>
                                                <TableCell>{getErrorSeverityCell(error)}</TableCell>
                                                <TableCell align='right'>{error.message}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Box>
                        </Collapse>
                    </TableCell>
                </TableRow>
            </React.Fragment>
        );
    };

    const getActions = () => {
        if (isValidationComplete) {
            return (
                <>
                    <Button
                        color='primary'
                        onClick={props.onClose}
                        style={{
                            marginRight: '10px',
                        }}
                    >
                        {validationData.length >= 1 ? 'Cancel And Resolve' : 'Cancel'}
                    </Button>
                    <Button color='primary' onClick={props.onContinue}>
                        {validationData.length >= 1 ? 'Print Anyway' : 'Print'}
                    </Button>
                </>
            );
        } else {
            return (
                <Grid container>
                    <Grid
                        item
                        xs
                        style={{
                            textAlign: 'center',
                        }}
                    >
                        <Button
                            autoFocus
                            color='primary'
                            variant={'outlined'}
                            onClick={props.onClose}
                        >
                            Cancel
                        </Button>
                    </Grid>
                </Grid>
            );
        }
    };

    return (
        <Suspense fallback={<CircularProgress />}>
            <Dialog onClose={props.onClose} open={props.isOpen} maxWidth={'md'}>
                <DialogTitle>Verifying Report Data</DialogTitle>
                <DialogContent>
                    {isValidationComplete && <ValidationResultsContent />}
                    {!isValidationComplete && (
                        <Grid container>
                            <Grid
                                item
                                xs
                                style={{
                                    textAlign: 'center',
                                }}
                            >
                                {errorMessage === undefined && <CircularProgress />}
                                <ValidationStatus />
                            </Grid>
                        </Grid>
                    )}
                </DialogContent>
                <DialogActions>{getActions()}</DialogActions>
            </Dialog>
        </Suspense>
    );
};
