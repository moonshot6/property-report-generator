import React from 'react';
import { Grid, Paper } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import { CsvDropZone } from './CsvDropZone';

interface IDropZoneListArea {
    count: number;
    dropContentRenderer: (index: number) => React.ReactNode;
    onDropFilesReady: (index: number, files: File[]) => Promise<boolean>;
}

export const DropZoneListArea: React.FC<IDropZoneListArea> = (props) => {
    const DropZone: React.FC<{ index: number }> = (dropZoneProps) => {
        const [dropResult, setDropResult] = React.useState<boolean | undefined>(undefined);

        return (
            <Grid item>
                <Paper>
                    <CsvDropZone
                        onFileReady={async (files) => {
                            const processedSuccessfully = await props.onDropFilesReady(
                                dropZoneProps.index,
                                files
                            );

                            setDropResult(processedSuccessfully);

                            if (processedSuccessfully) {
                                return processedSuccessfully;
                            } else {
                                return 'Unable to read the contents of the file provided. Please ensure the file contents match the requested type.';
                            }
                        }}
                        dropZoneRenderer={() => props.dropContentRenderer(dropZoneProps.index)}
                    />
                    {dropResult && (
                        <div
                            style={{
                                backgroundColor: 'rgba(255,255,255,0.8)',
                            }}
                        >
                            <div
                                style={{
                                    marginTop: 'auto',
                                    marginBottom: 'auto',
                                    textAlign: 'center',
                                }}
                            >
                                <CheckCircleIcon
                                    style={{
                                        color: 'green',
                                    }}
                                />
                            </div>
                        </div>
                    )}
                    {dropResult !== undefined && !dropResult && (
                        <div
                            style={{
                                backgroundColor: 'rgba(255,255,255,0.8)',
                            }}
                        >
                            <div
                                style={{
                                    marginTop: 'auto',
                                    marginBottom: 'auto',
                                    textAlign: 'center',
                                }}
                            >
                                <ErrorIcon
                                    style={{
                                        color: 'red',
                                    }}
                                />
                            </div>
                        </div>
                    )}
                </Paper>
            </Grid>
        );
    };

    const dropZones = React.useMemo(() => {
        let zones: React.ReactNodeArray = [];

        for (let i = 0; i < props.count; ++i) {
            zones.push(<DropZone index={i} />);
        }

        return zones;
    }, [props.count]);

    return (
        <Grid container justify='center' spacing={1}>
            {dropZones}
        </Grid>
    );
};
